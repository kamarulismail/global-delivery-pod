<?php

/**
 * Description of IndexController
 *
 * @author Kamarul Ariffin Ismail <kamarul.ismail@gmail.com>
 */
class IndexController extends ZC_Controller_Action {
    protected $_loginCookieName = 'GDP_User';

    private $_configPrefix = 'main';
    private $_registryName = 'MainUser';
    private $_errorHandlerController = 'Index';

    public function init() {
        $_appSettings = array();
        if(Zend_Registry::isRegistered('appSettings')){
            $_appSettings = Zend_Registry::get('appSettings');
        }
        $this->_appSettings = $_appSettings;

        //DEFAULT LAYOUT
        $this->_layout = isset($_appSettings[$this->_configPrefix]['layout']) ? $_appSettings[$this->_configPrefix]['layout'] : 'main';

        //PREFIX URL
        $_prefixUrl       = isset($_appSettings['site']['prefixUrl']) ? $_appSettings['site']['prefixUrl'] : '/';
        $this->_prefixUrl = rtrim($_prefixUrl, '/') . '/';

        //BASE URL
        $_baseUrl       = isset($_appSettings[$this->_configPrefix]['baseUrl']) ? $_appSettings[$this->_configPrefix]['baseUrl'] : '/';
        $this->_baseUrl = rtrim($_prefixUrl, '/') . rtrim($_baseUrl, '/');

        //ASSET URL
        $_assetUrl       = isset($_appSettings[$this->_configPrefix]['assetUrl']) ? $_appSettings[$this->_configPrefix]['assetUrl'] : '/asset';
        $this->_assetUrl = rtrim($_prefixUrl, '/') . rtrim($_assetUrl, '/') ;

        //SITE NAME
        $this->_siteName = isset($_appSettings[$this->_configPrefix]['siteName']) ? $_appSettings[$this->_configPrefix]['siteName'] : 'SiteName';

        //CHANGE ERROR HANDLER
        $front        = Zend_Controller_Front::getInstance();
        $errorHandler = $front->getPlugin('Zend_Controller_Plugin_ErrorHandler');
        if(isset($front) && $errorHandler){
            $errorHandler->setErrorHandlerController($this->_errorHandlerController);
        }

        //SESSION
        // $registryName = $this->_registryName;
        // if(!Zend_Registry::isRegistered($registryName)){
        //     Zend_Registry::set($registryName, new Zend_Session_Namespace($registryName));
        // }

        //MAIN SIDEBAR
        $this->view->dataMainSidebar = 'Customer';

        //EXCECUTE PARENT INIT()
        parent::init();
    }

    public function preDispatch() {
        parent::preDispatch();

        //DEBUGGING
        $this->debugPHPSession($this->_registryName);
    }

    private function isUserAuthorized(){
        //DEBUGGING
        $tableData = array(
            array('Variable', 'Data')
        );

        //DEFAULT
        $isUserAuthorized = 0;
        $authUser = null;

        //GET FROM SESSION
        $registryName = $this->_registryName;
        if(Zend_Registry::isRegistered($registryName)){
            $registryData = Zend_Registry::get($registryName);
            //DEBUGGING
            $tableData[] = array('$registryData', $registryData);

            //GET AUTH USER
            $authUser = isset($registryData->authUser) ? $registryData->authUser : array();
            //DEBUGGING
            $tableData[] = array('$authUser', $authUser);
        }

        //GET FROM COOKIE
        if (!$authUser) {
            $cookieName = isset($this->_loginCookieName) ? $this->_loginCookieName : 'HLBCookieName';
            $cookieData = $this->getSessionCookie($cookieName);
            //DEBUGGING
            $tableData[] = array("getSessionCookie({$cookieName})", $cookieData);

            //DECODE
            $cookieValue = base64_decode($cookieData);
            //DEBUGGING
            $tableData[] = array('base64_decode($cookieValue)', $cookieValue);

            if($cookieValue){
                $authUser = json_decode($cookieValue, true);
                //DEBUGGING
                $tableData[] = array('cookie.$authUser', $authUser);
            }
        }

        if($authUser){
            $id = isset($authUser['id']) ? $authUser['id'] : null;
            if(!empty($id)){
                $isUserAuthorized = 1;

                if(Zend_Registry::isRegistered($registryName)){
                    //REASSIGN TO SESSION
                    $registryData = Zend_Registry::get($registryName);
                    $registryData->authUser = $authUser;
                }
            }
        }

        //
        $this->view->authUser_ = $authUser;
        $this->view->isUserAuthorized = $isUserAuthorized;
        //DEBUGGING
        $tableData[] = array('$isUserAuthorized', $isUserAuthorized);

        //DEBUGGING
        $debugMode = (APPLICATION_ENV == 'development') ? 1 : 0;
        ZC_FirePHP::table($tableData, __CLASS__.'.'.__FUNCTION__, array('enable' => $debugMode));

        if(!$isUserAuthorized){
            $url = $this->_baseUrl . '/login';
            $this->redirect($url);
        }

        return $authUser;
    }

    private function getLoginCookieOptions(){
        $cookieOptions = array(
            'path'        => $this->_baseUrl,
            #'expires'     => 0, //browser-session cookie
            'secure'      => false
        );

        //SECURE
        if (isset($_SERVER['HTTPS']) && (strtolower($_SERVER['HTTPS']) == 'on')) {
            $cookieOptions['secure'] = true;
        }

        return $cookieOptions;
    }

    public function errorAction(){
        //PAGE TITLE
        $this->_pageTitle = array('Error');

        //
        $errors = $this->getParam('error_handler');

        if (!$errors || !$errors instanceof ArrayObject) {
            $this->view->message = 'You have reached the error page';
            return;
        }

        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                // 404 error -- controller or action not found
                $httpResponseCode = 404;
                $this->getResponse()->setHttpResponseCode(404);
                $priority = Zend_Log::NOTICE;
                $this->view->message = 'Page not found';
                break;
            default:
                // application error
                $httpResponseCode = 500;
                $this->getResponse()->setHttpResponseCode(500);
                $priority = Zend_Log::CRIT;
                $this->view->message = 'Application error';
                break;
        }

        // conditionally display exceptions
        if ($this->getInvokeArg('displayExceptions') == true) {
            $this->view->exception = $errors->exception;
        }

        $this->view->request    = $errors->request;
        $this->view->pageHeader = $this->view->message;
        $this->view->httpResponseCode  = $httpResponseCode;
        $this->view->displayExceptions = $this->getInvokeArg('displayExceptions');
        $this->view->exception         = $errors->exception;
    }

    public function loginAction(){
        //JS
        $customJS = array(
            $this->_assetUrl . '/js/login.js'
        );
        $this->loadCustomScript($customJS);

        #Zend_Debug::dump($this->_debugSettings, '_debugSettings');
        #Zend_Debug::dump(ZC_FirePHP::isEnable(), 'isEnable');

        //USE BOOTSTRAP LAYOUT
        $this->view->useBootstrapLayout = 1;

        //PAGE TITLE
        $this->_pageTitle = array('Login');

        //POST
        $httpRequest = $this->getHttpRequest();
        if($httpRequest->isPost()){
            #$isAjax   = $this->getFilterParam('ajax', 0);

            //DEBUGGING
            $_functionName = __FUNCTION__;

            //CHECK IF AJAX
            $isAjax = $this->getFilterParam('ajax', 0);

            //LOG
            $logFile = APPLICATION_LOG_PATH . DIRECTORY_SEPARATOR . __CLASS__ . '-' . __FUNCTION__ . '-' . date('Ymd') . '.txt';

            //XSS: honeypot
            $honeypot = $this->getParam('honeypot', null);
            if (!empty($honeypot)) {
                $logData = "[{$_functionName}] Possible spam attack! (honeypot:{$honeypot})" . PHP_EOL . $this->getClientIP(1);
                #$this->phpErrorLogger($logData);
                #$this->fileLogger($logFile, $logData);

                $response = $this->getJsonResponse(200);
                $response['debug']['error_message'] = $logData;
                $this->jsonResponse($response);
                return;
            }

            //XSS: CSRF TOKEN
            $csrfToken = $this->getParam('csrf_token', null);
            if (empty($csrfToken)) {
                $logData = "[{$_functionName}] Possible spam attack! (csrf_token:{$csrfToken})" . PHP_EOL . $this->getClientIP(1);
                #$this->phpErrorLogger($logData);

                $response = $this->getJsonResponse(200);
                $response['debug']['error_message'] = $logData;
                $this->jsonResponse($response);
                return;
            }

            $csrfStatus = $this->verifyCSRFToken($csrfToken);
            if ($csrfStatus != 1) {
                $logData = "[{$_functionName}] Possible spam attack! (verifyCSRFToken({$csrfToken}))" . PHP_EOL . $this->getClientIP(1);
                #$this->phpErrorLogger($logData);
                #$this->fileLogger($logFile, $logData);

                $response = $this->getJsonResponse(200);
                $response['debug']['error_message'] = $logData;
                $this->jsonResponse($response);
                return;
            }

            //
            $username = $this->getFilterParam('login_name');
            $password = $this->getFilterParam('login_pass');

            if(empty($username) || empty($password)){
                //AJAX
                if ($isAjax) {
                    $response = $this->getJsonResponse(400);

                    //LOGIN STATUS
                    $response['loginStatus'] = 0;

                    //
                    $response['debug']['param'] = $this->getAllParams();
                    $this->jsonResponse($response);
                    return;
                }

                return;
            }

            //MODEL
            $optionLeadUser = array(
                'username' => $username,
                'password' => $password
            );
            $mCustomer = new Customer();
            $authUser  = $mCustomer->validateUser($optionLeadUser);
            ZC_FirePHP::warn($authUser, __CLASS__.'.'.__FUNCTION__.'.$authUser');

            if(!$authUser || empty($authUser)){
                //AJAX
                if ($isAjax) {
                    $response = $this->getJsonResponse(500);

                    //LOGIN STATUS
                    $response['loginStatus'] = 0;

                    //
                    $response['debug']['authUser'] = $authUser;
                    $response['debug']['param'] = $this->getAllParams();
                    $this->jsonResponse($response);
                    return;
                }
            }

            if($authUser){
                //SESSION
                $session = Zend_Registry::get($this->_registryName);
                $session->authUser = $authUser;

                //REDIRECT
                $redirectURL = $this->_baseUrl . '/';
                $_r          = $this->getParam('_r');
                if(!empty($_r) && substr($_r, 0, 1) == '/'){
                    $redirectURL = $_r;
                }

                //AJAX
                if ($isAjax) {
                    $response = $this->getJsonResponse(200);

                    //COOKIE
                    $cookieOption = array(
                        'secure'  => false,
                        'expires' => 1
                    );

                    if(isset($_SERVER['HTTPS']) && (strtolower($_SERVER['HTTPS']) == 'on')){
                        $cookieOption['secure'] = true;
                    }

                    $response['cookieOption'] = $cookieOption;
                    $response['cookieValue']  = json_encode($authUser);
                    $response['cookieName']   = $this->_loginCookieName;

                    //REDIRECT
                    $response['redirectURL'] = $redirectURL;

                    //LOGIN STATUS
                    $response['loginStatus'] = 1;

                    //for debugging
                    $response['debug']['authUser']  = $authUser;
                    $response['debug']['AllParams'] = $this->getAllParams();

                    //
                    $this->jsonResponse($response);
                    return;
                }

                //REDIRECT
                #$this->redirect($redirectURL);

            } //if($authUser)

        } //$httpRequest->isPost()
    }

    public function logoutAction(){
        //JS
        $customJS = array(
            $this->_assetUrl . '/js/login.js'
        );
        $this->loadCustomScript($customJS);

        //USE BOOTSTRAP LAYOUT
        $this->view->useBootstrapLayout = 1;

        //
        $_SERVER['PHP_AUTH_USER'] = null;
        $_SERVER['PHP_AUTH_PW']   = null;

        //CLEAR SESSION
        if(Zend_Registry::isRegistered($this->_registryName)){
            $session = Zend_Registry::get($this->_registryName);
            $session->authUser = null;
        }

        //CLEAR SESSION COOKIE
        $cookieOptions = array(
            'cookieName'  => $this->_loginCookieName,
            'cookieValue' => null,
            'expires'     => time() - (60*60*24*2) // - 2 days
        );
        #$this->setSessionCookie($cookieOptions);

        //CLEAR PHP SESSION
        session_destroy();

        //CLEAR LOGIN COOKIE
        $loginCookieOptions = $this->getLoginCookieOptions();
        $loginCookieParams  = array('cookieName' => $this->_loginCookieName, 'cookieOption' => $loginCookieOptions);
        $this->view->loginCookieParams = json_encode($loginCookieParams);
    }

    public function indexAction() {
        //VALIDATE
        $this->isUserAuthorized();

        // action body
    }

    public function advertistmentBuyAction(){
        //VALIDATE
        $authUser = $this->isUserAuthorized();

        //GET HTTP REQUEST
        $httpRequest = $this->getHttpRequest();

        //MODEL
        $mCustomerPackage = new CustomerPackage();
        $mAdvertistment = new Advertistment();

        //GET
        if($httpRequest->isGet()){
            //LIST
            $optionsAdvertistment = array(
                'debug' => 1
            );
            $resultAdvertistment = $mAdvertistment->getData($optionsAdvertistment);

            $listAdvertistment = array();
            if($resultAdvertistment){
                $listAdvertistment = $resultAdvertistment;
            }
            $this->view->listAdvertistment = $listAdvertistment;

            //LIST
            $optionsCustomerPackage = array(
                'debug' => 1
            );

            //FILTER: id
            $customer_id = isset($authUser['id']) ? $authUser['id'] : 0;
            $optionsCustomerPackage['where'][] = array('condition' => 'customer_id = ?', 'value' => $customer_id);

            $resultCustomerPackage = $mCustomerPackage->getData($optionsCustomerPackage);

            $listCsutomerPackage = array();
            if($resultCustomerPackage){
                $listCsutomerPackage = $resultCustomerPackage;
            }
            $this->view->listCsutomerPackage = $listCsutomerPackage;
        }

        //POST
    }

}
