<?php

/**
 * Description of ControlPanelController
 *
 * @author Kamarul Ariffin Ismail <kamarul.ismail@gmail.com>
 */
class ControlPanelController extends ZC_Controller_Action {
    protected $_loginCookieName = 'GDP_Admin';

    private $_configPrefix = 'controlPanel';
    private $_registryName = 'ControlPanelUser';
    private $_errorHandlerController = 'ControlPanel';

    public function init() {
        $_appSettings = array();
        if(Zend_Registry::isRegistered('appSettings')){
            $_appSettings = Zend_Registry::get('appSettings');
        }
        $this->_appSettings = $_appSettings;

        //DEFAULT LAYOUT
        $this->_layout = isset($_appSettings[$this->_configPrefix]['layout']) ? $_appSettings[$this->_configPrefix]['layout'] : 'main';

        //PREFIX URL
        $_prefixUrl       = isset($_appSettings['site']['prefixUrl']) ? $_appSettings['site']['prefixUrl'] : '/';
        $this->_prefixUrl = rtrim($_prefixUrl, '/') . '/';

        //BASE URL
        $_baseUrl       = isset($_appSettings[$this->_configPrefix]['baseUrl']) ? $_appSettings[$this->_configPrefix]['baseUrl'] : '/';
        $this->_baseUrl = rtrim($_prefixUrl, '/') . rtrim($_baseUrl, '/');

        //ASSET URL
        $_assetUrl       = isset($_appSettings[$this->_configPrefix]['assetUrl']) ? $_appSettings[$this->_configPrefix]['assetUrl'] : '/asset';
        $this->_assetUrl = rtrim($_prefixUrl, '/') . rtrim($_assetUrl, '/') ;

        //SITE NAME
        $this->_siteName = isset($_appSettings[$this->_configPrefix]['siteName']) ? $_appSettings[$this->_configPrefix]['siteName'] : 'SiteName';

        //CHANGE ERROR HANDLER
        $front        = Zend_Controller_Front::getInstance();
        $errorHandler = $front->getPlugin('Zend_Controller_Plugin_ErrorHandler');
        if(isset($front) && $errorHandler){
            $errorHandler->setErrorHandlerController($this->_errorHandlerController);
        }

        //SESSION
        // $registryName = $this->_registryName;
        // if(!Zend_Registry::isRegistered($registryName)){
        //     Zend_Registry::set($registryName, new Zend_Session_Namespace($registryName));
        // }

        //MAIN SIDEBAR
        $this->view->dataMainSidebar = 'Admin';

        //PAGE TITLE
        $this->_pageTitle = array('Control Panel');

        //EXCECUTE PARENT INIT()
        parent::init();
    }

    public function preDispatch() {
        parent::preDispatch();

        //DEBUGGING
        $this->debugPHPSession($this->_registryName);
    }

    private function isUserAuthorized(){
        //DEBUGGING
        $tableData = array(
            array('Variable', 'Data')
        );

        //DEFAULT
        $isUserAuthorized = 0;
        $authUser = null;

        //GET FROM SESSION
        $registryName = $this->_registryName;
        if(Zend_Registry::isRegistered($registryName)){
            $registryData = Zend_Registry::get($registryName);
            //DEBUGGING
            $tableData[] = array('$registryData', $registryData);

            //GET AUTH USER
            $authUser = isset($registryData->authUser) ? $registryData->authUser : array();
            //DEBUGGING
            $tableData[] = array('$authUser', $authUser);
        }

        //GET FROM COOKIE
        if (!$authUser) {
            $cookieName = isset($this->_loginCookieName) ? $this->_loginCookieName : 'HLBCookieName';
            $cookieData = $this->getSessionCookie($cookieName);
            //DEBUGGING
            $tableData[] = array("getSessionCookie({$cookieName})", $cookieData);

            //DECODE
            $cookieValue = base64_decode($cookieData);
            //DEBUGGING
            $tableData[] = array('base64_decode($cookieValue)', $cookieValue);

            if($cookieValue){
                $authUser = json_decode($cookieValue, true);
                //DEBUGGING
                $tableData[] = array('cookie.$authUser', $authUser);
            }
        }

        if($authUser){
            $id = isset($authUser['id']) ? $authUser['id'] : null;
            if(!empty($id)){
                $isUserAuthorized = 1;

                if(Zend_Registry::isRegistered($registryName)){
                    //REASSIGN TO SESSION
                    $registryData = Zend_Registry::get($registryName);
                    $registryData->authUser = $authUser;
                }
            }
        }

        //
        $this->view->authUser_ = $authUser;
        $this->view->isUserAuthorized = $isUserAuthorized;
        //DEBUGGING
        $tableData[] = array('$isUserAuthorized', $isUserAuthorized);

        //DEBUGGING
        $debugMode = (APPLICATION_ENV == 'development') ? 1 : 0;
        ZC_FirePHP::table($tableData, __CLASS__.'.'.__FUNCTION__, array('enable' => $debugMode));

        if(!$isUserAuthorized){
            $url = $this->_baseUrl . '/login';
            $this->redirect($url);
        }
    }

    private function getLoginCookieOptions(){
        $cookieOptions = array(
            'path'        => $this->_baseUrl,
            #'expires'     => 0, //browser-session cookie
            'secure'      => false
        );

        //SECURE
        if (isset($_SERVER['HTTPS']) && (strtolower($_SERVER['HTTPS']) == 'on')) {
            $cookieOptions['secure'] = true;
        }

        return $cookieOptions;
    }

    public function errorAction(){
        //PAGE TITLE
        // $this->_pageTitle = array('Error');
        $this->appendPageTitle(array('Error'));

        $errors = $this->getParam('error_handler');

        if (!$errors || !$errors instanceof ArrayObject) {
            $this->view->message = 'You have reached the error page';
            return;
        }

        switch ($errors->type) {
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
                // 404 error -- controller or action not found
                $httpResponseCode = 404;
                $this->getResponse()->setHttpResponseCode(404);
                $priority = Zend_Log::NOTICE;
                $this->view->message = 'Page not found';
                break;
            default:
                // application error
                $httpResponseCode = 500;
                $this->getResponse()->setHttpResponseCode(500);
                $priority = Zend_Log::CRIT;
                $this->view->message = 'Application error';
                break;
        }

        // conditionally display exceptions
        if ($this->getInvokeArg('displayExceptions') == true) {
            $this->view->exception = $errors->exception;
        }

        $this->view->request    = $errors->request;
        $this->view->pageHeader = $this->view->message;
        $this->view->httpResponseCode  = $httpResponseCode;
        $this->view->displayExceptions = $this->getInvokeArg('displayExceptions');
        $this->view->exception         = $errors->exception;
    }

    public function loginAction(){
        //JS
        $customJS = array(
            $this->_assetUrl . '/js/login.js'
        );
        $this->loadCustomScript($customJS);

        #Zend_Debug::dump($this->_debugSettings, '_debugSettings');
        #Zend_Debug::dump(ZC_FirePHP::isEnable(), 'isEnable');

        //USE BOOTSTRAP LAYOUT
        $this->view->useBootstrapLayout = 1;

        //PAGE TITLE
        $this->appendPageTitle(array('Login'));

        //POST
        $httpRequest = $this->getHttpRequest();
        if($httpRequest->isPost()){
            #$isAjax   = $this->getFilterParam('ajax', 0);

            //DEBUGGING
            $_functionName = __FUNCTION__;

            //CHECK IF AJAX
            $isAjax = $this->getFilterParam('ajax', 0);

            //LOG
            $logFile = APPLICATION_LOG_PATH . DIRECTORY_SEPARATOR . __CLASS__ . '-' . __FUNCTION__ . '-' . date('Ymd') . '.txt';

            //XSS: honeypot
            $honeypot = $this->getParam('honeypot', null);
            if (!empty($honeypot)) {
                $logData = "[{$_functionName}] Possible spam attack! (honeypot:{$honeypot})" . PHP_EOL . $this->getClientIP(1);
                #$this->phpErrorLogger($logData);
                #$this->fileLogger($logFile, $logData);

                $response = $this->getJsonResponse(200);
                $response['debug']['error_message'] = $logData;
                $this->jsonResponse($response);
                return;
            }

            //XSS: CSRF TOKEN
            $csrfToken = $this->getParam('csrf_token', null);
            if (empty($csrfToken)) {
                $logData = "[{$_functionName}] Possible spam attack! (csrf_token:{$csrfToken})" . PHP_EOL . $this->getClientIP(1);
                #$this->phpErrorLogger($logData);

                $response = $this->getJsonResponse(200);
                $response['debug']['error_message'] = $logData;
                $this->jsonResponse($response);
                return;
            }

            $csrfStatus = $this->verifyCSRFToken($csrfToken);
            if ($csrfStatus != 1) {
                $logData = "[{$_functionName}] Possible spam attack! (verifyCSRFToken({$csrfToken}))" . PHP_EOL . $this->getClientIP(1);
                #$this->phpErrorLogger($logData);
                #$this->fileLogger($logFile, $logData);

                $response = $this->getJsonResponse(200);
                $response['debug']['error_message'] = $logData;
                $this->jsonResponse($response);
                return;
            }

            //
            $username = $this->getFilterParam('login_name');
            $password = $this->getFilterParam('login_pass');

            if(empty($username) || empty($password)){
                //AJAX
                if ($isAjax) {
                    $response = $this->getJsonResponse(400);

                    //LOGIN STATUS
                    $response['loginStatus'] = 0;

                    //
                    $response['debug']['param'] = $this->getAllParams();
                    $this->jsonResponse($response);
                    return;
                }

                return;
            }

            //MODEL
            $optionLeadUser = array(
                'username' => $username,
                'password' => $password
            );
            $mAdministrator = new Administrator();
            $authUser  = $mAdministrator->validateUser($optionLeadUser);
            #ZC_FirePHP::warn($authUser, __CLASS__.'.'.__FUNCTION__.'.$authUser');

            if(!$authUser || empty($authUser)){
                //AJAX
                if ($isAjax) {
                    $response = $this->getJsonResponse(500);

                    //LOGIN STATUS
                    $response['loginStatus'] = 0;

                    //
                    $response['debug']['authUser'] = $authUser;
                    $response['debug']['param'] = $this->getAllParams();
                    $this->jsonResponse($response);
                    return;
                }
            }

            if($authUser){
                //SESSION
                $session = Zend_Registry::get($this->_registryName);
                $session->authUser = $authUser;

                //REDIRECT
                $redirectURL = $this->_baseUrl;
                $_r          = $this->getParam('_r');
                if(!empty($_r) && substr($_r, 0, 1) == '/'){
                    $redirectURL = $_r;
                }

                //AJAX
                if ($isAjax) {
                    $response = $this->getJsonResponse(200);

                    //COOKIE
                    $cookieOption = array(
                        'secure'  => false,
                        'expires' => 1
                    );

                    if(isset($_SERVER['HTTPS']) && (strtolower($_SERVER['HTTPS']) == 'on')){
                        $cookieOption['secure'] = true;
                    }

                    $response['cookieOption'] = $cookieOption;
                    $response['cookieValue']  = json_encode($authUser);
                    $response['cookieName']   = $this->_loginCookieName;

                    //REDIRECT
                    $response['redirectURL'] = $redirectURL;

                    //LOGIN STATUS
                    $response['loginStatus'] = 1;

                    //debug
                    $response['debug']['authUser'] = $authUser;
                    $response['debug']['param'] = $this->getAllParams();
                    $this->jsonResponse($response);
                    return;
                }

                //REDIRECT
                #$this->redirect($redirectURL);

            } //if($authUser)

        } //$httpRequest->isPost()
    }

    public function logoutAction(){
        //JS
        $customJS = array(
            $this->_assetUrl . '/js/login.js'
        );
        $this->loadCustomScript($customJS);

        //USE BOOTSTRAP LAYOUT
        $this->view->useBootstrapLayout = 1;

        //
        $_SERVER['PHP_AUTH_USER'] = null;
        $_SERVER['PHP_AUTH_PW']   = null;

        //CLEAR SESSION
        if(Zend_Registry::isRegistered($this->_registryName)){
            $session = Zend_Registry::get($this->_registryName);
            $session->authUser = null;
        }

        //CLEAR SESSION COOKIE
        $cookieOptions = array(
            'cookieName'  => $this->_loginCookieName,
            'cookieValue' => null,
            'expires'     => time() - (60*60*24*2) // - 2 days
        );
        #$this->setSessionCookie($cookieOptions);

        //CLEAR PHP SESSION
        session_destroy();

        //CLEAR LOGIN COOKIE
        $loginCookieOptions = $this->getLoginCookieOptions();
        $loginCookieParams  = array('cookieName' => $this->_loginCookieName, 'cookieOption' => $loginCookieOptions);
        $this->view->loginCookieParams = json_encode($loginCookieParams);
    }

    public function indexAction() {
        //VALIDATE
        $this->isUserAuthorized();

        // action body
    }

    public function redemptionAction() {
        //VALIDATE
        // $this->isUserAuthorized();

        // action body
    }

    /*ADMINISTRATOR*/
    public function administratorAction(){
        //VALIDATE
        $this->isUserAuthorized();

        //ENABLE DATATABLE
        $customCSS = array(
            $this->_assetUrl . '/plugins/datatables/dataTables.bootstrap.css'
        );
        $this->loadCustomStylesheet($customCSS);

        $customJS = array(
            $this->_assetUrl . '/plugins/datatables/jquery.dataTables.min.js',
            $this->_assetUrl . '/plugins/datatables/dataTables.bootstrap.min.js',
            $this->_assetUrl . '/js/administrator.js'
        );
        $this->loadCustomScript($customJS);

        //
        $httpRequest = $this->getHttpRequest();
        if($httpRequest->isPost()){

            //MODEL
            $mAdministrator = new Administrator();

            //CHECK IF DATATABLE REQUEST
            $isDataTableRequest = $this->getParam('isDataTableRequest', 0);

            if($isDataTableRequest){
                $response = $this->getJsonResponse(200);

                //DRAW - Draw counter
                $draw = $this->getParam('draw', 0);

                //START - Paging first record indicator.
                $start = $this->getParam('start', 0);

                //LENGTH - Number of records that the table can display in the current draw.
                $length = $this->getParam('length', 15);

                //
                $optionsDatatable = array(
                    'param_datatable' => $this->getAllParams(),
                    'param_custom' => 'x'

                );
                $result = $mAdministrator->datatableRequest($optionsDatatable);

                //
                $this->jsonResponse($result);
                exit();
            }
        }
    }

    public function administratorUpdateAction(){
        //VALIDATE
        $this->isUserAuthorized();

        //
        $customJS = array(
            $this->_assetUrl . '/js/administrator.js'
        );
        $this->loadCustomScript($customJS);

        //GET HTTP REQUEST
        $httpRequest = $this->getHttpRequest();

        //GET
        if($httpRequest->isGet()){
            //PARAM
            $id = $this->getParam('id');

            if(is_null($id)){
                return;
            }

            //MODEL
            $mAdministrator = new Administrator();

            //OPTIONS
            $optionsData = array(
                'debug' => 1,
                'fetchType' => 'fetchRow'
            );

            //FILTER: id
            $optionsData['where'][] = array('condition' => 'id = ?', 'value' => $id);
            //
            $result = $mAdministrator->getData($optionsData);

            // for debugging
            #Zend_Debug::dump($result);

            //
            $formData = array();
            if($result){
                $formData = $result;
            }
            $this->view->formData = $formData;
            return;
        }

        //POST
        if($httpRequest->isPost()){
            //DEBUGGING
            $_functionName = __FUNCTION__;

            //CHECK IF AJAX
            $isAjax = $this->getParam('ajax', 0);

            //LOG
            $logFile = APPLICATION_LOG_PATH . DIRECTORY_SEPARATOR . __CLASS__ . '-' . __FUNCTION__ . '-'.date('Ymd').'.txt';

            //XSS: honeypot
            $honeypot = $this->getParam('honeypot', null);
            if (!empty($honeypot)) {
                $logData = "[{$_functionName}] Possible spam attack! (honeypot:{$honeypot})" . PHP_EOL . $this->getClientIP(1);
                #$this->phpErrorLogger($logData);
                #$this->fileLogger($logFile, $logData);

                $response = $this->getJsonResponse(200);
                $response['debug']['error_message'] = $logData;
                $this->jsonResponse($response);
                return;
            }

            //XSS: CSRF TOKEN
            $csrfToken = $this->getParam('csrf_token', null);
            if (empty($csrfToken)) {
                $logData = "[{$_functionName}] Possible spam attack! (csrf_token:{$csrfToken})" . PHP_EOL . $this->getClientIP(1);
                #$this->phpErrorLogger($logData);

                $response = $this->getJsonResponse(200);
                $response['debug']['error_message'] = $logData;
                $this->jsonResponse($response);
                return;
            }

            $csrfStatus = $this->verifyCSRFToken($csrfToken);
            if ($csrfStatus != 1) {
                $logData = "[{$_functionName}] Possible spam attack! (verifyCSRFToken({$csrfToken}))" . PHP_EOL . $this->getClientIP(1);
                #$this->phpErrorLogger($logData);
                #$this->fileLogger($logFile, $logData);

                $response = $this->getJsonResponse(200);
                $response['debug']['error_message'] = $logData;
                $this->jsonResponse($response);
                return;
            }

            //
            $response = $this->getJsonResponse(200);

            //MODEL
            $mAdministrator = new Administrator();

            //
            $optionsSave = array(
                'data'     => $this->getParam('data'),
                #'password' => $this->getParam('password-new')
            );

            //
            $result = $mAdministrator->saveData($optionsSave);

            //debugging
            $response['debug']['Administrator'] = $result;

            //FLAG
            $isSaved = 0;
            if($result){
                $isSaved = 1;
            }
            $response['isSaved'] = $isSaved;

            //
            $this->jsonResponse($response);
            return;
        }
    }

    /*CUSTOMER*/
    public function customerAction(){
        //VALIDATE
        $this->isUserAuthorized();

        //ENABLE DATATABLE
        $customCSS = array(
            $this->_assetUrl . '/plugins/datatables/dataTables.bootstrap.css'
        );
        $this->loadCustomStylesheet($customCSS);

        $customJS = array(
            $this->_assetUrl . '/plugins/datatables/jquery.dataTables.min.js',
            $this->_assetUrl . '/plugins/datatables/dataTables.bootstrap.min.js',
            $this->_assetUrl . '/js/customer.js'
        );
        $this->loadCustomScript($customJS);

        //
        $httpRequest = $this->getHttpRequest();
        if($httpRequest->isPost()){

            //MODEL
            $mCustomer = new Customer();

            //CHECK IF DATATABLE REQUEST
            $isDataTableRequest = $this->getParam('isDataTableRequest', 0);

            if($isDataTableRequest){
                $response = $this->getJsonResponse(200);

                //DRAW - Draw counter
                $draw = $this->getParam('draw', 0);

                //START - Paging first record indicator.
                $start = $this->getParam('start', 0);

                //LENGTH - Number of records that the table can display in the current draw.
                $length = $this->getParam('length', 15);

                //
                $optionsDatatable = array(
                    'param_datatable' => $this->getAllParams(),
                    'param_custom' => 'x'

                );
                $result = $mCustomer->datatableRequest($optionsDatatable);

                //
                $this->jsonResponse($result);
                exit();
            }
        }
    }

    public function customerUpdateAction(){
        //VALIDATE
        $this->isUserAuthorized();

        //
        $customJS = array(
            $this->_assetUrl . '/js/customer.js'
        );
        $this->loadCustomScript($customJS);

        //GET HTTP REQUEST
        $httpRequest = $this->getHttpRequest();

        //GET
        if($httpRequest->isGet()){
            //PARAM
            $id = $this->getParam('id');

            if(is_null($id)){
                return;
            }

            //MODEL
            $mCustomer = new Customer();

            //OPTIONS
            $optionsData = array(
                'debug' => 1,
                'fetchType' => 'fetchRow'
            );

            //FILTER: id
            $optionsData['where'][] = array('condition' => 'id = ?', 'value' => $id);
            //
            $result = $mCustomer->getData($optionsData);

            // for debugging
            #Zend_Debug::dump($result);

            //
            $formData = array();
            if($result){
                $formData = $result;
            }
            $this->view->formData = $formData;
            return;
        }

        //POST
        if($httpRequest->isPost()){
            //DEBUGGING
            $_functionName = __FUNCTION__;

            //CHECK IF AJAX
            $isAjax = $this->getParam('ajax', 0);

            //LOG
            $logFile = APPLICATION_LOG_PATH . DIRECTORY_SEPARATOR . __CLASS__ . '-' . __FUNCTION__ . '-'.date('Ymd').'.txt';

            //XSS: honeypot
            $honeypot = $this->getParam('honeypot', null);
            if (!empty($honeypot)) {
                $logData = "[{$_functionName}] Possible spam attack! (honeypot:{$honeypot})" . PHP_EOL . $this->getClientIP(1);
                #$this->phpErrorLogger($logData);
                #$this->fileLogger($logFile, $logData);

                $response = $this->getJsonResponse(200);
                $response['debug']['error_message'] = $logData;
                $this->jsonResponse($response);
                return;
            }

            //XSS: CSRF TOKEN
            $csrfToken = $this->getParam('csrf_token', null);
            if (empty($csrfToken)) {
                $logData = "[{$_functionName}] Possible spam attack! (csrf_token:{$csrfToken})" . PHP_EOL . $this->getClientIP(1);
                #$this->phpErrorLogger($logData);

                $response = $this->getJsonResponse(200);
                $response['debug']['error_message'] = $logData;
                $this->jsonResponse($response);
                return;
            }

            $csrfStatus = $this->verifyCSRFToken($csrfToken);
            if ($csrfStatus != 1) {
                $logData = "[{$_functionName}] Possible spam attack! (verifyCSRFToken({$csrfToken}))" . PHP_EOL . $this->getClientIP(1);
                #$this->phpErrorLogger($logData);
                #$this->fileLogger($logFile, $logData);

                $response = $this->getJsonResponse(200);
                $response['debug']['error_message'] = $logData;
                $this->jsonResponse($response);
                return;
            }

            //
            $response = $this->getJsonResponse(200);

            //MODEL
            $mCustomer = new Customer();

            //
            $optionsSave = array(
                'data'     => $this->getParam('data'),
                #'password' => $this->getParam('password-new')
            );

            //
            $result = $mCustomer->saveData($optionsSave);

            //debugging
            $response['debug']['Customer'] = $result;

            //FLAG
            $isSaved = 0;
            if($result){
                $isSaved = 1;
            }
            $response['isSaved'] = $isSaved;

            //
            $this->jsonResponse($response);
            return;
        }
    }

    /*CUSTOMER PACKAGE*/
    public function customerPackageAction(){
        //VALIDATE
        $this->isUserAuthorized();

        //ENABLE DATATABLE
        $customCSS = array(
            $this->_assetUrl . '/plugins/datatables/dataTables.bootstrap.css'
        );
        $this->loadCustomStylesheet($customCSS);

        $customJS = array(
            $this->_assetUrl . '/plugins/datatables/jquery.dataTables.min.js',
            $this->_assetUrl . '/plugins/datatables/dataTables.bootstrap.min.js',
            $this->_assetUrl . '/js/customer-package.js'
        );
        $this->loadCustomScript($customJS);

        //MODEL
        $mCustomerPackage = new CustomerPackage();

        //
        $httpRequest = $this->getHttpRequest();
        if($httpRequest->isPost()){

            //CHECK IF DATATABLE REQUEST
            $isDataTableRequest = $this->getParam('isDataTableRequest', 0);

            if($isDataTableRequest){
                $response = $this->getJsonResponse(200);

                //DRAW - Draw counter
                $draw = $this->getParam('draw', 0);

                //START - Paging first record indicator.
                $start = $this->getParam('start', 0);

                //LENGTH - Number of records that the table can display in the current draw.
                $length = $this->getParam('length', 15);

                //
                $optionsDatatable = array(
                    'param_datatable' => $this->getAllParams(),
                    'param_custom' => 'x'

                );
                $result = $mCustomerPackage->datatableRequest($optionsDatatable);

                //
                $this->jsonResponse($result);
                exit();
            }
        }
    }

    public function customerPackageUpdateAction(){
        //VALIDATE
        $this->isUserAuthorized();

        //
        $customJS = array(
            $this->_assetUrl . '/js/customer-package.js'
        );
        $this->loadCustomScript($customJS);

        //GET HTTP REQUEST
        $httpRequest = $this->getHttpRequest();

        //MODEL
        $mCustomerPackage = new CustomerPackage();
        $mCustomer = new Customer();
        $mAdvertistment = new Advertistment();

        //GET
        if($httpRequest->isGet()){
            //CUSTOMER LISTING
            $optionsCustomerData = array(
                'debug' => 1
            );
            $resultCustomer = $mCustomer->getData($optionsCustomerData);

            $listCustomer = array();
            if($resultCustomer){
                $listCustomer = $resultCustomer;
            }
            $this->view->listCustomer = $listCustomer;

            //LIST
            $optionsDataAdvertistment = array(
                'debug' => 1
            );
            $resultAdvertistment = $mAdvertistment->getData($optionsDataAdvertistment);

            $listAdvertistment = array();
            if($resultAdvertistment){
                $listAdvertistment = $resultAdvertistment;
            }
            $this->view->listAdvertistment = $listAdvertistment;

            //LIST
            $this->view->listPackageType = $mCustomerPackage->getListPackageType();

            //PARAM
            $id = $this->getParam('id');

            if(is_null($id)){
                return;
            }

            //OPTIONS
            $optionsData = array(
                'debug' => 1,
                'fetchType' => 'fetchRow'
            );

            //FILTER: id
            $optionsData['where'][] = array('condition' => 'id = ?', 'value' => $id);
            //
            $result = $mCustomerPackage->getData($optionsData);

            // for debugging
            //Zend_Debug::dump($result);

            //
            $formData = array();
            if($result){
                $formData = $result;
            }
            $this->view->formData = $formData;
            return;
        }

        //POST
        if($httpRequest->isPost()){
            //DEBUGGING
            $_functionName = __FUNCTION__;

            //CHECK IF AJAX
            $isAjax = $this->getParam('ajax', 0);

            //LOG
            $logFile = APPLICATION_LOG_PATH . DIRECTORY_SEPARATOR . __CLASS__ . '-' . __FUNCTION__ . '-'.date('Ymd').'.txt';

            //XSS: honeypot
            $honeypot = $this->getParam('honeypot', null);
            if (!empty($honeypot)) {
                $logData = "[{$_functionName}] Possible spam attack! (honeypot:{$honeypot})" . PHP_EOL . $this->getClientIP(1);
                #$this->phpErrorLogger($logData);
                #$this->fileLogger($logFile, $logData);

                $response = $this->getJsonResponse(200);
                $response['debug']['error_message'] = $logData;
                $this->jsonResponse($response);
                return;
            }

            //XSS: CSRF TOKEN
            $csrfToken = $this->getParam('csrf_token', null);
            if (empty($csrfToken)) {
                $logData = "[{$_functionName}] Possible spam attack! (csrf_token:{$csrfToken})" . PHP_EOL . $this->getClientIP(1);
                #$this->phpErrorLogger($logData);

                $response = $this->getJsonResponse(200);
                $response['debug']['error_message'] = $logData;
                $this->jsonResponse($response);
                return;
            }

            $csrfStatus = $this->verifyCSRFToken($csrfToken);
            if ($csrfStatus != 1) {
                $logData = "[{$_functionName}] Possible spam attack! (verifyCSRFToken({$csrfToken}))" . PHP_EOL . $this->getClientIP(1);
                #$this->phpErrorLogger($logData);
                #$this->fileLogger($logFile, $logData);

                $response = $this->getJsonResponse(200);
                $response['debug']['error_message'] = $logData;
                $this->jsonResponse($response);
                return;
            }

            //
            $response = $this->getJsonResponse(200);

            //
            $data = $this->getParam('data');

            //
            $package_rules = $this->getParam('package_rules', array());
            $data['package_rules'] = json_encode($package_rules);

            //debugging
            $response['debug']['data'] = $data;

            //
            $optionsSave = array(
                'data' => $data,
            );

            //
            $result = $mCustomerPackage->saveData($optionsSave);

            //debugging
            $response['debug']['CustomerPackage'] = $result;

            //FLAG
            $isSaved = 0;
            if($result){
                $isSaved = 1;
            }
            $response['isSaved'] = $isSaved;

            //
            $this->jsonResponse($response);
            return;
        }
    }

    /*ADVERTISTMENT*/
    public function advertistmentAction(){
        //VALIDATE
        $this->isUserAuthorized();

        //ENABLE DATATABLE
        $customCSS = array(
            $this->_assetUrl . '/plugins/datatables/dataTables.bootstrap.css'
        );
        $this->loadCustomStylesheet($customCSS);

        $customJS = array(
            $this->_assetUrl . '/plugins/datatables/jquery.dataTables.min.js',
            $this->_assetUrl . '/plugins/datatables/dataTables.bootstrap.min.js',
            $this->_assetUrl . '/js/advertistment.js'
        );
        $this->loadCustomScript($customJS);

        //
        $httpRequest = $this->getHttpRequest();
        if($httpRequest->isPost()){

            //MODEL
            $mAdvertistment = new Advertistment();

            //CHECK IF DATATABLE REQUEST
            $isDataTableRequest = $this->getParam('isDataTableRequest', 0);

            if($isDataTableRequest){
                $response = $this->getJsonResponse(200);

                //DRAW - Draw counter
                $draw = $this->getParam('draw', 0);

                //START - Paging first record indicator.
                $start = $this->getParam('start', 0);

                //LENGTH - Number of records that the table can display in the current draw.
                $length = $this->getParam('length', 15);

                //
                $optionsDatatable = array(
                    'param_datatable' => $this->getAllParams(),
                    'param_custom' => 'x'

                );
                $result = $mAdvertistment->datatableRequest($optionsDatatable);

                //
                $this->jsonResponse($result);
                exit();
            }
        }
    }

    public function advertistmentUpdateAction(){
        //VALIDATE
        $this->isUserAuthorized();

        //
        $customJS = array(
            $this->_assetUrl . '/js/advertistment.js'
        );
        $this->loadCustomScript($customJS);

        //MODEL
        $mAdvertistment = new Advertistment();

        //GET HTTP REQUEST
        $httpRequest = $this->getHttpRequest();

        //GET
        if($httpRequest->isGet()){
            //LIST
            $this->view->listAdsType = $mAdvertistment->getListAdsType();

            //PARAM
            $id = $this->getParam('id');

            if(is_null($id)){
                return;
            }

            //OPTIONS
            $optionsData = array(
                'debug' => 1,
                'fetchType' => 'fetchRow'
            );

            //FILTER: id
            $optionsData['where'][] = array('condition' => 'id = ?', 'value' => $id);
            //
            $result = $mAdvertistment->getData($optionsData);

            // for debugging
            #Zend_Debug::dump($result);

            //
            $formData = array();
            if($result){
                $formData = $result;
            }
            $this->view->formData = $formData;
            return;
        }

        //POST
        if($httpRequest->isPost()){
            //DEBUGGING
            $_functionName = __FUNCTION__;

            //CHECK IF AJAX
            $isAjax = $this->getParam('ajax', 0);

            //LOG
            $logFile = APPLICATION_LOG_PATH . DIRECTORY_SEPARATOR . __CLASS__ . '-' . __FUNCTION__ . '-'.date('Ymd').'.txt';

            //XSS: honeypot
            $honeypot = $this->getParam('honeypot', null);
            if (!empty($honeypot)) {
                $logData = "[{$_functionName}] Possible spam attack! (honeypot:{$honeypot})" . PHP_EOL . $this->getClientIP(1);
                #$this->phpErrorLogger($logData);
                #$this->fileLogger($logFile, $logData);

                $response = $this->getJsonResponse(200);
                $response['debug']['error_message'] = $logData;
                $this->jsonResponse($response);
                return;
            }

            //XSS: CSRF TOKEN
            $csrfToken = $this->getParam('csrf_token', null);
            if (empty($csrfToken)) {
                $logData = "[{$_functionName}] Possible spam attack! (csrf_token:{$csrfToken})" . PHP_EOL . $this->getClientIP(1);
                #$this->phpErrorLogger($logData);

                $response = $this->getJsonResponse(200);
                $response['debug']['error_message'] = $logData;
                $this->jsonResponse($response);
                return;
            }

            $csrfStatus = $this->verifyCSRFToken($csrfToken);
            if ($csrfStatus != 1) {
                $logData = "[{$_functionName}] Possible spam attack! (verifyCSRFToken({$csrfToken}))" . PHP_EOL . $this->getClientIP(1);
                #$this->phpErrorLogger($logData);
                #$this->fileLogger($logFile, $logData);

                $response = $this->getJsonResponse(200);
                $response['debug']['error_message'] = $logData;
                $this->jsonResponse($response);
                return;
            }

            //
            $response = $this->getJsonResponse(200);

            //
            $optionsSave = array(
                'data'     => $this->getParam('data'),
                #'password' => $this->getParam('password-new')
            );

            //
            $result = $mAdvertistment->saveData($optionsSave);

            //debugging
            $response['debug']['Advertistment'] = $result;

            //FLAG
            $isSaved = 0;
            if($result){
                $isSaved = 1;
            }
            $response['isSaved'] = $isSaved;

            //
            $this->jsonResponse($response);
            return;
        }
    }
}
