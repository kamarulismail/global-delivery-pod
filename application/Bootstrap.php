<?php
/**
 * Description of Bootstrap
 *
 * @author Kamarul Ariffin Ismail <kamarul.ismail@gmail.com>
 */
require_once('ZC/Bootstrap.php');
class Bootstrap extends ZC_Bootstrap {

    protected function _initZCBootstrap() {
        parent::init();

        //SET DEFAULT CHARSET
        ini_set('default_charset', 'UTF-8');
    }

    protected function _initRegisterySession(){
        // MAIN - USER
        Zend_Registry::set('MainUser', new Zend_Session_Namespace('MainUser'));

        // CONTROL PANEL - USER
        Zend_Registry::set('ControlPanelUser', new Zend_Session_Namespace('ControlPanelUser'));
    }

    protected function _initRouter(){
        // GET DEBUGGING SETTINGS
        $debugSettings = isset($this->_debugSettings) ? $this->_debugSettings : array();
        if(empty($debugSettings)){
            $debugSettings = $this->getApplication()->getOption('debug');
        }
        $debugMode = isset($debugSettings['initRouter']['enable']) ? $debugSettings['initRouter']['enable'] : 0;

        //
        #$routerFile     = APPLICATION_PATH . '/configs/router.ini';
        $routerFile     = APPLICATION_CONFIG_PATH . DIRECTORY_SEPARATOR . 'router.ini';
        $routerSettings = @parse_ini_file($routerFile, true);
        if($routerSettings == FALSE || empty($routerSettings)){
            ZC_FirePHP::error("Invalid Router File: {$routerFile}",__CLASS__.'.'.__FUNCTION__,$debugMode);
            return;
            #header('Location: setup/error-router.php');
            #exit();
        }


        //GET SETTINGS
        $appSettings = isset($this->_appSettings) ? $this->_appSettings : $this->getApplication()->getOption('appSettings');
        $front       = Zend_Controller_Front::getInstance();
        $router      = $front->getRouter();
        $prefixUrl   = isset($appSettings['site']['prefixUrl']) ? $appSettings['site']['prefixUrl'] : '/';

        //debugging
        ZC_FirePHP::groupCollapsed(__CLASS__.'.'.__FUNCTION__, array('enable' => $debugMode));
        $_tableData = array(
            array('Variable', 'Data'),
            array('routerSettings', $routerSettings),
            array('router', $router),
            array('get_class(router)', get_class($router)),
            array('router', $router->getRoutes()),
            array('prefixUrl', $prefixUrl),
        );
        
        // remove any default routes
        #$router->removeDefaultRoutes();

        //loop through sections
        foreach($routerSettings as $routerSection => $routerConfig){
            //debugging
            $tableData = array(
                array('Variable', 'Data'),
                array('routerSection', $routerSection),
            );
            
            //loading ini file
            $routerConfig = new Zend_Config_Ini($routerFile, $routerSection, array('allowModifications' => true));
            //debugging
            $tableData[] = array('routerConfig', $routerConfig->toArray());

            //BASE URL
            $baseUrl = isset($appSettings['main']['baseUrl']) ? $appSettings['main']['baseUrl'] : '/';
            if(!in_array($routerSection, array('main'))){
                $_baseUrl = isset($appSettings[$routerSection]['baseUrl']) ? $appSettings[$routerSection]['baseUrl'] : '/' . $routerSection;
                $urlPath  = parse_url($_baseUrl, PHP_URL_PATH);
                $baseUrl  = rtrim($urlPath, '/') . '/';
            }
            $baseUrl = rtrim($prefixUrl, '/') . '/' . ltrim($baseUrl, '/');
            //debugging
            $tableData[] = array('$baseUrl', $baseUrl);

            // GENERATE NEW ROUTE
            $newRoutes = array();
            foreach ($routerConfig as $key => $route) {
                if (strtolower($key) == 'index') {
                    $newRoute = $baseUrl;
                }
                else {
                    $newRoute = $baseUrl . ltrim($route->route, '/');
                }
                $route->route = $newRoute;

                // GENERATE NEW ROUTE KEY
                $routeKey = str_replace('-', ' ', $routerSection);
                $routeKey = str_replace(' ', '', ucwords($routeKey)) . ucfirst($key);
                $newRoutes[$routeKey] = $route;
                //debugging
                $tableData[] = array("newRoute[{$routeKey}]", $newRoute);
            }

            //CREATE NEW CONFIG
            $newRouteConfig = new Zend_Config($newRoutes);
            $router->addConfig($newRouteConfig);

            //debugging
            $tableData[] = array('newRouteConfig', $newRouteConfig);
            $tableData[] = array('newRouteConfig', $newRouteConfig->toArray());
            ZC_FirePHP::table($tableData, "routerSection[{$routerSection}]", array('enable' => $debugMode));
        }

        //debugging
        $_tableData[] = array('router.final', $router);
        $_tableData[] = array('router.final', $router->getRoutes());
        ZC_FirePHP::groupEnd($debugMode);
    }

    protected function _initCacheManager() {
        // GET DEBUGGING SETTINGS
        $debugSettings = isset($this->_debugSettings) ? $this->_debugSettings : null;
        if(empty($debugSettings)){
            $debugSettings = $this->getApplication()->getOption('debug');
        }

        //
        $debugMode = isset($debugSettings['cache']['enable']) ? $debugSettings['cache']['enable'] : 0;
        $debugMode = ($debugMode == 2) ? 1 : 0;

        #http://framework.zend.com/manual/1.12/en/zend.cache.cache.manager.html
        #http://framework.zend.com/manual/1.12/en/zend.cache.backends.html
        ZC_FirePHP::groupCollapsed(__CLASS__.'.'.__FUNCTION__, array('enable' => $debugMode));
        $appSettings = $this->getApplication()->getOption('appSettings');
        $_isEnable   = isset($appSettings['cache']['enable']) ? $appSettings['cache']['enable'] : 0;

        $iniFile       = APPLICATION_PATH . '/configs/cache.ini';
        $cacheSettings = @parse_ini_file($iniFile, true);
        if($cacheSettings == FALSE || empty($cacheSettings)){
            return;
        }

        //
        $defaultCacheFolder = rtrim(sys_get_temp_dir(), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . 'hlbMain';
        if(!file_exists($defaultCacheFolder)){
            @mkdir($defaultCacheFolder, 0777, true);
        }

        //
        $baseSetting = isset($cacheSettings['base']) ? $cacheSettings['base'] : array();
        $cacheFolder = isset($baseSetting['cache_dir']) ? $baseSetting['cache_dir'] : realpath(APPLICATION_PATH . DIRECTORY_SEPARATOR . '..') . DIRECTORY_SEPARATOR . 'caches';
        if(!file_exists($cacheFolder)){
            mkdir($cacheFolder, 0777, true);
        }

        //CHECK CACHE FOLDER
        $isCacheFolderWritable = is_writable($cacheFolder);
        ZC_FirePHP::warn($isCacheFolderWritable, "isCacheFolderWritable({$cacheFolder})", $debugMode);

        $zendResourceCacheManager = new Zend_Application_Resource_Cachemanager();
        $zendCacheManager         = $zendResourceCacheManager->getCacheManager();

        //LOOP EXISTING CACHE
        $listCache = $zendCacheManager->getCaches();
        foreach($listCache as $cacheName => $cacheConfig){
            $backend = $cacheConfig->getBackend();
            $backend->setOption('cache_dir', $defaultCacheFolder);
            if($isCacheFolderWritable){
                $backend->setOption('cache_dir', $cacheFolder);
            }
        }

        //ADD NEW CACHE
        foreach($cacheSettings as $cacheSection => $cacheSetting){
            if($cacheSection == 'base'){
                continue;
            }

            ZC_FirePHP::groupCollapsed("section({$cacheSection})", array('enable' => $debugMode));

            $cacheSection  = str_replace(':base', '', $cacheSection);
            $zendConfigIni = new Zend_Config_Ini($iniFile, $cacheSection);
            $cacheSetting  = $zendConfigIni->toArray();
            ZC_FirePHP::info($cacheSetting, 'cacheSetting', $debugMode);

            //GLOBAL SETTINGS
            if($_isEnable == 0){
                $cacheSetting['frontend']['options']['caching'] = 0;
                ZC_FirePHP::warn('DISABLED', 'Cache Status', $debugMode);
            }

            //CACHE DIR
            $cacheSetting['backend']['options']['cache_dir'] = $defaultCacheFolder;
            if($isCacheFolderWritable){
                $cacheSetting['backend']['options']['cache_dir'] = $cacheFolder;
            }

            //
            $zendCacheManager->setCacheTemplate($cacheSection, $cacheSetting);
            ZC_FirePHP::groupEnd($debugMode);
        }

        //
        if($debugMode){
            foreach($listCache as $cacheName => $cacheConfig){
                ZC_FirePHP::info($cacheConfig, "cacheName({$cacheName})", $debugMode);
            }
        }

        //
        Zend_Registry::set('zendCacheManager', $zendCacheManager);
        $this->registerPluginResource($zendResourceCacheManager);
        ZC_FirePHP::groupEnd($debugMode);
    }

    protected function _initDbSettings(){
        // GET DEBUGGING SETTINGS
        $debugSettings = isset($this->_debugSettings) ? $this->_debugSettings : null;
        if(empty($debugSettings)){
            $debugSettings = $this->getApplication()->getOption('debug');
        }

        //
        $_enable = isset($debugSettings['dbSettings']['enable']) ? $debugSettings['dbSettings']['enable'] : 0;
        try {
            //DEBUGGING
            $tableData = array(
                array('Variable', 'Data')
            );

            //GET OPTIONS FROM application.ini
            $applicationOptions = $this->getApplication()->getOptions();
            //DEBUGGING
            #$tableData[] = array('$applicationOptions', $applicationOptions);

            //DEFAULT
            $dbOptionsDefault = array(
                'adapter' => 'pdo_mysql',
                'params'  => array(
                    'host' => 'localhost',
                    'username' => 'dbusername',
                    'password' => 'dbpassword',
                    'dbname'   => 'dbname'
                )
            );

            //db.*
            $dbOptions = isset($applicationOptions['db']) ? $applicationOptions['db'] : $dbOptionsDefault;
            //DEBUGGING
            $tableData[] = array('$dbOptions', $dbOptions);

            //application-db.ini
            $dbIniFile = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'configs' . DIRECTORY_SEPARATOR . 'application-db.ini';
            if(is_readable($dbIniFile)){
                $dbIniSettings = new Zend_Config_Ini($dbIniFile, 'settings');
                $dbIniData     = $dbIniSettings->toArray();

                //DEBUGGING
                $tableData[] = array('$dbIniData', $dbIniData);

                //SET
                $dbOptions = isset($dbIniData['db']) ? $dbIniData['db'] : $dbOptions;

                //CLEAR MEMORY
                $dbIniSettings = null;
            }

            //db.params.*
            $dbParams = isset($dbOptions['params']) ? $dbOptions['params'] : array();
            //DEBUGGING
            $tableData[] = array('$dbParams', $dbParams);

            //db.params.password_encrypted
            $password_encrypted = isset($dbParams['password_encrypted']) ? $dbParams['password_encrypted'] : '';
            if (!empty($password_encrypted)) {
                $crypto = new ZC_Crypto();
                $password_decrypted = $crypto->decrypt($password_encrypted);

                //
                $dbOptions['params']['password'] = $password_decrypted;

                //DEBUGGING
                $tableData[] = array('ZC_Crypto', array('$password_encrypted' => $password_encrypted, '$password_decrypted' => $password_decrypted));
            }

            //CREATE NEW RESOURCE
            $resourceDB = new Zend_Application_Resource_Db($dbOptions);
            //DEBUGGING
            $tableData[] = array('$resourceDB->getOptions()', $resourceDB->getOptions());

            //REGISTER PLUGIN
            $this->registerPluginResource($resourceDB, $dbOptions);
            //DEBUGGING
            $tableData[] = array('$this->getPluginResourceNames()', $this->getPluginResourceNames());

            //DEBUGGING
            ZC_FirePHP::table($tableData, __CLASS__ . '.' . __FUNCTION__, array('enable' => $_enable));
        }
        catch(Exception $e){
            header('HTTP/1.0 500 Internal Server Error');
            echo '500 Internal Server Error - Please Check Database Configuration';
            echo '<p>'.$e->getMessage().'</p>';
            exit();
        }
    }

}