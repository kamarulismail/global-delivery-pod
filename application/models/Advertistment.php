<?php

/**
 * Description of Advertistment class
 *
 * @author Kamarul Ariffin Ismail <kamarul.ismail@gmail.com>
 */

class Advertistment extends BaseModel {
    //DB TABLE NAME
    protected $_name = 'advertisment';

    const ADS_TYPE_CLASSIC = 'classic';
    const ADS_TYPE_STANDOUT = 'standout';
    const ADS_TYPE_PREMIUM = 'premium';

    public function init() {
        parent::init();
    }

    public function update(array $data, $where){
        if(!isset($data['updated'])){
            $data['updated'] = date('Y-m-d H:i:s');
        }

        $result = parent::update($data, $where);

        return $result;
    }

    public function saveData($options = array()){
        //DATA
        $data = isset($options['data']) ? $options['data'] : array();

        //ID
        $id = isset($data['id']) ? $data['id'] : 0;

        //FLAG
        $isUpdate = 0;

        //
        $model = $this->fetchRow(array('id = ?' => $id));
        if(!$model){
            unset($data['id']);
            //CREATE NEW MODEL
            $model = $this->createRow();

            //CREATED
            $model['created'] = date('Y-m-d H:i:s');

            //
            $model['is_active'] = 1;
        }
        else{
            //UPDATED
            $model['updated'] = date('Y-m-d H:i:s');

            //FLAG
            $isUpdate = 1;

            //
            unset($data['id']);
        }

        $model->setFromArray($data);

        //
        // $arrExludeColumn = array('id');
        // foreach($data as $dataColumn => $dataValue){
        //     if(in_array($dataColumn, $arrExludeColumn)){
        //         continue;
        //     }
        //     $model[$dataColumn] = $dataValue;
        // }

        try {
            $pKey = $model->save();
        }
        catch (Exception $ex) {
            $pKey = null;
        }

        return $pKey;
    }

    public function datatableRequest($options = array()){
        //
        $paramDatatable = isset($options['param_datatable']) ? $options['param_datatable'] : array();

        //
        $resultDataTable = array(
            //DRAW - Draw counter
            'draw' => isset($paramDatatable['draw']) ? (int)$paramDatatable['draw'] : 0,

            //Total records, before filtering (i.e. the total number of records in the database)
            'recordsTotal' => 0,

            //Total records, after filtering (i.e. the total number of records after filtering has been applied - not just the number of records being returned for this page of data).
            'recordsFiltered' => 0,

            //The data to be displayed in the table. This is an array of data source objects, one for each row, which will be used by DataTables.
            'data' => array(),

            //Optional: If an error occurs during the running of the server-side processing script, you can inform the user of this error by passing back the error message to be displayed using this parameter. Do not include if there is no error.
            #'error' => ''
        );

        //MAIN QUERY
        $optionsData = array(
            'debug' => 1
        );

        //LENGTH - Number of records that the table can display in the current draw.
        $length = isset($paramDatatable['length']) ? $paramDatatable['length'] : 0;
        if($length){
            $optionsData['limit']['count'] = $length;
        }

        //START - Paging first record indicator.
        $start = isset($paramDatatable['start']) ? $paramDatatable['start'] : 0;
        if($start){
            $optionsData['limit']['offset'] = $start;
        }

        //
        $result = $this->getData($optionsData);

        //
        if($result){
            //[data]
            $resultDataTable['data'] = $result;

            //[recordsFiltered]
            #$resultDataTable['recordsFiltered'] = count($result);
        }

        //[recordsTotal]
        $optionsData['getRowCount'] = 1;
        $rowCount = $this->getData($optionsData);
        $resultDataTable['recordsTotal'] = $rowCount;

        //[recordsFiltered]
        $resultDataTable['recordsFiltered'] = $rowCount;

        return $resultDataTable;
    }

    public function getListAdsType(){
        $arrList = array(
            self::ADS_TYPE_CLASSIC  => 'Classic',
            self::ADS_TYPE_STANDOUT => 'Standout',
            self::ADS_TYPE_PREMIUM => 'Premium'
        );

        return $arrList;
    }

}