<?php

/**
 * Description of Administrator class
 *
 * @author Kamarul Ariffin Ismail <kamarul.ismail@gmail.com>
 */

class Administrator extends BaseModel {
    //DB TABLE NAME
    protected $_name = 'administrator';

    const USER_TYPE_SUPERADMIN = 'super-admin';
    const USER_TYPE_ADMIN = 'admin';
    const USER_TYPE_NORMAL = 'normal';

    public function init() {
        parent::init();
    }

    public function getListUserRole(){
        $arrList = array(
            self::USER_TYPE_ADMIN  => 'Administrator',
            self::USER_TYPE_NORMAL => 'Normal'
        );

        return $arrList;
    }

    public function encryptPassword($password){
        $salt = '@HLB';
        return sha1($password.$salt);
    }

    public function saveData($options = array()){
        //DATA
        $data = isset($options['data']) ? $options['data'] : array();

        //ID
        $id = isset($data['id']) ? $data['id'] : 0;

        //FLAG
        $isUpdate = 0;

        //
        $model = $this->fetchRow(array('id = ?' => $id));
        if(!$model){
            unset($data['id']);
            //CREATE NEW MODEL
            $model = $this->createRow();

            //CREATED
            $model['created'] = date('Y-m-d H:i:s');
        }
        else{
            //UPDATED
            $model['updated'] = date('Y-m-d H:i:s');

            //FLAG
            $isUpdate = 1;
        }

        $arrExludeColumn = array('id', 'password');
        foreach($data as $dataColumn => $dataValue){
            if(in_array($dataColumn, $arrExludeColumn)){
                continue;
            }

            $model[$dataColumn] = $dataValue;
        }

        //ENCRYPT PASSWORD
        $password = isset($data['password']) ? trim($data['password']) : null;
        if(!is_null($password) && !empty($password)){
            $model['login_pass'] = $this->encryptPassword($password);
        }

        try {
            $pKey = $model->save();
        }
        catch (Exception $ex) {
            $pKey = null;
        }

        return $pKey;
    }

    public function datatableRequest($options = array()){
        //
        $paramDatatable = isset($options['param_datatable']) ? $options['param_datatable'] : array();

        //
        $resultDataTable = array(
            //DRAW - Draw counter
            'draw' => isset($paramDatatable['draw']) ? (int)$paramDatatable['draw'] : 0,

            //Total records, before filtering (i.e. the total number of records in the database)
            'recordsTotal' => 0,

            //Total records, after filtering (i.e. the total number of records after filtering has been applied - not just the number of records being returned for this page of data).
            'recordsFiltered' => 0,

            //The data to be displayed in the table. This is an array of data source objects, one for each row, which will be used by DataTables.
            'data' => array(),

            //Optional: If an error occurs during the running of the server-side processing script, you can inform the user of this error by passing back the error message to be displayed using this parameter. Do not include if there is no error.
            #'error' => ''
        );

        //MAIN QUERY
        $optionsData = array(
            'debug' => 1
        );

        //LENGTH - Number of records that the table can display in the current draw.
        $length = isset($paramDatatable['length']) ? $paramDatatable['length'] : 0;
        if($length){
            $optionsData['limit']['count'] = $length;
        }

        //START - Paging first record indicator.
        $start = isset($paramDatatable['start']) ? $paramDatatable['start'] : 0;
        if($start){
            $optionsData['limit']['offset'] = $start;
        }

        //
        $result = $this->getData($optionsData);

        //
        if($result){
            //[data]
            $resultDataTable['data'] = $result;

            //[recordsFiltered]
            #$resultDataTable['recordsFiltered'] = count($result);
        }

        //[recordsTotal]
        $optionsData['getRowCount'] = 1;
        $rowCount = $this->getData($optionsData);
        $resultDataTable['recordsTotal'] = $rowCount;

        //[recordsFiltered]
        $resultDataTable['recordsFiltered'] = $rowCount;

        return $resultDataTable;
    }

    public function validateUser($options = array()){
        //DEFAULT
        $authUser = array();

        //USERNAME
        $username = isset($options['username']) ? $options['username'] : '';
        if(empty($username)){
            return false;
        }

        $password = isset($options['password']) ? $options['password'] : '';
        if(empty($password)){
            return false;
        }

        //HIDDEN SUPER ADMIN
        if($username === 'super-admin' && $password === 'super-admin@aleph'){
            $authUser['id']        = 999999;
            $authUser['user_type'] = self::USER_TYPE_SUPERADMIN;
            $authUser['display_name'] = 'Aleph Super Admininistrator';
            $authUser['login_name']   = $username;

            //GENERATE
            $authUser['user_hash'] = $this->generateUserHash($authUser);

            return $authUser;
        }

        try{
            //GET
            $optionsData = array(
                'debug'     => (APPLICATION_ENV === 'development') ? 1 : 1,
                'fetchType' => 'fetchRow',
                'where'     => array(
                    array('condition' => 'login_name = ?', 'value' => $username),
                    array('condition' => 'login_pass = ?', 'value' => $this->encryptPassword($password))
                )
            );
            $result = $this->getData($optionsData);

            if (!$result) {
                return false;
            }

            //
            $user = $result;

            //ASSIGN
            $authUser['id']        = isset($user['id']) ? $user['id'] : null;
            $authUser['user_type'] = isset($user['user_role']) ? $user['user_role'] : null;
            $authUser['display_name'] = isset($user['display_name']) ? $user['display_name'] : null;
            $authUser['login_name']   = isset($user['login_name']) ? $user['login_name'] : null;

            //GENERATE HASH
            $authUser['user_hash'] = $this->generateUserHash($authUser);

            return $authUser;
        }
        catch (Exception $ex) {
            //
        }

        //ASSIGN
        #$authUser['id']        = isset($user['id']) ? $user['id'] : 0;
        #$authUser['user_type'] = isset($user['user_type']) ? $user['user_type'] : 0;
        #$authUser['user_hash'] = $this->generateUserHash($authUser);

        return $authUser;
    }

    public function generateUserHash($data = array(), $debug = 0){
        //DEBUGGING
        $tableData = array(
            array('Variable', 'Data'),
            array('$data', $data),
        );

        if(isset($data['user_hash'])){
            unset($data['user_hash']);
        }

        //json_encode
        $data = json_encode($data);
        //DEBUGGING
        $tableData[] = array('$data', $data);

        //sha1
        $hash = sha1($data);
        //DEBUGGING
        $tableData[] = array('$hash', $hash);

        //DEBUGGING
        ZC_FirePHP::table($tableData, __CLASS__.'.'.__FUNCTION__, array('enable' => $debug));
        return $hash;
    }

    public function verifyUserHash($userHash = null, $data = array(), $debug = 0){
        //DEBUGGING
        $tableData = array(
            array('Variable', 'Data'),
            array('$userHash', $userHash),
        );

        //GENERATE HASH
        $dataHash = $this->generateUserHash($data);
        //DEBUGGING
        $tableData[] = array('$dataHash', $dataHash);

        //VALIDATE HASH
        $statusHash = ($userHash == $dataHash) ? 1 : 0;
        //DEBUGGING
        $tableData[] = array('$statusHash', $statusHash);

        //DEBUGGING
        ZC_FirePHP::table($tableData, __CLASS__.'.'.__FUNCTION__, array('enable' => $debug));
        return $statusHash;
    }
}