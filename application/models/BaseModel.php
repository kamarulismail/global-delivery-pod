<?php

/**
 * Description of BaseModel class
 *
 * @author Kamarul Ariffin Ismail <kamarul.ismail@gmail.com>
 */

class BaseModel extends Zend_Db_Table_Abstract {
    //
    protected $_appSettings;
    protected $_debugSettings;

    //ERROR
    protected $_errorCode;
    protected $_errorMessage;

    public function init() {
        parent::init();

        //SETTINGS
        $_appSettings = array();
        if(Zend_Registry::isRegistered('appSettings')){
            $_appSettings = Zend_Registry::get('appSettings');
        }
        $this->_appSettings = $_appSettings;

        //SETTINGS
        $_debugSettings = array();
        if(Zend_Registry::isRegistered('debugSettings')){
            $_appSettings = Zend_Registry::get('debugSettings');
        }
        $this->_debugSettings = $_debugSettings;

        //ERROR HANDLER
        $this->_errorCode    = 0;
        $this->_errorMessage = null;
    }

    public function getError(){
        $arrError = array(
            'code'    => $this->_errorCode,
            'message' => $this->_errorMessage
        );

        return $arrError;
    }

    public function hasError(){
        $hasError = ($this->_errorCode != 0);
        return $hasError;
    }

    public function getErrorCode(){
        return $this->_errorCode;
    }

    public function getErrorMessage(){
        return $this->_errorMessage;
    }

    public function fileLogger($logData = '', $logFile = ''){
        $logDate = '[' . date('Y-m-d H:i:s P') . ']' . PHP_EOL;
        $logData = (is_array($logData)) ? var_export($logData, true) : $logData;
        $logData = $logDate . $logData . str_repeat(PHP_EOL, 2);

        //DEFAULT
        $className = get_called_class();
        if(!$className){
            $className = __CLASS__;
        }
        $logFileDefault = APPLICATION_LOG_PATH . DIRECTORY_SEPARATOR . $className . '-' . date('Ym').'.log';

        if(empty($logFile)){
            $logFile = $logFileDefault;
        }


        @file_put_contents($logFile, $logData, FILE_APPEND);
    }

    public function selectListFormat($listData = array()){
        $selectData = array();
        foreach ($listData as $index => $data) {
            $optValue = isset($data['value']) ? $data['value'] : $index;
            $optName  = isset($data['name']) ? $data['name'] : '';

            $selectData[$optValue] = $optName;
        }

        return $selectData;
    }

    public function dateFormatter($options = array()){
        $dateTime = isset($options['dateTime']) ? trim($options['dateTime']) : '';
        if(empty($dateTime)){
            return $dateTime;
        }

        //DATE FORMAT
        $inputFormat = isset($options['inputFormat']) ? trim($options['inputFormat']) : 'Y-m-d H:i:s';
        $ouputFormat = isset($options['outputFormat']) ? trim($options['outputFormat']) : 'd/m/Y h:i a';

        //
        $zendDate = new Zend_Date();
        $zendDate->setOptions(array('format_type' => 'php'));

        //VALIDATE IF VALID DATE
        if(!$zendDate->isDate($dateTime, $inputFormat)){
            return $dateTime;
        }

        //SET DATE
        $zendDate->set($dateTime, $inputFormat);

        //GET NEW DATE FORMAT
        $dateFormatted = $zendDate->get($ouputFormat);
        return $dateFormatted;
    }

    public function saveData($data = array()){
        if(empty($data)){
            return false;
        }

        try{
            $model = $this->createRow($data);
            $pKey  = $model->save();
            return $pKey;

        } catch (Exception $ex) {
            $this->fileLogger('Exception: '.$ex->getMessage());
            $this->fileLogger($data);
            return false;
        }
    }

    public function getDataV1($options = array()){
        //DEBUG
        $debug = isset($options['debug']) ? $options['debug'] : 0;

        //QUERY
        $query = $this->select(true)->setIntegrityCheck(false);

        //FILTERS
        $where = isset($options['where']) ? $options['where'] : array();
        if (!empty($where)) {
            foreach ($where as $w) {
                $w_cond  = isset($w['condition']) ? $w['condition'] : null;
                $w_value = isset($w['value']) ? $w['value'] : null;
                $w_type  = isset($w['type']) ? $w['type'] : null;
                $w_isOR  = isset($w['isOR']) ? $w['isOR'] : 0;

                if ($w_isOR === 1) {
                    $query->orWhere($w_cond, $w_value, $w_type);
                }
                else {
                    $query->where($w_cond, $w_value, $w_type);
                }
            }
        }

        //SORTING
        $order = isset($options['order']) ? $options['order'] : '';
        if(!empty($order)){
            $query->order($order);
        }

        //GROUP
        $group = isset($options['group']) ? $options['group'] : '';
        if(!empty($group)){
            $query->group($group);
        }

        //LIMIT
        $limit = isset($options['limit']) ? $options['limit'] : null;
        if(!is_null($limit)){
            $l_count  = isset($limit['count']) ? $limit['count'] : null;
            $l_offset = isset($limit['offset']) ? $limit['offset'] : null;
            $query->limit($l_count, $l_offset);
        }

        //LIMIT PAGE
        $limitPage = isset($options['limitPage']) ? $options['limitPage'] : null;
        if(!is_null($limitPage)){
            $lp_page     = isset($limitPage['page']) ? $limitPage['page'] : null;
            $lp_rowCount = isset($limitPage['rowCount']) ? $limitPage['rowCount'] : null;
            $query->limitPage($lp_page, $lp_rowCount);
        }

        //GET ROW COUNT
        $getRowCount = isset($options['getRowCount']) ? $options['getRowCount'] : 0;
        if($getRowCount){
            $debugTable = array(
                array('Variable', 'Data'),
            );

            //RESET
            $query->reset(Zend_Db_Select::ORDER);
            $query->reset(Zend_Db_Select::LIMIT_COUNT);
            $query->reset(Zend_Db_Select::LIMIT_OFFSET);

            //DEBUGGING
            $debugTable[] = array('query.main', (string)$query);

            //OPTIONS
            $countColumn = isset($options['countColumn']) ? $options['countColumn'] : '*';

            //ROW COUNT QUERY
            $countQuery = $this->select()->setIntegrityCheck(false)->from(array('t' => $query), array('rowCount' => "count({$countColumn})"));

            //DEBUGGING
            $debugTable[] = array('query.count', (string)$countQuery);

            //EXECUTE QUERY
            $result = $this->fetchRow($countQuery);
            if($result){
                $result = $result->toArray();
            }

            //
            $rowCount = (isset($result['rowCount']) ? $result['rowCount'] : 0);

            //DEBUGGING
            $debugTable[] = array('rowCount', $rowCount);
            ZC_FirePHP::table($debugTable, __CLASS__ . '.' . __FUNCTION__.'.getRowCount', array('enable' => $debug));

            return $rowCount;
        }

        //DEBUGGING
        ZC_FirePHP::info((string)$query, __CLASS__.'.'.__FUNCTION__, $debug);

        //EXECUTE
        $result = $this->fetchAll($query)->toArray();

        return $result;
    }

    public function getData($options = array()){
        $_class    = (get_called_class()) ? get_called_class() : __CLASS__;
        $_function = __FUNCTION__;

        //DEBUGGING
        $debugMode = isset($options['debug']) ? $options['debug'] : 0;
        $tableData = array(
            array('Variable', 'Data'),
            array('origin', __CLASS__),
            array('get_called_class', get_called_class()),
            array('$options', $options)
        );

        //CACHE
        $_cache     = $this->getSystemCache('cacheStaticData');
        $_cacheID   = isset($options['cacheID']) ? $options['cacheID'] : $_class.'_'.$_function.'_'. $this->generateCacheID($options);
        $_cacheTag  = isset($options['cacheTag']) ? $options['cacheTag'] : array($_class, $_function);
        $_noCache   = isset($options['noCache']) ? intval($options['noCache']) : 0;
        $_cacheData = $_cache->load($_cacheID);
        if($_cacheData !== false && !$_noCache){
            //DEBUGGING
            $tableData[] = array('data.cache', $_cacheData);
            ZC_FirePHP::table($tableData, "cacheID: {$_cacheID}", array('enable' => $debugMode));

            return $_cacheData;
        }

        //MAIN QUERY
        $query = $this->select(true)->setIntegrityCheck(false);

        //FILTERS
        $where = isset($options['where']) ? $options['where'] : array();
        if (!empty($where)) {
            foreach ($where as $w) {
                $w_cond  = isset($w['condition']) ? $w['condition'] : null;
                $w_value = isset($w['value']) ? $w['value'] : null;
                $w_type  = isset($w['type']) ? $w['type'] : null;
                $w_isOR  = isset($w['isOR']) ? $w['isOR'] : 0;

                if ($w_isOR === 1) {
                    $query->orWhere($w_cond, $w_value, $w_type);
                }
                else {
                    $query->where($w_cond, $w_value, $w_type);
                }
            }
        }

        //SORTING
        $order = isset($options['order']) ? $options['order'] : '';
        if(!empty($order)){
            $query->order($order);
        }

        //GROUP
        $group = isset($options['group']) ? $options['group'] : '';
        if(!empty($group)){
            $query->group($group);
        }

        //ROW COUNT
        $getRowCount = isset($options['getRowCount']) ? $options['getRowCount'] : 0;
        if($getRowCount){
            $getDataRowCountOptions = array(
                'query'   => $query,
                'noCache' => $_noCache,
                'debug'   => isset($debugMode) ? $debugMode : 0
            );
            $data = $this->getDataRowCount($getDataRowCountOptions);
            return $data;
        }

        //LIMIT
        $limit = isset($options['limit']) ? $options['limit'] : null;
        if(!is_null($limit)){
            $l_count  = isset($limit['count']) ? $limit['count'] : null;
            $l_offset = isset($limit['offset']) ? $limit['offset'] : null;
            $query->limit($l_count, $l_offset);
        }

        //LIMIT PAGE
        $limitPage = isset($options['limitPage']) ? $options['limitPage'] : null;
        if(!is_null($limitPage)){
            $lp_page     = isset($limitPage['page']) ? $limitPage['page'] : null;
            $lp_rowCount = isset($limitPage['rowCount']) ? $limitPage['rowCount'] : null;
            $query->limitPage($lp_page, $lp_rowCount);
        }

        //EXPLAIN
        $explain = isset($options['explain']) ? $options['explain'] : 0;
        if($explain){
            $this->doExplainQuery($query);
        }

        //FETCH DATA
        $fetchType = isset($options['fetchType']) ? $options['fetchType'] : null;
        if($fetchType == 'fetchRow'){
            $data = $this->fetchRow($query);
            if($data){
                $data = $data->toArray();
            }
        }
        else {
            $data = $this->fetchAll($query)->toArray();
        }

        //CACHE
        if(!$_noCache){
            $_cache->save($data, $_cacheID, $_cacheTag);
            //DEBUGGING
            $tableData[] = array('$_cacheID', $_cacheID);
            $tableData[] = array('$_cacheTag', $_cacheTag);
        }

        //DEBUGGING
        $tableData[] = array('$query', (string)$query);
        ZC_FirePHP::table($tableData, $_class.'.'.$_function, array('enable' => $debugMode));
        $this->dumpDataToFirePHPTable($data, $_cacheID, $debugMode);

        return $data;
    }

    public function getDataRowCount($options = array()){
        $_class    = (get_called_class()) ? get_called_class() : __CLASS__;
        $_function = __FUNCTION__;
        //DEBUGGING
        $debugMode = isset($options['debug']) ? $options['debug'] : 0;
        $tableData = array(
            array('Variable', 'Data'),
            array('origin', __CLASS__),
            array('get_called_class', get_called_class()),
            array('$options', $options)
        );

        //CACHE
        $_cache     = $this->getSystemCache('cacheStaticData');
        $_cacheID   = isset($options['cacheID']) ? $options['cacheID'] : $_class.'_'.$_function.'_'. $this->generateCacheID($options);
        $_cacheTag  = isset($options['cacheTag']) ? $options['cacheTag'] : array($_class, $_function);
        $_noCache   = isset($options['noCache']) ? intval($options['noCache']) : 0;
        $_cacheData = $_cache->load($_cacheID);
        if($_cacheData !== false && !$_noCache){
            //DEBUGGING
            $tableData[] = array('data.cache', $_cacheData);
            ZC_FirePHP::table($tableData, "cacheID: {$_cacheID}", array('enable' => $debugMode));

            return $_cacheData;
        }

        //
        $countColumn = isset($options['countColumn']) ? $options['countColumn'] : '1';
        $query       = isset($options['query']) ? $options['query'] : $this->_name;

        //
        if(get_class($query) === 'Zend_Db_Table_Select'){
            $query->reset(Zend_Db_Select::LIMIT_COUNT);
            $query->reset(Zend_Db_Select::LIMIT_OFFSET);
            $query->reset(Zend_Db_Select::ORDER);

            //RESET COLUMN
            $query->reset(Zend_Db_Select::COLUMNS);
            $query->columns(array('1' => new Zend_Db_Expr('1')));
        }

        $countQuery = $this->select()->setIntegrityCheck(false)->from(array('tmp_row_count' => $query), array('count' => "COUNT({$countColumn})"));

        //DEBUGGING
        $tableData[] = array('$countQuery', (string)$countQuery);

        //EXECUTE QUERY
        $result = $this->fetchRow($countQuery);
        if($result){
            $result = $result->toArray();
        }

        //COUNT
        $data = (isset($result['count']) ? $result['count'] : 0);

        //CACHE
        if(!$_noCache){
            $_cache->save($data, $_cacheID, $_cacheTag);
            //DEBUGGING
            $tableData[] = array('$_cacheID', $_cacheID);
            $tableData[] = array('$_cacheTag', $_cacheTag);
        }

        //DEBUGGING
        $tableData[] = array('$data', $data);
        ZC_FirePHP::table($tableData, 'ROWCOUNT: '.$_cacheID, array('enable' => $debugMode));

        return $data;
    }

    public function getTableName(){
        return isset($this->_name) ? $this->_name : '';
    }

    public function dumpDataToFirePHPTable($data, $label, $debugMode = 0){
        if(!$debugMode){
            return;
        }

        if(empty($data)){
            $data   = array();
            $data[] = array(__FUNCTION__ => 'Data is empty!');
        }

        //
        $tableData = $data;

        //GET COLUMN NAME
        $rowFirst    = isset($data[0]) ? $data[0] : $data;
        $tableColumn = array_keys($rowFirst);
        array_unshift($tableData, $tableColumn);

        ZC_FirePHP::table($tableData, $label, array('enable' => $debugMode));
    }

    /**
     * getCacheManager
     *
     * @return Zend_Cache_Manager
     */
    public function getCacheManager(){
        $zendResourceCacheManager = new Zend_Application_Resource_Cachemanager();
        $zendCacheManager         = $zendResourceCacheManager->getCacheManager();
        if(Zend_Registry::isRegistered('zendCacheManager')){
            $zendCacheManagerX = Zend_Registry::get('zendCacheManager');
            if(get_class($zendCacheManagerX) === 'Zend_Cache_Manager'){
                $zendCacheManager = $zendCacheManagerX;
            }
        }

        return $zendCacheManager;
    }

    /**
     * getSystemCache
     *
     * @param string $cacheKey
     * @return Zend_Cache_Core
     */
    public function getSystemCache($cacheKey = 'default'){
        $debugSettings = $this->_debugSettings;
        $debugMode     = isset($debugSettings['cache']['enable']) ? $debugSettings['cache']['enable'] : 0;

        ZC_FirePHP::groupCollapsed(__CLASS__.'.'.__FUNCTION__, array('enable' => $debugMode));
        ZC_FirePHP::log($cacheKey, 'cacheKey', $debugMode);

        //CACHE
        $zendCacheManager = $this->getCacheManager();
        $hasCache         = $zendCacheManager->hasCache($cacheKey);
        ZC_FirePHP::log($hasCache, 'hasCache', $debugMode);
        if(!$hasCache){
            $cacheKey = 'default';
        }

        $zendCache = $zendCacheManager->getCache($cacheKey);
        ZC_FirePHP::groupEnd($debugMode);
        return $zendCache;
    }

    public function generateCacheID($options = array()){
        if(!is_array($options)){
            $options = array($options);
        }

        $cacheID = sha1(preg_replace('/[^a-z0-9]/', 'X', strtolower(serialize($options))));
        return $cacheID;
    }

    /* DATATABLE */
    public function datatableRequest($options = array()){
        //
        $paramDatatable = isset($options['param_datatable']) ? $options['param_datatable'] : array();

        //
        $resultDataTable = array(
            //DRAW - Draw counter
            'draw' => isset($paramDatatable['draw']) ? (int)$paramDatatable['draw'] : 0,

            //Total records, before filtering (i.e. the total number of records in the database)
            'recordsTotal' => 0,

            //Total records, after filtering (i.e. the total number of records after filtering has been applied - not just the number of records being returned for this page of data).
            'recordsFiltered' => 0,

            //The data to be displayed in the table. This is an array of data source objects, one for each row, which will be used by DataTables.
            'data' => array(),

            //Optional: If an error occurs during the running of the server-side processing script, you can inform the user of this error by passing back the error message to be displayed using this parameter. Do not include if there is no error.
            #'error' => ''
        );

        //MAIN QUERY
        $optionsData = array(
            'debug' => 1
        );

        //LENGTH - Number of records that the table can display in the current draw.
        $length = isset($paramDatatable['length']) ? $paramDatatable['length'] : 0;
        if($length){
            $optionsData['limit']['count'] = $length;
        }

        //START - Paging first record indicator.
        $start = isset($paramDatatable['start']) ? $paramDatatable['start'] : 0;
        if($start){
            $optionsData['limit']['offset'] = $start;
        }

        //
        $result = $this->getData($optionsData);

        //
        if($result){
            //[data]
            $resultDataTable['data'] = $result;

            //[recordsFiltered]
            #$resultDataTable['recordsFiltered'] = count($result);
        }

        //[recordsTotal]
        $optionsData['getRowCount'] = 1;
        $rowCount = $this->getData($optionsData);
        $resultDataTable['recordsTotal'] = $rowCount;

        //[recordsFiltered]
        $resultDataTable['recordsFiltered'] = $rowCount;

        return $resultDataTable;
    }
}