<?php
/**
 * Description of zend_View_Helper_Utility
 *
 * @author Kamarul Ariffin Ismail <kamarul.ismail@gmail.com>
 */

/**
 * @see Zend_View_Helper_Abstract
 */
require_once 'Zend/View/Helper/Abstract.php';

class zend_View_Helper_Utility extends Zend_View_Helper_Abstract {
    protected $_instance;

    /**
     * Get helper object/instance
     *
     * @return zend_View_Helper_Utility
     */
    public function utility() {
        $_instance = isset($this->_instance) ? $this->_instance : null;
        if(!empty($_instance)){
            return $_instance;
        }

        //
        $this->_instance = $this;
        return $this->_instance;
    }

    /**
     * $this->Utility()->baseUrlFormatter()
     * @param type $baseUrl
     * @param type $currentUrl
     * @return type
     */
    public function baseUrlFormatter($baseUrl, $currentUrl, $debugMode = 0){
        #$debugMode = 0;
        ZC_FirePHP::groupCollapsed(__CLASS__.'.'.__FUNCTION__, array('enable' => $debugMode));
        //DEFAULT
        $formattedUrl = $currentUrl;

        //
        if(preg_match('/^htt(ps|p):/i', $currentUrl)){
            //START WITH HTTP or HTTPS
            $formattedUrl = $currentUrl;
        }
        else if((substr($currentUrl, 0, 1) == '/')){
            //START WITH "/"
            $formattedUrl = rtrim($baseUrl, '/') . $currentUrl;
        }

        ZC_FirePHP::log($baseUrl, 'baseUrl', $debugMode);
        ZC_FirePHP::log($currentUrl, 'currentUrl', $debugMode);
        ZC_FirePHP::info($formattedUrl, 'formattedUrl', $debugMode);
        ZC_FirePHP::groupEnd($debugMode);
        return $formattedUrl;
    }

    /**
     * $this->Utility()->assetUrlFormatter()
     * @param type $baseUrl
     * @param type $currentUrl
     * @return type
     */
    public function assetUrlFormatter($assetUrl, $currentUrl = '', $debugMode = 0){
        #$debugMode = 0;
        ZC_FirePHP::groupCollapsed(__CLASS__.'.'.__FUNCTION__, array('enable' => $debugMode));
        //DEFAULT
        $formattedUrl = $currentUrl;

        //
        if(preg_match('/^htt(ps|p):/i', $currentUrl)){
            //START WITH HTTP or HTTPS
            $formattedUrl = $currentUrl;
        }
        else if((substr($currentUrl, 0, 1) == '/')){
            //START WITH "/"
            $formattedUrl = rtrim($assetUrl, '/') . $currentUrl;
        }
        else if((substr($currentUrl, 0, 1) == '#')){
            //START WITH "#"
            //do nothing
        }
        else{
            #$formattedUrl = rtrim($assetUrl, '/') .'/'. $currentUrl;
            $formattedUrl  = rtrim($assetUrl, '/') . '/' . ltrim($currentUrl, '/');
        }

        ZC_FirePHP::log($assetUrl, 'assetUrl', $debugMode);
        ZC_FirePHP::log($currentUrl, 'currentUrl', $debugMode);
        ZC_FirePHP::info($formattedUrl, 'formattedUrl', $debugMode);
        ZC_FirePHP::groupEnd($debugMode);
        return $formattedUrl;
    }

    /**
     * $this->Utility()->parseSettings()
     * @param type $settings
     * @return array
     */
    public function parseSettings($settings, $output = array()){
        $debugMode = 1;
        ZC_FirePHP::groupCollapsed(__CLASS__.'.'.__FUNCTION__, array('enable' => $debugMode));
        ZC_FirePHP::log($settings, 'param.settings', $debugMode);
        ZC_FirePHP::log($output, 'param.output', $debugMode);


        $lastOutput = array_slice($output, -1, 1, true);

        #$settingID    = isset($output['id']) ? $output['id'] : '';
        #$settingValue = isset($output['value']) ? $output['value'] : '';
        #ZC_FirePHP::log($settingID, 'output.settingID', $debugMode);
        #ZC_FirePHP::log($settingValue, 'output.settingValue', $debugMode);

        //
        foreach($settings as $id => $value){
            ZC_FirePHP::log($id, 'loop.id', $debugMode);
            ZC_FirePHP::log($value, 'loop.value', $debugMode);

//            $settingID    = (empty($settingID)) ? $id : "{$settingID}.{$id}";
//            $settingValue = $value;
//            ZC_FirePHP::log($settingID, 'loop.settingID', $debugMode);
//            ZC_FirePHP::log($settingValue, 'loop.settingValue', $debugMode);

            if(is_array($value)){
                #$newOutput = $this->parseSettings($value,  array('id' => $settingID, 'value' => $settingValue));
                $newOutput = $this->parseSettings($value,  array('id' => "{$settingID}.{$id}", 'value' => $value));
                ZC_FirePHP::info($newOutput, 'newOutput', $debugMode);

                $settingID    = isset($newOutput['id']) ? $newOutput['id'] : $settingID;
                $settingValue = isset($newOutput['value']) ? $newOutput['value'] : $settingValue;
                ZC_FirePHP::log($settingID, 'newOutput.settingID', $debugMode);
                ZC_FirePHP::log($settingValue, 'newOutput.settingValue', $debugMode);
            }
            else{
                $settingID    = (empty($settingID)) ? $id : "{$settingID}.{$id}";
                $settingValue = $value;
                ZC_FirePHP::log($settingID, 'loop.settingID', $debugMode);
                ZC_FirePHP::log($settingValue, 'loop.settingValue', $debugMode);
            }
        }

        $result = array('id' => $settingID, 'value' => $settingValue);
        ZC_FirePHP::info($result, 'result', $debugMode);
        ZC_FirePHP::groupEnd($debugMode);
        return $result;
    }

    /**
     * $this->Utility()->truncate_word()
     * @param string $text
     * @param int $max_word
     * @param string $suffix
     * @return string
     */
    public function truncate_word($text, $max_word, $suffix = '...') {
        $debugMode = 0;
        ZC_FirePHP::groupCollapsed(__CLASS__.'.'.__FUNCTION__, array('enable' => $debugMode));
        ZC_FirePHP::info($text, 'parameter.text', $debugMode);
        ZC_FirePHP::info($max_word, 'parameter.max_word', $debugMode);
        ZC_FirePHP::info($suffix, 'parameter.suffix', $debugMode);

        $textLength = strlen($text);
        ZC_FirePHP::log($textLength, 'textLength', $debugMode);
        if ($textLength > $max_word) {
            $text = substr($text, 0, $max_word);
            $text = substr($text, 0, strrpos($text, " "));

            if(!empty($suffix)){
                $text .= " {$suffix}";
            }
        }

        ZC_FirePHP::warn($text, 'text.final', $debugMode);
        ZC_FirePHP::groupEnd($debugMode);
        return $text;
    }
}
