# Require any additional compass plugins here.

# Set default environment (overridable with -e)
tmp = environment
if environment.nil? then
  environment = :production
else
  environment = tmp
end
puts "SASS Environment = #{environment.inspect}"

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "css"
sass_dir = "sass"
images_dir = "images"
javascripts_dir = "js"
fonts_dir = "fonts"

# To enable relative paths to assets via compass helper functions. Uncomment:
relative_assets = true

# dont append random parameters to image url in css to bust cache
asset_cache_buster :none

# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed
output_style = :compressed
#output_style = :expanded
#output_style = :nested

#default environment = :production
# To disable debugging comments that display the original location of your selectors. Uncomment:
line_comments = false

# environment settings
if environment == :production
  sass_options = { :debug_info => false }
end

if environment == :development
  line_comments = true
  sass_options = { :debug_info => false }
  output_style = :expanded
end

if environment == :debugging
  line_comments = true
  sass_options = { :debug_info => true }
  output_style = :nested
end
