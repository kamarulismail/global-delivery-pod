/* CUSTOM JAVASCRIPT */
function initControlPanelLogin(){
    var $form = $('#form-control-panel-login');

    //for debugging
    // console.info('[initControlPanelLogin] $form \n', $form);

    if($form.length === 0){
        return;
    }

    //
    var $errorBlock = $form.find('#login-error-message');

    $form.on('submit', function(evt){
        evt.preventDefault();

        //for debugging
        console.groupCollapsed('[initControlPanelLogin][submit]');

        //
        var $this = $(this);

        //HIDE ERROR
        $errorBlock.removeClass('show').addClass('hidden');
        $this.find('.has-error').removeClass('has-error');

        //FLAG
        var isFormValid = 1;

        //VALIDATE
        var $inputLoginName = $this.find('#input-login-name');
        if($inputLoginName){

            var loginName = $.trim($inputLoginName.val());
            if(loginName == '' || loginName.length < 5){
                isFormValid = 0;

                //show error
                $formGroup = $inputLoginName.parents('.form-group');
                // $formGroup.find('.error-block').html(errMsgUsernameLength);
                $formGroup.addClass('has-error');

                //for debugging
                console.error('[$inputLoginName]');
            }

        }

        var $inputPassword = $this.find('#input-password');
        if($inputPassword){

            var password = $.trim($inputPassword.val());
            if(password == '' || password.length < 5){
                isFormValid = 0;

                //show error
                $formGroup = $inputPassword.parents('.form-group');
                // $formGroup.find('.error-block').html(errMsgUsernameLength);
                $formGroup.addClass('has-error');

                //for debugging
                console.error('[$inputPassword]');
            }

        }

        //for debugging
        console.info('isFormValid(%) ', isFormValid);
        console.groupEnd();

        var errorMessageLogin = 'Invalid Login Name/Password';
        if(isFormValid == 0){
            $errorBlock.text(errorMessageLogin).addClass('show').removeClass('hidden');
        }

        if(isFormValid){
            var ajaxUrl  = $this.attr("action") || getApiUrl();
            var ajaxData = $this.serializeArray();

            //ADD AJAX FLAG
            ajaxData.push({ "name" : "ajax" , "value" : 1 });

            //FOR DEBUGGING
            console.groupCollapsed('[initControlPanelLogin][ajaxRequest]');
            console.log('ajaxUrl \n', ajaxUrl);
            console.log('ajaxData \n', ajaxData);
            console.groupEnd();

            //AJAX REQUEST
            var $ajaxRequest = $.ajax({
                url: ajaxUrl,
                type: "POST",
                dataType: "JSON",
                data: ajaxData
            });

            $ajaxRequest.done(function(data, textStatus, jqXHR) {
                var loginStatus = data['loginStatus'] || 0;

                //FOR DEBUGGING
                console.info('[initControlPanelLogin] loginStatus(%s)', loginStatus);

                if(loginStatus == 0){
                    $errorBlock.text(errorMessageLogin).addClass('show').removeClass('hidden');
                    return;
                }
                
                if(loginStatus == 1){
                    //CREATE COOKIE
                    var cookieName   = data.cookieName || null;
                    var cookieOption = data.cookieOption || {};
                    var cookieValue  = data.cookieValue || {};
                    var cookieParam = {
                        cookieName : cookieName,
                        cookieOption : cookieOption
                    };
                    loginCookie('create', cookieValue, cookieParam);

                    //REDIRECT
                    var redirectURL = data.redirectURL || '';
                    console.log('redirectURL ', redirectURL);
                    if(redirectURL.length > 0){
                        window.location.href = redirectURL;
                    }
                }

                //for debugging
                console.groupCollapsed("[initControlPanelLogin][ajax][done|success]");
                console.log('data \n', data);
                console.log('textStatus \n', textStatus);
                console.log('jqXHR \n', jqXHR);
                console.groupEnd();
            });
            
            $ajaxRequest.fail(function(jqXHR, textStatus, errorThrown) {
                //for debugging
                console.groupCollapsed("[initControlPanelLogin][ajax][error|fail]");
                console.log('errorThrown \n', errorThrown);
                console.log('textStatus \n', textStatus);
                console.log('jqXHR \n', jqXHR);
                console.groupEnd();
            });

            // $ajaxRequest.always(function(param1, param2, param3) {
            //     //for debugging
            //     //data|jqXHR, textStatus, jqXHR|errorThrown
            //     console.groupCollapsed("[initControlPanelLogin][ajax][always|complete]");
            //     console.log('param1 \n', param1);
            //     console.log('param2 \n', param2);
            //     console.log('param3 \n', param3);
            //     console.groupEnd();
            // });
            
        } //if(isFormValid)

    });

}

function initControlPanelLogout(){
    //for debugging
    // console.info('[initControlPanelLogout] REMOVE_LOGIN_COOKIE(%s) ', typeof(REMOVE_LOGIN_COOKIE));
    
    //REMOVE COOKIE
    if(typeof(REMOVE_LOGIN_COOKIE) != 'undefined'){
        var cookieParam = (typeof(LOGIN_COOKIE_PARAMS) != 'undefined') ? LOGIN_COOKIE_PARAMS : null;
        loginCookie('delete', null, cookieParam);
    }
}

function initCustomerLogin(){
    var $form = $('#form-customer-login');

    //for debugging
    // console.info('[initCustomerLogin] $form \n', $form);

    //
    if($form.length === 0){
        return;
    }

    //
    var $errorBlock = $form.find('#login-error-message');

    $form.on('submit', function(evt){
        evt.preventDefault();

        //for debugging
        console.groupCollapsed('[initCustomerLogin][submit]');

        //
        var $this = $(this);

        //HIDE ERROR
        $errorBlock.removeClass('show').addClass('hidden');
        $this.find('.has-error').removeClass('has-error');

        //FLAG
        var isFormValid = 1;

        //VALIDATE
        var $inputEmail = $this.find('#input-email');
        if($inputEmail){

            var inputEmailValue = $.trim($inputEmail.val());
            if(inputEmailValue == '' || inputEmailValue.length < 5){
                isFormValid = 0;

                //show error
                $formGroup = $inputEmail.parents('.form-group');
                // $formGroup.find('.error-block').html(errMsgUsernameLength);
                $formGroup.addClass('has-error');

                //for debugging
                console.error('[$inputEmail]');
            }

        }

        var $inputPassword = $this.find('#input-password');
        if($inputPassword){

            var password = $.trim($inputPassword.val());
            if(password == '' || password.length < 5){
                isFormValid = 0;

                //show error
                $formGroup = $inputPassword.parents('.form-group');
                // $formGroup.find('.error-block').html(errMsgUsernameLength);
                $formGroup.addClass('has-error');

                //for debugging
                console.error('[$inputPassword]');
            }

        }

        //for debugging
        console.info('isFormValid(%) ', isFormValid);
        console.groupEnd();

        var errorMessageLogin = 'Invalid Login Name/Password';
        if(isFormValid == 0){
            $errorBlock.text(errorMessageLogin).addClass('show').removeClass('hidden');
        }

        if(isFormValid){
            var ajaxUrl  = $this.attr("action") || getApiUrl();
            var ajaxData = $this.serializeArray();

            //ADD AJAX FLAG
            ajaxData.push({ "name" : "ajax" , "value" : 1 });

            //FOR DEBUGGING
            console.groupCollapsed('[initCustomerLogin][ajaxRequest]');
            console.log('ajaxUrl \n', ajaxUrl);
            console.log('ajaxData \n', ajaxData);
            console.groupEnd();

            //AJAX REQUEST
            var $ajaxRequest = $.ajax({
                url: ajaxUrl,
                type: "POST",
                dataType: "JSON",
                data: ajaxData
            });

            $ajaxRequest.done(function(data, textStatus, jqXHR) {
                //for debugging
                debugAjaxDone(data, textStatus, jqXHR, "[initCustomerLogin]");

                var loginStatus = data['loginStatus'] || 0;

                //FOR DEBUGGING
                console.info('[initCustomerLogin] loginStatus(%s)', loginStatus);

                if(loginStatus == 0){
                    $errorBlock.text(errorMessageLogin).addClass('show').removeClass('hidden');
                    return;
                }
                
                if(loginStatus == 1){
                    //CREATE COOKIE
                    var cookieName   = data.cookieName || null;
                    var cookieOption = data.cookieOption || {};
                    var cookieValue  = data.cookieValue || {};
                    var cookieParam = {
                        cookieName : cookieName,
                        cookieOption : cookieOption
                    };
                    loginCookie('create', cookieValue, cookieParam);

                    //REDIRECT
                    var redirectURL = data.redirectURL || '';
                    if(redirectURL.length > 0){
                        window.location.href = redirectURL;
                    }
                } //if(loginStatus == 1)

            });
            
            $ajaxRequest.fail(function(jqXHR, textStatus, errorThrown) {
                //for debugging
                debugAjaxFail(jqXHR, textStatus, errorThrown, "[initCustomerLogin]");
            });
            
        } //if(isFormValid)

    });

}

function initCustomerLogout(){
    //for debugging
    // console.info('[initControlPanelLogout] REMOVE_LOGIN_COOKIE(%s) ', typeof(REMOVE_LOGIN_COOKIE));
    
    //REMOVE COOKIE
    if(typeof(REMOVE_LOGIN_COOKIE) != 'undefined'){
        var cookieParam = (typeof(LOGIN_COOKIE_PARAMS) != 'undefined') ? LOGIN_COOKIE_PARAMS : null;
        loginCookie('delete', null, cookieParam);
    }
}
/* DOCUMENT READY */
$(function () {
    //
    initCustomerLogin();
    initCustomerLogout();


    //
    initControlPanelLogin();
    initControlPanelLogout();
});
