/* CUSTOM JAVASCRIPT */
function initCustomerPackageDatatable(){
    var $table = $('#table-customer-package');

    //for debugging
    console.info('[initCustomerPackageDatatable] $table \n', $table);

    if($table.length === 0){
        return;
    }

    var apiUrl = $table.attr('data-api-url') || getApiUrl();

    //https://datatables.net/reference/option/
    var myDatatable = $table.DataTable({
        "lengthChange": false,
        "pageLength" : 10,
        "info" : false,
        "searching": false,
        "ordering": false,
        "processing": true,
        "serverSide": true,
        "deferRender": true,
        "language": {
            "emptyTable": "<b>No data available</b>",
            "zeroRecords" : "No matching records found",
            "infoEmpty" : ""
        },
        "ajax" : {
            "type" : "POST",
            "url" : apiUrl,
            "data" : function(d) {
                d.isDataTableRequest = 1
            }
        },
        "columns": [
        {
            name: "id",
            data: null,
            title: "ID",
            orderable: true,
            searchable: false,
            render: function(columnData, type, rowData){
                var data = parseData(rowData['id']);
                return data;
            }
        },
        {
            name: "package_name",
            data: null,
            title: "Package Name",
            orderable: true,
            searchable: false,
            render: function(columnData, type, rowData){
                var data = parseData(rowData['package_name']);
                return data;
            }
        },
        {
            name: "customer_id",
            data: null,
            title: "Name",
            orderable: true,
            searchable: false,
            render: function(columnData, type, rowData){
                var data = parseData(rowData['customer_id']);
                return data;
            }
        },
        {
            name: "created",
            data: null,
            title: "Created",
            orderable: true,
            searchable: false,
            render: function(columnData, type, rowData){
                var data = parseData(rowData['created']);
                return data;
            }
        },
        {
            name: "updated",
            data: null,
            title: "Updated",
            orderable: true,
            searchable: false,
            render: function(columnData, type, rowData){
                var data = parseData(rowData['updated']);
                return data;
            }
        },
        // {
        //     name: "is_active",
        //     data: null,
        //     title: "Status",
        //     orderable: true,
        //     searchable: false,
        //     render: function(columnData, type, rowData){
        //         var data = parseData(rowData['is_active']);

        //         //
        //         var $label = $('<div>').addClass('label');
        //         if(data == 1){
        //             $label.addClass('label-success');
        //             $label.text('ACTIVE');
        //         }
        //         else {
        //             $label.addClass('label-default');
        //             $label.text('DISABLED');
        //         }

        //         //GET HTML CODE
        //         var html = $label[0].outerHTML;
        //         return html;
        //     }
        // },
        {
            name: "menu", 
            data: null, 
            orderable: false, 
            searchable: false,
            width: "30px",
            render: function(columnData, type, rowData){
                var id = parseData(rowData['id']);

                var optLink = {
                    class: 'btn btn-primary btn-xs',
                    html: '<i class="fa fa-edit"></i>',
                    title: 'Update',
                    href: getUpdateUrl() + "/id/" + id
                };
                var $link = $('<a>' ,optLink);

                //GET HTML CODE
                var html = $link[0].outerHTML;
                return html;
            }
        }
        ] //"columns"
    });

}

function toggleOverlay(toggle){
    var $overlay = $('#box-customer-package-update .overlay');

    if(toggle === 'show'){
        $overlay.removeClass('hidden');
    }
    else if(toggle === 'hide'){
        $overlay.addClass('hidden');
    }
}

function initCustomerPackageForm(){
    var $form = $('#form-customer-package');

    //for debugging
    console.info('[initCustomerPackageForm] $form \n', $form);

    //
    if($form.length === 0){
        return;
    }

    //MODAL
    var $modalSuccess = $('#modal-edit-success');
    $modalSuccess.modal({
        backdrop: "static",
        keyboard: false,
        show: false
    });

    var $modalError = $('#modal-edit-error');
    $modalError.modal({
        backdrop: "static",
        keyboard: false,
        show: false
    });

    var $btnCancel = $('#btnCancel');
    $btnCancel.on('click', function(evt){
        evt.preventDefault();
        window.location.href = getListingUrl();
    });

    var $btnSubmit = $('#btnSubmit');
    $btnSubmit.on('click', function(evt){
        evt.preventDefault();
        $form.submit();
    });

    //EVENT: submit
    $form.on('submit', function(evt){
        evt.preventDefault();

        //
        toggleOverlay('show');

        //for debugging
        console.groupCollapsed('[initCustomerPackageForm][submit]');

        var $this = $(this);
        console.log('form ', $this);

        //RESET ERROR
        $this.find('.has-error').removeClass('has-error');

        //
        var isFormValid = 1;
        var $formGroup, $formInput, formValue;

        //VALIDATION: package-name
        $formInput = $this.find('#input-package-name');

        if($formInput.length === 1){
            formValue = $.trim($formInput.val());

            //for debugging
            console.log('[$formInput] value(%s) length(%s) \n',formValue ,formValue.length ,$formInput);

            if(formValue.length === 0 || formValue.length < 5){
                isFormValid = 0;

                //show error
                $formGroup = $formInput.parents('.form-group');
                $formGroup.find('.error-block').html('Minimum length is 6 characters');
                $formGroup.addClass('has-error');

                //for debugging
                console.error('[$formInput] \n', $formInput);
            }
        }

        //VALIDATION: select-customer
        $formInput = $this.find('#select-customer');
        if($formInput.length === 1){
            formValue = $formInput.val();

            //for debugging
            console.log('[$formInput] value(%s)  \n',formValue ,$formInput);

            if(formValue == 0 || formValue == ''){
                isFormValid = 0;

                //show error
                $formGroup = $formInput.parents('.form-group');
                $formGroup.find('.error-block').html('Please select a value');
                $formGroup.addClass('has-error');

                //for debugging
                console.error('[$formInput] \n', $formInput);
            }
        }

        //VALIDATION: select-advertistment
        $formInput = $this.find('#select-advertistment');
        if($formInput.length === 1){
            formValue = $formInput.val();

            //for debugging
            console.log('[$formInput] value(%s)  \n',formValue ,$formInput);

            if(formValue == 0 || formValue == ''){
                isFormValid = 0;

                //show error
                $formGroup = $formInput.parents('.form-group');
                $formGroup.find('.error-block').html('Please select a value');
                $formGroup.addClass('has-error');

                //for debugging
                console.error('[$formInput] \n', $formInput);
            }
        }

        //VALIDATION: select-package-type
        var packageType;
        $formInput = $this.find('#select-package-type');
        if($formInput.length === 1){
            formValue = $formInput.val();

            //
            packageType = formValue;

            //for debugging
            console.log('[$formInput] value(%s)  \n',formValue ,$formInput);

            if(formValue == 0 || formValue == ''){
                isFormValid = 0;

                //show error
                $formGroup = $formInput.parents('.form-group');
                $formGroup.find('.error-block').html('Please select a value');
                $formGroup.addClass('has-error');

                //for debugging
                console.error('[$formInput] \n', $formInput);
            }
        }

        if(packageType === 'deal'){
            //VALIDATION: input-deal-buy
            $formInput = $this.find('#input-deal-buy');
            if($formInput.length === 1){
                formValue = $formInput.val();

                //for debugging
                console.log('[$formInput] value(%s)  \n',formValue ,$formInput);

                if(formValue == 0 || formValue == ''){
                    isFormValid = 0;

                    //show error
                    $formGroup = $formInput.parents('.form-group');
                    $formGroup.find('.error-block').html('Please enter a value');
                    $formGroup.addClass('has-error');

                    //for debugging
                    console.error('[$formInput] \n', $formInput);
                }
            }
        }

        if(packageType === 'discount'){
            
        }

        // isFormValid = 0;

        //for debugging
        console.info('isFormValid(%s) ',isFormValid);
        console.groupEnd();

        if(isFormValid === 0){
            toggleOverlay('hide');
            return;
        }

        if(isFormValid){
            var ajaxUrl  = $this.attr("action") || getApiUrl();
            var ajaxData = $this.serializeArray();

            //ADD AJAX FLAG
            ajaxData.push({ "name" : "ajax" , "value" : 1 });

            //FOR DEBUGGING
            console.groupCollapsed('[initCustomerPackageForm][ajaxRequest]');
            console.log('ajaxUrl \n', ajaxUrl);
            console.log('ajaxData \n', ajaxData);
            console.groupEnd();

            //AJAX REQUEST
            var $ajaxRequest = $.ajax({
                url: ajaxUrl,
                type: "POST",
                dataType: "JSON",
                data: ajaxData
            });

            $ajaxRequest.done(function(data, textStatus, jqXHR) {
                toggleOverlay('hide');
                //for debugging
                debugAjaxDone(data, textStatus, jqXHR, "[initCustomerPackageForm]");

                var isSaved = data['isSaved'] || 0;

                //FOR DEBUGGING
                // console.info('[initCustomerPackageForm] isSaved(%s)', isSaved);

                if(isSaved == 0){
                    $modalError.modal('show');
                }
                else if(isSaved == 1){
                    $modalSuccess.modal('show');
                }
            });
            
            $ajaxRequest.fail(function(jqXHR, textStatus, errorThrown) {
                toggleOverlay('hide');
                //for debugging
                debugAjaxFail(jqXHR, textStatus, errorThrown, "[initCustomerPackageForm]");
            });
            
        } //if(isFormValid)

    });
}

/* DOCUMENT READY */
$(function () {
    //DATATABLE
    if(typeof $.fn.dataTable != 'undefined'){
        //https://datatables.net/reference/option/%24.fn.dataTable.ext.errMode
        //https://datatables.net/reference/event/error
        $.fn.dataTable.ext.errMode = 'throw';
    }

    /* ADMINISTRATOR */
    initCustomerPackageDatatable();
    initCustomerPackageForm();
});