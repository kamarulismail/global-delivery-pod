/* CUSTOM JAVASCRIPT */
// function parseData(data, defaultValue){
//     var defaultValue = (typeof defaultValue != 'undefined') ? defaultValue : '';
//     var data         = (typeof data != 'undefined') ? data : defaultValue;
//     return data;
// }

// function parseNullData(data, defaultValue){
//     var defaultValue = (typeof defaultValue != 'undefined') ? defaultValue : '';
//     var data         = (typeof data != 'undefined') ? data : defaultValue;

//     if(data == 'null' || data == null){
//         data = defaultValue;
//     }
//     return data;
// }

function initRewardCodeDatatable(){
    var $table = $('#table-reward-code');

    //for debugging
    console.info('[initRewardCodeDatatable] $table \n', $table);

    if($table.length === 0){
        return;
    }

    var apiUrl = $table.attr('data-api-url') || '#'

    //$("#example1").DataTable();
    //https://datatables.net/reference/option/
    var myDatatable = $table.DataTable({
        "lengthChange": false,
        "pageLength" : 10,
        "info" : false,
        "searching": false,
        "ordering": false,
        "processing": true,
        "serverSide": true,
        "deferRender": true,
        "ajax" : {
            "type" : "POST",
            "url" : apiUrl,
            "data" : function(d) {
                d.isDataTableRequest = 1
            }
        },
        "columns": [
        {
            name: "id",
            data: null,
            title: "ID",
            orderable: true,
            searchable: false,
            render: function(columnData, type, rowData){
                var data = parseData(rowData['id']);
                return data;
            }
        },
        {
            name: "reward_code",
            data: null,
            title: "Code",
            orderable: true,
            searchable: false,
            render: function(columnData, type, rowData){
                var data = parseData(rowData['reward_code']);
                return data;
            }
        },
        {
            name: "reward_point",
            data: null,
            title: "Point",
            orderable: true,
            searchable: false,
            render: function(columnData, type, rowData){
                var data = parseData(rowData['reward_point']);
                return data;
            }
        },
        // {
        //     name: "admin_type",
        //     data: null,
        //     title: "Admin Level",
        //     orderable: true,
        //     searchable: false,
        //     render: function(columnData, type, rowData){
        //         var data = parseData(rowData['admin_type']);
        //         return data;
        //     }
        // },
        {
            name: "created",
            data: null,
            title: "Created",
            orderable: true,
            searchable: false,
            render: function(columnData, type, rowData){
                var data = parseData(rowData['created']);
                return data;
            }
        },
        {
            name: "updated",
            data: null,
            title: "Updated",
            orderable: true,
            searchable: false,
            render: function(columnData, type, rowData){
                var data = parseData(rowData['updated']);
                return data;
            }
        },
        {
            name: "is_redeem",
            data: null,
            title: "Redeem",
            orderable: true,
            searchable: false,
            render: function(columnData, type, rowData){
                var data = parseData(rowData['is_redeem']);

                //
                var $label = $('<div>').addClass('label');
                if(data == 1){
                    $label.addClass('label-default');
                    $label.text('REDEEMED');
                }
                else {
                    $label.addClass('label-success');
                    $label.text('AVAILABLE');
                }

                //GET HTML CODE
                var html = $label[0].outerHTML;
                return html;
            }
        },
        {
            name: "is_active",
            data: null,
            title: "Status",
            orderable: true,
            searchable: false,
            render: function(columnData, type, rowData){
                var data = parseData(rowData['is_active']);

                //
                var $label = $('<div>').addClass('label');
                if(data == 1){
                    $label.addClass('label-success');
                    $label.text('ACTIVE');
                }
                else {
                    $label.addClass('label-default');
                    $label.text('DISABLED');
                }

                //GET HTML CODE
                var html = $label[0].outerHTML;
                return html;
            }
        },
        {
            name: "menu", 
            data: null, 
            title: "&nbsp;",
            orderable: false, 
            searchable: false,
            width: "15px",
            render: function(columnData, type, rowData){
                var id = parseData(rowData['id']);

                var optLink = {
                    class: 'btn btn-primary btn-xs',
                    html: '<i class="fa fa-edit"></i>',
                    title: 'Update',
                    href: getUpdateUrl() + "/id/" + id
                };
                var $link = $('<a>' ,optLink);

                //GET HTML CODE
                var html = $link[0].outerHTML;
                return html;
            }
        }
        ] //"columns"
    });

}

function initRewardCodeForm(){
    var $form = $('#form-reward-code');

    //for debugging
    console.info('[initRewardCodeForm] $form \n', $form);

    //
    if($form.length === 0){
        return;
    }

    //MODAL
    var $modalSuccess = $('#modal-edit-success');
    $modalSuccess.modal({
        backdrop: "static",
        keyboard: false,
        show: false
    });

    var $modalError = $('#modal-edit-error');
    $modalError.modal({
        backdrop: "static",
        keyboard: false,
        show: false
    });

    //EVENT: submit
    $form.on('submit', function(evt){
        evt.preventDefault();

        //for debugging
        console.groupCollapsed('[initRewardCodeForm][submit]');

        var $this = $(this);
        console.log('form ', $this);

        //RESET ERROR
        $this.find('.has-error').removeClass('has-error');


        //
        var isFormValid = 1;
        var $formGroup;

        //VALIDATION: reward-code
        var $inputRewardCode = $this.find('#input-reward-code');
        //for debugging
        console.log('[$inputRewardCode]\n',$inputRewardCode);

        if($inputRewardCode.length === 1){
            var errMsgMinimumLength = "Minimum length is 6 characters";

            var inputRewardCodeValue = $.trim($inputRewardCode.val());

            //for debugging
            console.log('[$inputRewardCode] value(%s) length(%s) \n',inputRewardCodeValue ,inputRewardCodeValue.length ,$inputRewardCode);

            if(inputRewardCodeValue.length === 0 || inputRewardCodeValue.length < 5){
                isFormValid = 0;

                //show error
                $formGroup = $inputRewardCode.parents('.form-group');
                $formGroup.find('.error-block').html(errMsgMinimumLength);
                $formGroup.addClass('has-error');

                //for debugging
                console.error('[$inputRewardCode]');
            }
        }

        //VALIDATION: reward-point
        var $inputRewardPoint = $this.find('#input-reward-point');
        //for debugging
        console.log('[$inputRewardPoint]\n',$inputRewardPoint);

        if($inputRewardPoint.length === 1){
            var errMsgEmpty = "Please enter a value";

            var inputRewardPointValue = $.trim($inputRewardPoint.val());

            //for debugging
            console.log('[$inputRewardPoint] value(%s) length(%s) \n',inputRewardPointValue ,inputRewardPointValue.length ,$inputRewardPoint);

            if(inputRewardPointValue.length === 0){
                isFormValid = 0;

                //show error
                $formGroup = $inputRewardPoint.parents('.form-group');
                $formGroup.find('.error-block').html(errMsgEmpty);
                $formGroup.addClass('has-error');

                //for debugging
                console.error('[$inputRewardPoint]');
            }
            else {
                //CHECK
                var errMsgInvalidFormat = "Please input number only [0-9]";

                var regex   = /^\d+$/; //integer only
                var isMatch = regex.test(inputRewardPointValue);

                //for debugging
                console.log('[$inputRewardPoint] value(%s) isMatch(%s) \n',inputRewardPointValue ,isMatch);

                if(!isMatch){
                    //show error
                    $formGroup = $inputRewardPoint.parents('.form-group');
                    $formGroup.find('.error-block').html(errMsgInvalidFormat);
                    $formGroup.addClass('has-error');

                    //for debugging
                    console.error('[$inputRewardPoint]');
                }
            }
        }

        //
        //VALIDATION: select-status
        var $selectStatus = $this.find('#select-status');
        //for debugging
        console.log('[$selectStatus]\n',$selectStatus);

        if($selectStatus.length === 1){
            var errMsgNoSelect = "Please select a value";

            var selectStatusValue = $.trim($selectStatus.val());

            //for debugging
            console.log('[$selectStatus] value(%s) length(%s) \n',selectStatusValue ,selectStatusValue.length ,$selectStatus);

            if(selectStatusValue == ''){
                isFormValid = 0;

                //show error
                $formGroup = $selectStatus.parents('.form-group');
                $formGroup.find('.error-block').html(errMsgNoSelect);
                $formGroup.addClass('has-error');

                //for debugging
                console.error('[$selectStatus]');
            }
        }

        //for debugging
        console.info('isFormValid(%s) ',isFormValid);
        console.groupEnd();

        if(isFormValid){
            var ajaxUrl  = $this.attr("action") || getApiUrl();
            var ajaxData = $this.serializeArray();

            //ADD AJAX FLAG
            ajaxData.push({ "name" : "ajax" , "value" : 1 });

            //FOR DEBUGGING
            console.groupCollapsed('[initRewardCodeForm][ajaxRequest]');
            console.log('ajaxUrl \n', ajaxUrl);
            console.log('ajaxData \n', ajaxData);
            console.groupEnd();

            //AJAX REQUEST
            var $ajaxRequest = $.ajax({
                url: ajaxUrl,
                type: "POST",
                dataType: "JSON",
                data: ajaxData
            });

            $ajaxRequest.done(function(data, textStatus, jqXHR) {
                var isSaved = data['isSaved'] || 0;

                //FOR DEBUGGING
                console.info('[initRewardCodeForm] isSaved(%s)', isSaved);

                if(isSaved == 0){
                    $modalError.modal('show');
                }
                else if(isSaved == 1){
                    $modalSuccess.modal('show');
                }

                //for debugging
                console.groupCollapsed("[initRewardCodeForm][ajax][done|success]");
                console.log('data \n', data);
                console.log('textStatus \n', textStatus);
                console.log('jqXHR \n', jqXHR);
                console.groupEnd();
            });
            
            $ajaxRequest.fail(function(jqXHR, textStatus, errorThrown) {
                //for debugging
                console.groupCollapsed("[initRewardCodeForm][ajax][error|fail]");
                console.log('errorThrown \n', errorThrown);
                console.log('textStatus \n', textStatus);
                console.log('jqXHR \n', jqXHR);
                console.groupEnd();
            });

            $ajaxRequest.always(function(param1, param2, param3) {
                //for debugging
                //data|jqXHR, textStatus, jqXHR|errorThrown
                console.groupCollapsed("[initRewardCodeForm][ajax][always|complete]");
                console.log('param1 \n', param1);
                console.log('param2 \n', param2);
                console.log('param3 \n', param3);
                console.groupEnd();
            });
            
        } //if(isFormValid)

    });
}

/* DOCUMENT READY */
$(function () {
    //DATATABLE
    if(typeof $.fn.dataTable != 'undefined'){
        //https://datatables.net/reference/option/%24.fn.dataTable.ext.errMode
        //https://datatables.net/reference/event/error
        $.fn.dataTable.ext.errMode = 'throw';
    }

    /* ADMINISTRATOR */
    initRewardCodeDatatable();
    initRewardCodeForm();
});