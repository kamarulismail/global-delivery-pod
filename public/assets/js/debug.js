/* CUSTOM JAVASCRIPT */
function debugCustomerLogin(){
    var $form = $('#form-customer-login');
    if($form.length === 0){
        return;
    }

    $form.find('#input-email').val('super-user@aleph-labs.com');
    $form.find('#input-password').val('super-user@aleph');
}

function debugControlPanelLogin(){
    var $form = $('#form-control-panel-login');
    if($form.length === 0){
        return;
    }

    $form.find('#input-login-name').val('super-admin');
    $form.find('#input-password').val('super-admin@aleph');
}

function debugRewardRegistration(){
    var $form = $('#form-reward-registration');
    if($form.length === 0){
        return;
    }

    $form.find('#input-name').val('Customer Full Name');
    $form.find('#input-mobile-number').val('0123456789');
    $form.find('#input-email').val('customer@test.net');
}

function debugRewardLogin(){
    var $form = $('#form-reward-login');
    if($form.length === 0){
        return;
    }

    $form.find('#input-login-name').val('customer@test.net');
    $form.find('#input-password').val('0123456789');
}


/* DOCUMENT READY */
$(function () {
    debugCustomerLogin();
    debugControlPanelLogin();

    //REWARD
    debugRewardRegistration();
    debugRewardLogin();
});
