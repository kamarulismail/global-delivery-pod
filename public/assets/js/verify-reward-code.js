/* CUSTOM JAVASCRIPT */
function initVerifyCode(){
    //FORM
    var $formVerifyCode = $('#form-verify-code');
    //FOR DEBUGGING
    // console.log('[initVerifyCode]$formVerifyCode \n',$formVerifyCode);

    if($formVerifyCode.length === 0){
        return false;
    }

    //MESSAGE
    var errorMessageEmptyCode   = "Please enter the verification code";
    var errorMessageInvalidCode = "The verification code is invalid";

    //MODAL
    var $modalVerificationError = $('#modal-verification-error');
    $modalVerificationError.modal({
        backdrop: "static",
        keyboard: false,
        show: false
    });

    //MODAL
    var $modalRegistration = $('#modal-registration');
    $modalRegistration.modal({
        backdrop: "static",
        keyboard: false,
        show: false
    });

    //MODAL
    var $modalRegistrationCancel = $('#modal-registration-cancel');
    $modalRegistrationCancel.modal({
        backdrop: "static",
        keyboard: false,
        show: false
    });

    //MODAL
    var $modalThankYou = $('#modal-thank-you');
    $modalThankYou.modal({
        backdrop: "static",
        keyboard: false,
        show: false
    });

    //EVENT: form-reward-registration
    var $formRegistration = $('#form-reward-registration');
    // console.info('$formRegistration \n', $formRegistration);

    $formRegistration.on('click', '[data-action="register-submit"],[data-action="register-cancel"]', function(evt){
        evt.preventDefault();
        //for debugging
        console.groupCollapsed('[#form-registration][click]');
        console.log('evt \n', evt);
        console.log('evt.target \n', evt.target);
        console.log('evt.currentTarget \n', evt.currentTarget);

        var $this = $(this);
        //for debugging
        console.log('$this \n', $this);
        console.log('$this.attr(data-action) \n', $this.attr('data-action'));
        console.groupEnd();

        if($this.attr('data-action') == "register-cancel"){
            $modalRegistrationCancel.modal('show');
        }

        if($this.attr('data-action') == "register-submit"){
            $formRegistration.submit();
        }
    });

    $formRegistration.on('submit', function(evt){
        evt.preventDefault();

        //for debugging
        console.groupCollapsed('[#form-registration][submit]');

        var $form = $(this);
        console.log('$form \n',$form);

        //HIDE ERROR BLOCK
        $form.find('.has-error').removeClass('has-error');
        $form.find('#error-message-register').addClass('hidden');

        //
        var isFormValid = 1;
        var $formGroup, errorMessage;

        //VALIDATE
        var $inputName = $form.find('#input-name');
        if($inputName){
            var inputNameValue = $inputName.val() || '';
            if(!validateFullName(inputNameValue)){
                isFormValid = 0;

                //SHOW ERROR BLOCK
                $formGroup   = $inputName.parents('.form-group');
                errorMessage = "Please enter a valid name (a-z, -, space)";
                $formGroup.find('.error-block').html(errorMessage);
                $formGroup.addClass('has-error');
            }
        }

        //VALIDATE : mobile
        var $inputMobileNumber = $form.find('#input-mobile-number');
        if($inputMobileNumber){
            var inputMobileNumberValue = $inputMobileNumber.val() || '';

            if(!validateMobileNumber(inputMobileNumberValue)){
                isFormValid = 0;

                //SHOW ERROR BLOCK
                $formGroup   = $inputMobileNumber.parents('.form-group');
                errorMessage = "Please enter valid mobile number - 01xxxxxxxx";
                $formGroup.find('.error-block').html(errorMessage);
                $formGroup.addClass('has-error');
            }
        }

        //VALIDATE : email
        var $inputEmail = $form.find('#input-email');
        if($inputEmail){
            var inputEmailValue = $inputEmail.val() || '';

            if(!validateEmail(inputEmailValue)){
                isFormValid = 0;

                //SHOW ERROR BLOCK
                $formGroup   = $inputEmail.parents('.form-group');
                errorMessage = "Please enter a valid email address - user@email.my";
                $formGroup.find('.error-block').html(errorMessage);
                $formGroup.addClass('has-error');
            }
        }

        //FOR DEBUGGING
        console.log('isFormValid(%s) ', isFormValid);
        console.groupEnd();

        if(isFormValid){
            var apiUrl   = $form.attr("action") || getRegistrationUrl();
            var ajaxData = $form.serializeArray();

            //ADD REWARD REFERENCE
            $rewardReference = $formVerifyCode.find('#reward-reference').val() || '';
            ajaxData.push({ "name" : "reward_reference" , "value" : $rewardReference });

            //ADD AJAX FLAG
            ajaxData.push({ "name" : "ajax" , "value" : 1 });

            //FOR DEBUGGING
            console.groupCollapsed('[initVerifyCode][#form-registration][ajaxRequest]');
            console.log('apiUrl \n', apiUrl);
            console.log('ajaxData \n', ajaxData);
            console.groupEnd();

            //AJAX REQUEST
            var $ajaxRequest = $.ajax({
                url: apiUrl,
                type: "POST",
                dataType: "JSON",
                data: ajaxData
            });

            $ajaxRequest.done(function(data, textStatus, jqXHR) {
                //for debugging
                debugAjaxDone(data, textStatus, jqXHR, "[initVerifyCode][#form-registration]");

                //
                var errorMessage = parseData(data['errorMessage']);

                var isEmailRegistered = parseData(data['isEmailRegistered']);
                if(isEmailRegistered){
                    $form.find('#error-message-register').html(errorMessage).removeClass('hidden');
                    console.info('[] ', $form.find('#error-message-register'))
                }

                var isRegisterSuccess = parseData(data['isRegisterSuccess']);
            });
            
            $ajaxRequest.fail(function(jqXHR, textStatus, errorThrown) {
                //for debugging
                debugAjaxFail(jqXHR, textStatus, errorThrown, "[initVerifyCode][#form-registration]");
            });
            
        } //if(isFormValid)
    });
    
    //MODAL
    $modalRegistrationCancel.on('click', 'button', function(evt){
        var $btn = $(this);

        //FOR DEBUGGING
        console.info('[$modalRegistrationCancel][button][click] ', $btn);

        $modalRegistrationCancel.modal('hide');
        if($btn.attr('id') === 'btn-register-cancel'){
            $modalRegistration.modal('hide');
        }
    });

    //BUTTON: btn-verify-code
    var $btnVerifyCode = $formVerifyCode.find('#btn-verify-code');

    //EVENT: button click
    $btnVerifyCode.on('click', function(evt){
        evt.preventDefault();

        //FOR DEBUGGING
        // console.info('[initVerifyCode][click] \n', $(this));

        //TRIGGER FROM SUBMIT
        $formVerifyCode.submit();
    });

    //EVENT: submit verification code form
    $formVerifyCode.on('submit', function(evt){
        evt.preventDefault();
        //for debugging
        console.groupCollapsed('[initVerifyCode][submit]');

        var $this = $(this);
        var $rewardCode = $this.find('#input-reward-code');
        var rewardCode  = $.trim($rewardCode.val());

        //FOR DEBUGGIN
        console.log('rewardCode(%s) ', rewardCode);

        //HIDE ERROR BLOCK
        $this.find('.has-error').removeClass('has-error');

        //VALIDATION
        var isFormValid = 1;
        if(rewardCode.length === 0){
            isFormValid = 0;

            //SHOW ERROR BLOCK
            var $formGroup = $rewardCode.parents('.form-group');
            $formGroup.find('.error-block').html(errorMessageEmptyCode);
            $formGroup.addClass('has-error');
        }

        //FOR DEBUGGING
        console.log('isFormValid(%s) ', isFormValid);
        console.groupEnd();

        if(isFormValid){
            var apiUrl   = $this.attr("action") || getApiUrl();
            var ajaxData = $this.serializeArray();

            //ADD AJAX FLAG
            ajaxData.push({ "name" : "ajax" , "value" : 1 });

            //FOR DEBUGGING
            // console.groupCollapsed('[initVerifyCode][ajaxRequest]');
            // console.log('apiUrl \n', apiUrl);
            // console.log('ajaxData \n', ajaxData);
            // console.groupEnd();

            //AJAX REQUEST
            var $ajaxRequest = $.ajax({
                url: apiUrl,
                type: "POST",
                dataType: "JSON",
                data: ajaxData
            });

            $ajaxRequest.done(function(data, textStatus, jqXHR) {
                //for debugging
                debugAjaxDone(data, textStatus, jqXHR, 'initVerifyCode')

                var isCodeValid = data['isValidCode'] || 0;
                //FOR DEBUGGING
                // console.info('[initVerifyCode] isCodeValid(%s)', isCodeValid);

                if(isCodeValid == 0){
                    $modalVerificationError.modal('show');

                    var errorMessage = data.errorMessage || '';
                    if(errorMessage){
                        //SHOW ERROR BLOCK
                        var $formGroup = $rewardCode.parents('.form-group');
                        $formGroup.find('.error-block').html(errorMessage);
                        $formGroup.addClass('has-error');
                    }
                }
                else if(isCodeValid == 1){
                    $modalRegistration.modal('show');

                    //ASSIGN REWARD DATA
                    var rewardCode      = data['reward_code'] || '';
                    var rewardReference = data['reward_reference'] || '';

                    //MODAL
                    $modalRegistration.find('.modal-body').find('#input-reward-code').val(rewardCode);
                    $modalRegistration.find('.modal-body').find('#input-reward-reference').val(rewardReference);

                    //FORM
                    $this.find('#reward-reference').val(rewardReference);
                }
            });
            
            $ajaxRequest.fail(function(jqXHR, textStatus, errorThrown) {
                //for debugging
                debugAjaxFail(jqXHR, textStatus, errorThrown, 'initVerifyCode');
            });
            
        } //if(isFormValid)
    });

    /* LOGIN */
    var $formRewardLogin = $('#form-reward-login');

    //EVENT: button.click
    $formRewardLogin.on('click', '[data-action="reward-login"]', function(evt){
        evt.preventDefault();

        //FOR DEBUGGING
        // console.info('[$formRewardLogin][click] \n', $(this));

        //FORM SUBMIT
        $formRewardLogin.submit();
    });

    //EVENT: form.submit
    $formRewardLogin.on('submit', function(evt){
        evt.preventDefault();

        //for debugging
        console.groupCollapsed('[#form-reward-login][submit]');

        var $form = $(this);
        console.log('$form \n',$form);

        //
        var $errorMessageLogin = $form.find('#error-message-login');

        //HIDE ERROR BLOCK
        // $form.find('.has-error').removeClass('has-error');
        $errorMessageLogin.addClass('hidden');

        //
        var isFormValid = 1;
        var errorMessageInvalidLogin = 'Invalid Login Name/Password';

        //VALIDATE : mobile
        var $inputLoginName = $form.find('#input-login-name');
        if($inputLoginName){
            var inputLoginNameValue = $inputLoginName.val() || '';

            if(inputLoginNameValue.length === 0){
                isFormValid = 0;
            }
        }

        //VALIDATE : email
        var $inputPassword = $form.find('#input-password');
        if($inputPassword){
            var inputPasswordValue = $inputPassword.val() || '';

            if(inputPasswordValue.length === 0){
                isFormValid = 0;
            }
        }

        //for debugging
        console.log('isFormValid(%s) ', isFormValid);
        console.groupEnd();

        if(!isFormValid){
            $errorMessageLogin.html(errorMessageInvalidLogin).removeClass('hidden');
            return;
        }

        if(isFormValid){
            
            var ajaxUrl  = $form.attr("action") || getLoginUrl();
            var ajaxData = $form.serializeArray();

            //ADD REWARD REFERENCE
            var rewardReference = $('#reward-reference').val() || '';
            ajaxData.push({ "name" : "reward_reference", "value" : rewardReference });

            //ADD AJAX FLAG
            ajaxData.push({ "name" : "ajax", "value" : 1 });

            //FOR DEBUGGING
            console.groupCollapsed('[#form-reward-login][ajaxRequest]');
            console.log('ajaxUrl \n', ajaxUrl);
            console.log('ajaxData \n', ajaxData);
            console.groupEnd();

            //AJAX REQUEST
            var $ajaxRequest = $.ajax({
                url: ajaxUrl,
                type: "POST",
                dataType: "JSON",
                data: ajaxData
            });

            $ajaxRequest.done(function(data, textStatus, jqXHR) {
                //for debugging
                debugAjaxDone(data, textStatus, jqXHR, "[#form-reward-login]");

                var loginStatus = data['loginStatus'] || 0;

                //FOR DEBUGGING
                console.info('[#form-reward-login] loginStatus(%s)', loginStatus);

                if(loginStatus == 0){
                    $errorBlock.text(errorMessageLogin).addClass('show').removeClass('hidden');
                    return;
                }
                
                if(loginStatus == 1){
                    //CREATE COOKIE
                    var cookieName   = data.cookieName || null;
                    var cookieOption = data.cookieOption || {};
                    var cookieValue  = data.cookieValue || {};
                    var cookieParam = {
                        cookieName : cookieName,
                        cookieOption : cookieOption
                    };
                    loginCookie('create', cookieValue, cookieParam);

                    //REDIRECT
                    var redirectURL = data.redirectURL || '#';
                    if(redirectURL.length > 1){
                        window.location.href = redirectURL;
                    }
                } //if(loginStatus == 1)

            });
            
            $ajaxRequest.fail(function(jqXHR, textStatus, errorThrown) {
                //for debugging
                debugAjaxFail(jqXHR, textStatus, errorThrown, "[#form-reward-login]");
            });

        } //if(isFormValid)

    });

}

/* DOCUMENT READY */
$(function () {
    initVerifyCode();
});
