/* CUSTOM JAVASCRIPT */

/* Avoid `console` errors in browsers that lack a console. */
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

function disableConsole(){
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = {};

    while (length--) {
        method = methods[length];
        console[method] = noop;
    }

    return console;
}

function parseData(data, defaultValue){
    var defaultValue = (typeof defaultValue != 'undefined') ? defaultValue : '';
    var data         = (typeof data != 'undefined') ? data : defaultValue;
    return data;
}

function parseNullData(data, defaultValue){
    var defaultValue = (typeof defaultValue != 'undefined') ? defaultValue : '';
    var data         = (typeof data != 'undefined') ? data : defaultValue;

    if(data == 'null' || data == null){
        data = defaultValue;
    }
    return data;
}

function loginCookie(cookieAction, cookieValue, cookieParam){
    //for debugging
    console.groupCollapsed('[loginCookie]');
    console.log('cookieAction ', cookieAction);

    var cookieName = 'HLBReward';

    var cookieOption = {
        secure : (window.location.protocol === 'http:') ? false : true,
        expires: 1
    };

    if(cookieParam){
        console.log('cookieParam \n', cookieParam);
        if(cookieParam.cookieName){
            cookieName = cookieParam.cookieName;
        }
    }
    
    console.log('cookieName ', cookieName);
    if(cookieAction === 'create'){
        Cookies.set(cookieName, cookieValue, cookieOption);
    }

    if(cookieAction === 'delete'){
        Cookies.remove(cookieName, cookieOption);
    }
    //for debugging
    console.groupEnd();
}

function dateConversion(inputDate, inputFormat, outputFormat){
    
}

function validateMobileNumber(data){
    data = parseData(data);

    //LENGTH
    // console.info('[validateMobileNumber] data(%s) length(%s) result(%s) ',data, data.length);
    if(data.length < 10){
        return false;
    }

    //FILTER INVALID CHARACTER
    var pattern = /[^0-9]/i;
    var result  = pattern.test(data);
    // console.info('[validateMobileNumber] data(%s) result(%s) ',data, result);
    if(result){
        return false;
    }

    //PREFIX
    if (data.substring(0,2) != '01') {
        return false;
    }

    return true;
}

function validateEmail(data){
    data = parseData(data);

    //LENGTH
    // console.info('[validateEmail] data(%s) length(%s) result(%s) ',data, data.length);
    if(data.length < 5){
        return false;
    }
    
    //FILTER INVALID CHARACTER
    var pattern1 = /[^0-9a-z@\.\+\-\_]/i;
    var result1  = pattern1.test(data);
    // console.info('[validateEmail] data(%s) result(%s) ',data, result1);
    if(result1){
        return false;
    }

    //
    var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var result  = pattern.test(data);
    // console.info('[validateEmail] data(%s) result(%s) ',data, result);
    if(!result){
        return false;
    }

    return true;
}

function validateFullName(data){
    data = parseData(data);

    //LENGTH
    // console.info('[validateFullName] data(%s) length(%s) result(%s) ',data, data.length);
    if(data.length < 5){
        return false;
    }

    var pattern = /^[A-Za-z\s-']+$/;
    var result  = pattern.test(data);
    // console.info('[validateFullName] data(%s) result(%s) ',data, result);
    if(!result){
        return false;
    }

    return true;
}

function debugAjaxDone(data, textStatus, jqXHR, label){
    //for debugging
    var label = (label) ? label : 'label';
    console.groupCollapsed('ajax.done - ' +  label);
    console.log('data ', data);
    console.log('textStatus ', textStatus);

    //LOOP THROUGH
    console.groupCollapsed('jqXHR');
    for(var keyName in jqXHR) {
        var keyValue = jqXHR[keyName];
        console.log('jqXHR.%s : ', keyName, keyValue);
    }
    console.groupEnd();

    console.groupEnd();
}

function debugAjaxFail(jqXHR, textStatus, errorThrown, label){
    // for debugging
    var label = (label) ? label : 'label';
    console.groupCollapsed('ajax.fail - ' + label);
    console.log('errorThrown ', errorThrown);
    console.log('textStatus ', textStatus);

    //LOOP THROUGH
    console.groupCollapsed('jqXHR');
    for(var keyName in jqXHR) {
        var keyValue = jqXHR[keyName];
        console.log('jqXHR.%s : ', keyName, keyValue);
    }
    console.groupEnd();

    console.groupEnd();
}

function debugAjaxAlways(param1, param2, param3, label){
    // for debugging
    var label = (label) ? label : 'label';
    console.groupCollapsed('ajax.always - ' + label);
    console.log('param1 ', param1);
    console.log('param2 ', param2);
    console.log('param3 ', param3);

    //LOOP THROUGH
    // console.groupCollapsed('jqXHR');
    // for(var keyName in jqXHR) {
    //     var keyValue = jqXHR[keyName];
    //     console.log('jqXHR.%s : ', keyName, keyValue);
    // }
    // console.groupEnd();

    console.groupEnd();
}

/* DOCUMENT READY */
$(function () {
    // console.info('[common.js]');
});