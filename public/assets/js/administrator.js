/* CUSTOM JAVASCRIPT */
// function parseData(data, defaultValue){
//     var defaultValue = (typeof defaultValue != 'undefined') ? defaultValue : '';
//     var data         = (typeof data != 'undefined') ? data : defaultValue;
//     return data;
// }

// function parseNullData(data, defaultValue){
//     var defaultValue = (typeof defaultValue != 'undefined') ? defaultValue : '';
//     var data         = (typeof data != 'undefined') ? data : defaultValue;

//     if(data == 'null' || data == null){
//         data = defaultValue;
//     }
//     return data;
// }

function initAdministratorDatatable(){
    var $table = $('#table-administrator');

    //for debugging
    console.info('[initAdministratorDatatable] $table \n', $table);

    if($table.length === 0){
        return;
    }

    var apiUrl = $table.attr('data-api-url') || '#'

    //$("#example1").DataTable();
    //https://datatables.net/reference/option/
    var myDatatable = $table.DataTable({
        "lengthChange": false,
        "pageLength" : 10,
        "info" : false,
        "searching": false,
        "ordering": false,
        "processing": true,
        "serverSide": true,
        "deferRender": true,
        "ajax" : {
            "type" : "POST",
            "url" : apiUrl,
            "data" : function(d) {
                d.isDataTableRequest = 1
            }
        },
        "columns": [
        {
            name: "id",
            data: null,
            title: "ID",
            orderable: true,
            searchable: false,
            render: function(columnData, type, rowData){
                var data = parseData(rowData['id']);
                return data;
            }
        },
        {
            name: "login_name",
            data: null,
            title: "Login Name",
            orderable: true,
            searchable: false,
            render: function(columnData, type, rowData){
                var data = parseData(rowData['login_name']);
                return data;
            }
        },
        {
            name: "display_name",
            data: null,
            title: "Display Name",
            orderable: true,
            searchable: false,
            render: function(columnData, type, rowData){
                var data = parseData(rowData['display_name']);
                return data;
            }
        },
        // {
        //     name: "admin_type",
        //     data: null,
        //     title: "Admin Level",
        //     orderable: true,
        //     searchable: false,
        //     render: function(columnData, type, rowData){
        //         var data = parseData(rowData['admin_type']);
        //         return data;
        //     }
        // },
        {
            name: "created",
            data: null,
            title: "Created",
            orderable: true,
            searchable: false,
            render: function(columnData, type, rowData){
                var data = parseData(rowData['created']);
                return data;
            }
        },
        {
            name: "updated",
            data: null,
            title: "Updated",
            orderable: true,
            searchable: false,
            render: function(columnData, type, rowData){
                var data = parseData(rowData['updated']);
                return data;
            }
        },
        {
            name: "last_login",
            data: null,
            title: "Last Login",
            orderable: true,
            searchable: false,
            render: function(columnData, type, rowData){
                var data = parseData(rowData['last_login']);
                return data;
            }
        },
        {
            name: "is_active",
            data: null,
            title: "Status",
            orderable: true,
            searchable: false,
            render: function(columnData, type, rowData){
                var data = parseData(rowData['is_active']);

                //
                var $label = $('<div>').addClass('label');
                if(data == 1){
                    $label.addClass('label-success');
                    $label.text('ACTIVE');
                }
                else {
                    $label.addClass('label-default');
                    $label.text('DISABLED');
                }

                //GET HTML CODE
                var html = $label[0].outerHTML;
                return html;
            }
        },
        {
            name: "menu", 
            data: null, 
            orderable: false, 
            searchable: false,
            width: "30px",
            render: function(columnData, type, rowData){
                var id = parseData(rowData['id']);

                var optLink = {
                    class: 'btn btn-primary btn-xs',
                    html: '<i class="fa fa-edit"></i>',
                    title: 'Update',
                    href: getUpdateUrl() + "/id/" + id
                };
                var $link = $('<a>' ,optLink);

                //GET HTML CODE
                var html = $link[0].outerHTML;
                return html;
            }
        }
        ] //"columns"
    });

}

function toggleOverlay(toggle){
    var $overlay = $('#box-administrator-update .overlay');

    if(toggle === 'show'){
        $overlay.removeClass('hidden');
    }
    else if(toggle === 'hide'){
        $overlay.addClass('hidden');
    }
}

function initAdministratorForm(){
    var $form = $('#form-administrator');

    //for debugging
    console.info('[initAdministratorForm] $form \n', $form);

    //
    if($form.length === 0){
        return;
    }

    //MODAL
    var $modalSuccess = $('#modal-edit-success');
    $modalSuccess.modal({
        backdrop: "static",
        keyboard: false,
        show: false
    });

    var $modalError = $('#modal-edit-error');
    $modalError.modal({
        backdrop: "static",
        keyboard: false,
        show: false
    });

    var $btnCancel = $('#btnCancel');
    $btnCancel.on('click', function(evt){
        evt.preventDefault();
        window.location.href = getListingUrl();
    });

    var $btnSubmit = $('#btnSubmit');
    $btnSubmit.on('click', function(evt){
        evt.preventDefault();
        $form.submit();
    });

    //EVENT: submit
    $form.on('submit', function(evt){
        evt.preventDefault();

        //
        toggleOverlay('show');

        //for debugging
        console.groupCollapsed('[initAdministratorForm][submit]');

        var $this = $(this);
        console.log('form ', $this);

        //RESET ERROR
        $this.find('.has-error').removeClass('has-error');


        //
        var isFormValid = 1;
        var $formGroup;

        //VALIDATION: login-name
        var $inputLoginName = $this.find('#input-login-name');
        //for debugging
        console.log('[$inputLoginName]\n',$inputLoginName);

        if($inputLoginName.length === 1){
            var errMsgUsernameLength = "Minimum length for login name is 6 characters";
            var errMsgUsernameFormat = "Minimum length for username is 6 characters";

            var loginName = $.trim($inputLoginName.val());

            //for debugging
            console.log('[$inputLoginName] value(%s) length(%s) \n',loginName ,loginName.length ,$inputLoginName);

            if(loginName.length === 0 || loginName.length < 5){
                isFormValid = 0;

                //show error
                $formGroup = $inputLoginName.parents('.form-group');
                $formGroup.find('.error-block').html(errMsgUsernameLength);
                $formGroup.addClass('has-error');

                //for debugging
                console.error('[$inputLoginName]');
            }
        }

        //VALIDATION: password
        var errMsgPasswordLength = "Minimum length for password is 6 characters";
        var errMsgPasswordFormat = "Invalid password format";

        var $inputPassword = $this.find('#input-password');
        //for debugging
        console.log('[$inputPassword]\n',$inputPassword);

        //
        if($inputPassword.length === 1){
            var password = $.trim($inputPassword.val());

            //for debugging
            console.log('[$inputPassword] value(%s) length(%s) \n',password ,password.length ,$inputPassword);

            if(password == '' || password.length < 5){
                isFormValid = 0;

                //show error
                $formGroup = $inputPassword.parents('.form-group');
                $formGroup.find('.error-block').html(errMsgPasswordLength);
                $formGroup.addClass('has-error');

                //for debugging
                console.error('[$inputPassword]');
            }
        }

        //VALIDATION: password-new
        var $inputPasswordNew = $this.find('#input-password-new');
        //for debugging
        console.log('[$inputPasswordNew]\n',$inputPasswordNew);

        //
        if($inputPasswordNew.length === 1){
            var passwordNew = $.trim($inputPasswordNew.val());

            //for debugging
            console.log('[$inputPasswordNew] value(%s) length(%s) \n',passwordNew ,passwordNew.length ,$inputPasswordNew);

            if(passwordNew != ''){

                if(passwordNew.length < 5){
                    isFormValid = 0;
                    
                    //show error
                    $formGroup = $inputPasswordNew.parents('.form-group');
                    $formGroup.find('.error-block').html(errMsgPasswordLength);
                    $formGroup.addClass('has-error');

                    //for debugging
                    console.error('[$inputPasswordNew]');
                }
            }
        }

        //VALIDATION: email
        var $inputEmail = $this.find('#input-email');

        //for debugging
        console.info('isFormValid(%s) ',isFormValid);
        console.groupEnd();

        if(isFormValid === 0){
            toggleOverlay('hide');
            return;
        }

        if(isFormValid){
            var ajaxUrl  = $this.attr("action") || getApiUrl();
            var ajaxData = $this.serializeArray();

            //ADD AJAX FLAG
            ajaxData.push({ "name" : "ajax" , "value" : 1 });

            //FOR DEBUGGING
            console.groupCollapsed('[initAdministratorForm][ajaxRequest]');
            console.log('ajaxUrl \n', ajaxUrl);
            console.log('ajaxData \n', ajaxData);
            console.groupEnd();

            //AJAX REQUEST
            var $ajaxRequest = $.ajax({
                url: ajaxUrl,
                type: "POST",
                dataType: "JSON",
                data: ajaxData
            });

            $ajaxRequest.done(function(data, textStatus, jqXHR) {
                var isSaved = data['isSaved'] || 0;

                //FOR DEBUGGING
                console.info('[initAdministratorForm] isSaved(%s)', isSaved);

                if(isSaved == 0){
                    $modalError.modal('show');
                }
                else if(isSaved == 1){
                    $modalSuccess.modal('show');
                }

                //for debugging
                console.groupCollapsed("[initAdministratorForm][ajax][done|success]");
                console.log('data \n', data);
                console.log('textStatus \n', textStatus);
                console.log('jqXHR \n', jqXHR);
                console.groupEnd();
            });
            
            $ajaxRequest.fail(function(jqXHR, textStatus, errorThrown) {
                //for debugging
                console.groupCollapsed("[initAdministratorForm][ajax][error|fail]");
                console.log('errorThrown \n', errorThrown);
                console.log('textStatus \n', textStatus);
                console.log('jqXHR \n', jqXHR);
                console.groupEnd();
            });

            $ajaxRequest.always(function(param1, param2, param3) {
                //for debugging
                //data|jqXHR, textStatus, jqXHR|errorThrown
                console.groupCollapsed("[initAdministratorForm][ajax][always|complete]");
                console.log('param1 \n', param1);
                console.log('param2 \n', param2);
                console.log('param3 \n', param3);
                console.groupEnd();
            });
            
        } //if(isFormValid)

    });
}

/* DOCUMENT READY */
$(function () {
    //DATATABLE
    if(typeof $.fn.dataTable != 'undefined'){
        //https://datatables.net/reference/option/%24.fn.dataTable.ext.errMode
        //https://datatables.net/reference/event/error
        $.fn.dataTable.ext.errMode = 'throw';
    }

    /* ADMINISTRATOR */
    initAdministratorDatatable();
    initAdministratorForm();
});