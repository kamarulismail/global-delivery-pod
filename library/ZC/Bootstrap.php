<?php

/**
 * Description of ZC_Bootstrap
 *
 * @author Kamarul Ariffin Ismail <kamarul.ismail@gmail.com>
 */
class ZC_Bootstrap extends Zend_Application_Bootstrap_Bootstrap {
    protected $_appSettings;
    protected $_debugSettings;

    protected function init(){
        $autoloader = Zend_Loader_Autoloader::getInstance();
        $autoloader->registerNamespace('ZC_');
        $autoloader->setFallbackAutoloader(true);

        $this->initPath();
        $this->initRegistry();
        $this->initFirePHP();
        $this->initPHPLogs();
        // $this->initRouter();
    }

    protected function initPath(){
        $basePath = realpath(APPLICATION_PATH . DIRECTORY_SEPARATOR . '..');

        // Define path to application directory
        defined('APPLICATION_LIBRARY_PATH') || define('APPLICATION_LIBRARY_PATH', $basePath . DIRECTORY_SEPARATOR . 'library');

        // Define path to public directory
        defined('APPLICATION_PUBLIC_PATH') || define('APPLICATION_PUBLIC_PATH', $basePath . DIRECTORY_SEPARATOR . 'public');

        // Define path to log directory
        defined('APPLICATION_LOG_PATH') || define('APPLICATION_LOG_PATH', $basePath . DIRECTORY_SEPARATOR . 'logs');

        // Define path to cache directory
        defined('APPLICATION_CACHE_PATH') || define('APPLICATION_CACHE_PATH', $basePath . DIRECTORY_SEPARATOR . 'caches');

        // Define path to config directory
        defined('APPLICATION_CONFIG_PATH') || define('APPLICATION_CONFIG_PATH', APPLICATION_PATH . DIRECTORY_SEPARATOR . 'configs');

        // Define path to temp directory
        defined('APPLICATION_TEMP_PATH') || define('APPLICATION_TEMP_PATH', APPLICATION_PATH . DIRECTORY_SEPARATOR . 'temp');

        //
        defined('ENT_HTML401') || define('ENT_HTML401', 0);
    }

    protected function initFirePHP(){
        $debugSettings  = $this->_debugSettings;
        $firePHPEnabled = isset($debugSettings['firephp']['enable']) ? (int)$debugSettings['firephp']['enable'] : 0;
        ZC_FirePHP::setLibrary('FirePHPCore');
        ZC_FirePHP::setEnable($firePHPEnabled);

        $tableData = array(
            array('Variable', 'Data'),
            array('FirePHP Debug Enabled', date('Y-m-d H:i:s O T')),
            array('FirePHP Library', ZC_FirePHP::getLibrary()),
            array('APPLICATION_ENV', APPLICATION_ENV),
            array('Zend_Version::VERSION', Zend_Version::VERSION),
            array('_debugSettings', $debugSettings),
            array('_SERVER[DOCUMENT_ROOT]', $_SERVER['DOCUMENT_ROOT']),
            array('get_include_path', get_include_path()),
        );
        ZC_FirePHP::table($tableData, __CLASS__.'.'.__FUNCTION__, array('enable' => 1));

        // Zend_Debug::dump($tableData, __CLASS__.'.'.__FUNCTION__);
    }

    protected function initRegistry(){
        // APPLICATION
        $appSettings = $this->getApplication()->getOption('appSettings');
        Zend_Registry::set('appSettings', $appSettings);
        $this->_appSettings = $appSettings;

        // DEBUGGING
        $debugSettings = $this->getApplication()->getOption('debug');
        
        //application-debug.ini
        $iniFile = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'configs' . DIRECTORY_SEPARATOR . 'debug.ini';
        if (is_readable($iniFile)) {
            $zendConfigIni = new Zend_Config_Ini($iniFile, 'debug');
            $customDebug   = $zendConfigIni->toArray();
            
            if(!empty($customDebug)){
                if(!is_array($debugSettings)){
                    $debugSettings = array();
                }

                $debugSettingsTemp = array_merge($debugSettings, $customDebug);
                $debugSettings     = $debugSettingsTemp;
            }
        }

        if(!empty($debugSettings) && is_array($debugSettings)){
            //SORT KEY ASC
            ksort($debugSettings);
        }

        Zend_Registry::set('debugSettings', $debugSettings);
        $this->_debugSettings = $debugSettings;
    }

    protected function initPHPLogs(){
        //PHP ERROR LOG
        $phpLogFile = APPLICATION_LOG_PATH . DIRECTORY_SEPARATOR . 'php-error-log.txt';
        $phpLogDir  = dirname($phpLogFile);
        if(!file_exists($phpLogDir)){
            @mkdir($phpLogDir, 0777, true);
        }

        if(!file_exists($phpLogFile)){
            @file_put_contents($phpLogFile, '');
        }

        if(is_writable($phpLogFile)){
            ini_set('log_errors', 1);
            ini_set('error_log', $phpLogFile);
        }
    }

    protected function initRouter(){
        //debugging
        $debugSettings = $this->_debugSettings;
        $debugMode     = isset($debugSettings['router']['enable']) ? $debugSettings['router']['enable'] : 0;
        $debugMode     = ($debugMode == 2) ? 1 : 0;

        //router file
        $routerFile = APPLICATION_CONFIG_PATH . DIRECTORY_SEPARATOR . 'router.ini';
        if(!is_readable($routerFile)){
            ZC_FirePHP::error("Invalid Router File: {$routerFile}",__CLASS__.'.'.__FUNCTION__,$debugMode);
            return;
        }

        $routerSettings = @parse_ini_file($routerFile, true);
        if($routerSettings == FALSE || empty($routerSettings)){
            ZC_FirePHP::error("Invalid Router Data: {$routerFile}",__CLASS__.'.'.__FUNCTION__,$debugMode);
            return;
        }

        //
        $appSettings = isset($this->_appSettings) ? $this->_appSettings : array();
        $front       = Zend_Controller_Front::getInstance();
        $router      = $front->getRouter();
        $prefixUrl   = isset($appSettings['site']['prefixUrl']) ? $appSettings['site']['prefixUrl'] : '/';

        //debugging
        ZC_FirePHP::groupCollapsed(__CLASS__.'.'.__FUNCTION__, array('enable' => $debugMode));
        $_tableData = array(
            array('Variable', 'Data'),
            array('routerSettings', $routerSettings),
            array('router', $router),
            array('router', $router->getRoutes()),
            array('prefixUrl', $prefixUrl),
        );

        //loop through sections
        foreach($routerSettings as $routerSection => $routerConfig){
            //loading ini file
            $routerConfig = new Zend_Config_Ini($routerFile, $routerSection, array('allowModifications' => true));

            //debugging
            $tableData = array(
                array('Variable', 'Data'),
                array('routerSection', $routerSection),
                array('routerConfig', $routerConfig->toArray()),
            );

            //BASE URL
            $baseUrl = isset($appSettings['main']['baseUrl']) ? $appSettings['main']['baseUrl'] : '/';
            if(!in_array($routerSection, array('main'))){
                $_baseUrl = isset($appSettings[$routerSection]['baseUrl']) ? $appSettings[$routerSection]['baseUrl'] : '/' . $routerSection;
                $urlPath  = parse_url($_baseUrl, PHP_URL_PATH);
                $baseUrl  = rtrim($urlPath, '/') . '/';
            }
            $baseUrl = rtrim($prefixUrl, '/') . '/' . ltrim($baseUrl, '/');
            //debugging
            $tableData[] = array('$baseUrl', $baseUrl);

            // GENERATE NEW ROUTE
            $newRoutes = array();
            foreach ($routerConfig as $key => $route) {
                if (strtolower($key) == 'index') {
                    $newRoute = $baseUrl;
                }
                else {
                    $newRoute = $baseUrl . ltrim($route->route, '/');
                }
                $route->route = $newRoute;

                // GENERATE NEW ROUTE KEY
                $routeKey = str_replace('-', ' ', $routerSection);
                $routeKey = str_replace(' ', '', ucwords($routeKey)) . ucfirst($key);
                $newRoutes[$routeKey] = $route;
                //debugging
                $tableData[] = array("newRoute[{$routeKey}]", $newRoute);
            }

            //CREATE NEW CONFIG
            $newRouteConfig = new Zend_Config($newRoutes);
            $router->addConfig($newRouteConfig);

            //debugging
            $tableData[] = array('newRouteConfig', $newRouteConfig);
            $tableData[] = array('newRouteConfig', $newRouteConfig->toArray());
            ZC_FirePHP::table($tableData, "routerSection[{$routerSection}]", array('enable' => $debugMode));
        }

        //debugging
        $_tableData[] = array('router.final', $router);
        $_tableData[] = array('router.final', $router->getRoutes());
        ZC_FirePHP::table($_tableData, __CLASS__ . '.' . __FUNCTION__, array('enable' => $debugMode));
        ZC_FirePHP::groupEnd($debugMode);
    }
}
