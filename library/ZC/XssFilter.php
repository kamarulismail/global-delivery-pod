<?php

/**
 * Description of ZC_XssFilter
 *
 * @author Kamarul Ariffin Ismail <kamarul.ismail@gmail.com>
 */

class ZC_XssFilter {
    protected $_debugSettings;
    protected $_xssFilters;

    public function __construct() {
        $_appSettings = array();
        if(Zend_Registry::isRegistered('appSettings')){
            $_appSettings = Zend_Registry::get('appSettings');
        }
        $this->_appSettings = $_appSettings;

        $_debugSettings = array();
        if(Zend_Registry::isRegistered('debugSettings')){
            $_debugSettings = Zend_Registry::get('debugSettings');
        }
        $this->_debugSettings = $_debugSettings;

        //INI
        $iniFile = APPLICATION_PATH . DIRECTORY_SEPARATOR . 'configs' . DIRECTORY_SEPARATOR . 'xss-filter.ini';
        if(is_readable($iniFile)){
            $_xssFilters = array();
            try{
                $iniData = new Zend_Config_Ini($iniFile);
                $iniData = $iniData->toArray();

                $search  = array('/');
                $replace = array('\/');

                foreach($iniData as $filterID => $filterData){
                    $arrFilterKeyword = isset($filterData['keyword']) ? $filterData['keyword'] : array();
                    foreach($arrFilterKeyword as $filterValue){
                        #$filterValue = str_replace($search, $replace, $filterValue);
                        $_xssFilters[$filterID][] = $filterValue;
                    }
                }
            }
            catch (Exception $ex) {

            }
            $this->_xssFilters = $_xssFilters;
        }
    }

    public function filterData($data = '', $debug = 0){
        //debugging
        $debugMode = isset($this->_debugSettings['xssFilter']['enable']) ? $this->_debugSettings['xssFilter']['enable'] : $debug;
        $tableData = array(
            array('Variable', 'Data'),
            array('$data', $data),
        );

        //START FILTER
        $cleanData = trim($data);
        if ($cleanData == '') {
            //debugging
            $tableData[] = array('$cleanData', $cleanData);
            ZC_FirePHP::table($tableData, __CLASS__ . '.' . __FUNCTION__, array('enable' => $debugMode));

            return $cleanData;
        }

        //STRIP ANY HTML TAGS
        $stripData = strip_tags($cleanData);
        if (strlen($cleanData) != strlen($stripData)) {
            $cleanData = 'XSS-FILTER';

            //debugging
            $tableData[] = array('strlen($cleanData)|strlen($stripData)', strlen($cleanData) . '|' . strlen($stripData));
            $tableData[] = array('$stripData', $stripData);
            $tableData[] = array('$cleanData', $cleanData);
            ZC_FirePHP::table($tableData, __CLASS__ . '.' . __FUNCTION__, array('enable' => $debugMode));

            return $cleanData;
        }

        //FILTERS
        $_xssFilter = isset($this->_xssFilters) ? $this->_xssFilters : array();
        $arrFilters = (!empty($_xssFilter)) ? $_xssFilter : array();

        // HTML TAGS
        $arrFilters['html_tags'] = '<[^>]*>';

        // JS TAGS
        $arrFilters['filter_tags'] = array('script', 'javascript');

        // JS FUNCTION NAME
        $arrFilters['js_function'] = array('fromCharCode\(', 'reload\(', 'alert\(', 'redirect\(');

        //JS EVENTS (https://www.owasp.org/index.php/XSS_Filter_Evasion_Cheat_Sheet)
        $arrFilters['js_event_1'] = array('onLoad', 'onClick', 'onDblClick', 'onChange', 'onFocus', 'onSelect', 'onBlur', 'onBegin');
        $arrFilters['js_event_2'] = array('onFocus?', 'onMouse?', 'onKey?', 'onSelect?', 'onBefore?');
        $arrFilters['js_event_3'] = array('onPaste', 'onCopy', 'onScroll', 'onCut');

        //
        $cleanData = urldecode($cleanData);

        //LOOP
        foreach ($arrFilters as $filterID => $filterData) {
            //GENERATE PATTERN
            $filter  = (is_array($filterData)) ? implode('|', $filterData) : $filterData;
            $pattern = '/' . $filter . '/i';

            #echo PHP_EOL."PATTERN[{$filterID}] ( {$pattern} )".PHP_EOL;

            //EXECUTE
            $matches = null;
            $result  = preg_match_all($pattern, $cleanData, $matches);

            //debugging
            $tableData[] = array("pattern[{$filterID}]", $pattern);
            if ($result) {
                #Zend_Debug::dump(array($cleanData, $pattern, $matches), "MATCH[{$filterID}]");
                #echo PHP_EOL."MATCH[{$filterID}] ( {$pattern} )".PHP_EOL.$cleanData.PHP_EOL;

                //debugging
                $tableData[] = array("matches[{$filterID}]", $matches);

                //
                $cleanData = 'XSS-FILTER';

                //debugging
                $tableData[] = array('$cleanData', $cleanData);
                ZC_FirePHP::table($tableData, __CLASS__ . '.' . __FUNCTION__, array('enable' => $debugMode));

                return $cleanData;
            }
        }

        //debugging
        $tableData[] = array('$cleanData', $cleanData);
        ZC_FirePHP::table($tableData, __CLASS__ . '.' . __FUNCTION__, array('enable' => $debugMode));

        return $cleanData;
    }

}
