<?php
/**
 * Description of ZC_View_Helper_XssFilter
 *
 * @author Kamarul Ariffin Ismail <kamarul.ismail@gmail.com>
 */

/**
 * @see ZC_View_Helper_XssFilter
 */
require_once 'Zend/View/Helper/Abstract.php';

class ZC_View_Helper_XssFilter extends Zend_View_Helper_Abstract {

    public function xssFilter($data = '', $debug = 0){
        //START FILTER
        $cleanData = trim($data);
        if($cleanData == ''){
            return $cleanData;
        }

        //
        $xssFilter = new ZC_XssFilter();
        $cleanData = $xssFilter->filterData($data, $debug);

        return $cleanData;
    }
}