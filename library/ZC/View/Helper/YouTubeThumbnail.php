<?php
/**
 * Description of ZC_View_Helper_YouTubeThumbnail
 *
 * @author Kamarul Ariffin Ismail <kamarul.ismail@gmail.com>
 */

/**
 * @see Zend_View_Helper_Abstract
 */
require_once 'Zend/View/Helper/Abstract.php';

class ZC_View_Helper_YouTubeThumbnail extends Zend_View_Helper_Abstract {

    public function youTubeThumbnail($url = '', $thumbnailSize = '', $debug = 0){
        $url = trim($url);

        //debugging
        $tableData = array(
            array('Variable', 'Data'),
            array('$url', $url),
            array('$thumbnailSize', $thumbnailSize),
        );

        //CHECK IF $url IS EMPTY
        if(empty($url)){
            ZC_FirePHP::table($tableData, __CLASS__ . '.' . __FUNCTION__, array('enable' => $debug));
            return null;
        }

        //DEFAULTS
        $youtubeID = 'youtubeID';
        $matched   = null;

        if(preg_match('/(youtube.com\/embed)/i', $url, $matched)){
            //debugging
            $tableData[] = array('(youtube.com\/embed)', $matched);

            //GET YOUTUBE ID
            $search    = array('https:', 'http:', '//www.youtube.com/embed/');
            $replace   = '';
            $youtubeID = str_replace($search, $replace, $url);
            #$youtubeID = str_replace('//www.youtube.com/embed/', '', $url);

        } //https://youtu.be/YDqyvev0bLE
        elseif(preg_match('/(youtu.be)/i', $url, $matched)){
            //debugging
            $tableData[] = array('(youtu.be)', $matched);

            //GET YOUTUBE ID
            $search    = array('https:', 'http:', '//youtu.be/');
            $replace   = '';
            $youtubeID = str_replace($search, $replace, $url);
        }
        elseif(preg_match('/(youtube.com\/watch)/i', $url, $matched)){
            //debugging
            $tableData[] = array('/(youtube.com\/watch)/i', $matched);

            //GET URL PARAM
            $urlQuery = parse_url($url, PHP_URL_QUERY);
            //debugging
            $tableData[] = array('urlQuery', $urlQuery);

            //CONVERT URL PARAM TO ARRAY
            parse_str($urlQuery, $urlParams);
            $tableData[] = array('urlParams', $urlParams);

            //GET YOUTUBE ID
            $youtubeID = isset($urlParams['v']) ? $urlParams['v'] : '';
        }

        //debugging
        $tableData[] = array('$youtubeID', $youtubeID);

        //Default Thumbnail Image, Full-Size (480x360)
        $thumbnailUrl = "//img.youtube.com/vi/{$youtubeID}/default.jpg";

        if($thumbnailSize === '0'){
            //Default Thumbnail Image, Medium Quality, Full-Size (480x360)
            $thumbnailUrl = "//img.youtube.com/vi/{$youtubeID}/0.jpg";
        }
        elseif($thumbnailSize === '1'){
            //1st Thumbnail Image, Small (120x90)
            $thumbnailUrl = "//img.youtube.com/vi/{$youtubeID}/1.jpg";
        }
        elseif($thumbnailSize === '2'){
            //2nd | Default Thumbnail Image, Small (120x90)
            $thumbnailUrl = "//img.youtube.com/vi/{$youtubeID}/2.jpg";
        }
        elseif($thumbnailSize === '3'){
            //3rd Thumbnail Image, Small (120x90)
            $thumbnailUrl = "//img.youtube.com/vi/{$youtubeID}/3.jpg";
        }
        elseif($thumbnailSize === 'mqdefault'){
            //Low Quality, 320px W x 180px H
            $thumbnailUrl = "//img.youtube.com/vi/{$youtubeID}/mqdefault.jpg";
        }
        elseif($thumbnailSize === 'hqdefault'){
            // High Quality, 480px W x 360px H
            $thumbnailUrl = "//img.youtube.com/vi/{$youtubeID}/hqdefault.jpg";
        }
        elseif($thumbnailSize === 'maxresdefault'){
            $thumbnailUrl = "//img.youtube.com/vi/{$youtubeID}/maxresdefault.jpg";
        }

        //debugging
        $tableData[] = array('$thumbnailUrl', $thumbnailUrl);
        ZC_FirePHP::table($tableData, __CLASS__ . '.' . __FUNCTION__, array('enable' => $debug));

        return $thumbnailUrl;
    }
}