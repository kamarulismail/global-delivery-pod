<?php
/**
 * Description of ZC_View_Helper_Minify
 *
 * @author Kamarul Ariffin Ismail <kamarul.ismail@gmail.com>
 */

/**
 * @see Zend_View_Helper_Abstract
 */
require_once 'Zend/View/Helper/Abstract.php';

class ZC_View_Helper_Minify extends Zend_View_Helper_Abstract {
    protected $_instance;

    /**
     * Get helper object/instance
     *
     * @return ZC_View_Helper_Minify
     */
    public function minify(){
        $_instance = isset($this->_instance) ? $this->_instance : null;
        if(!empty($_instance)){
            return $_instance;
        }

        //
        $this->_instance = $this;
        return $this->_instance;
    }

    public function min($options = array()){
        //DEBUGGING
        $debugMode = isset($options['debug']) ? $options['debug'] : 0;
        ZC_FirePHP::groupCollapsed(__CLASS__.'.'.__FUNCTION__, array('enable' => $debugMode));
        ZC_FirePHP::info($options, 'options', $debugMode);

        //PARAMETERS
        $type      = isset($options['type']) ? $options['type'] : '';
        $minify    = isset($options['minify']) ? $options['minify'] : 0;
        $overwrite = isset($options['overwrite']) ? $options['overwrite'] : 0;
        $baseUrl   = isset($options['baseUrl']) ? $options['baseUrl'] : '';
        $scripts   = isset($options['scripts']) ? $options['scripts'] : array();

        if(empty($type)){
            $minify = 0;
        }

        //DEFAULTS
        $minifyOutput   = '';

        //MINIFY DISABLED ($minify = 0)
        if(!$minify){
            ZC_FirePHP::warn('minify disabled', 'STATUS', $debugMode);
            foreach($scripts as $fileIndex => $file){
                $fileSrc = isset($file['src']) ? trim($file['src']) : '';
                if(empty($fileSrc)){
                    continue;
                }

                if($type == 'js'){
                    $minifyOutput .= '<script src="' . $fileSrc . '"></script>' . PHP_EOL;
                }
                elseif($type == 'css'){
                    $minifyOutput .= '<link href="' . $fileSrc . '" rel="stylesheet" type="text/css" />' . PHP_EOL;
                }
            }
            ZC_FirePHP::groupEnd($debugMode);
            return $minifyOutput;
        }

        //BENCHMARK
        $enableBenchmark = ($debugMode == 2) ? 1 : 0;
        if($enableBenchmark == 1){
            //MEMORY BENCHMARK
            $_ubench = new ZC_Ubench();
            $_ubench->start();

            //TIME BENCHMARK
            $time      = explode(' ', microtime());
            $timeStart = $time[1] + $time[0];
        }

        //MINIFY ENABLE ($minify = 1)
        $arrFiles = array();
        $pattern     = '/^('. str_replace('/', '\/', $baseUrl) .')/';
        $replacement = '';
        ZC_FirePHP::warn($pattern, '$pattern', $debugMode);

        //LOOP THROUGH FILES TO GET THE LATEST MODIFIED DATE
        foreach($scripts as $fileIndex => $file){
            $fileSrc = isset($file['src']) ? trim($file['src']) : '';
            if(empty($fileSrc)){
                continue;
            }
            #ZC_FirePHP::log($fileSrc, '$fileSrc.1', $debugMode);

            //
            $fileSrc = str_replace('//', '/', $fileSrc);
            #ZC_FirePHP::log($fileSrc, '$fileSrc.2', $debugMode);

            $fileSrc = preg_replace($pattern, $replacement, $fileSrc);
            #ZC_FirePHP::warn($fileSrc, '$fileSrc.3', $debugMode);
            $arrFiles[] = $fileSrc;
        }
        //GENERATE MINIFY URL
        $minUrl = rtrim($baseUrl, '/') . '/min/?f='.implode(',', $arrFiles);

        if($overwrite){
            ZC_FirePHP::warn('true', '$overwrite', $debugMode);
            $minUrl .= '&test';
        }
        ZC_FirePHP::info($minUrl, '$minUrl', $debugMode);

        if($type === 'js'){
            $minifyOutput = '<script type="text/javascript" src="' . $minUrl . '"></script>' . PHP_EOL;
        }
        elseif($type == 'css'){
            $minifyOutput = '<link href="' . $minUrl . '" rel="stylesheet" type="text/css" />' . PHP_EOL;
        }

        if($enableBenchmark == 1){
            //TIME BENCHMARK
            $time       = explode(' ', microtime());
            $timeEnd    = $time[1] + $time[0];
            $total_time = round(($timeEnd - $timeStart), 4);
            ZC_FirePHP::info($total_time, 'Task completed (in seconds)', $debugMode);

            //MEMORY BENCHMARK
            $_ubench->end();
            $_ubenchStat = array(
                        array('Elapsed Time', 'Memory Usage', 'Memory Peak Usage'),
                        array( $_ubench->getTime(), $_ubench->getMemoryUsage(), $_ubench->getMemoryPeak()),
                        array( $_ubench->getTime(true), $_ubench->getMemoryUsage(true), $_ubench->getMemoryPeak(true))
                    );
            ZC_FirePHP::table($_ubenchStat, 'Benchmark: Memory', array('enable' => $debugMode));
        }

        ZC_FirePHP::groupEnd($debugMode);
        return $minifyOutput;
    }

    public function minifyJS($options = array()){
        //DEBUGGING
        $debugMode = isset($options['debug']) ? $options['debug'] : 0;
        ZC_FirePHP::groupCollapsed(__CLASS__.'.'.__FUNCTION__, array('enable' => $debugMode));
        #ZC_FirePHP::info($options, 'options', $debugMode);

        //PARAMETERS
        $minify   = isset($options['minify']) ? $options['minify'] : 0;
        $basePath = isset($options['basePath']) ? $options['basePath'] : '';
        $baseUrl  = isset($options['baseUrl']) ? $options['baseUrl'] : '';
        $scripts  = isset($options['scripts']) ? $options['scripts'] : array();

        //DEFAULTS
        $minifyOutput   = '';

        //MINIFY DISABLED ($minify = 0)
        if(!$minify){
            ZC_FirePHP::warn('disabled', 'STATUS', $debugMode);
            foreach($scripts as $fileIndex => $file){
                $fileSrc = isset($file['src']) ? trim($file['src']) : '';
                if(empty($fileSrc)){
                    continue;
                }

                if(substr($baseUrl, -1) == '/' && substr($fileSrc, 0, 1) == '/'){
                    $fileSrc = ltrim($fileSrc, '/');
                }
                $minifyOutput .= '<script src="' . $baseUrl . $fileSrc . '"></script>' . PHP_EOL;
            }
            ZC_FirePHP::groupEnd($debugMode);
            return $minifyOutput;
        }

        //BENCHMARK
        $enableBenchmark = ($debugMode == 2) ? 1 : 0;
        if($enableBenchmark == 1){
            //MEMORY BENCHMARK
            $_ubench = new ZC_Ubench();
            $_ubench->start();

            //TIME BENCHMARK
            $time      = explode(' ', microtime());
            $timeStart = $time[1] + $time[0];
        }

        //MINIFY ENABLE ($minify = 1)
        $minifyFileName = 'minify.js';
        $startMinify    = 0;
        $arrFiles       = array();
        $minifyHash     = 0;

        //LOOP THROUGH FILES TO GET THE LATEST MODIFIED DATE
        $debugTableScripts = array(array('fileSrc', 'filemtime', 'mtime', 'isMinified'));
        foreach($scripts as $fileIndex => $file){
            $fileSrc = isset($file['src']) ? trim($file['src']) : '';
            if(empty($fileSrc)){
                continue;
            }

            //FILE INFO
            $fileSrc     = $basePath . $fileSrc;
            $file['src'] = $fileSrc;
            $filemtime   = filemtime($fileSrc);
            $isMinified  = isset($file['isMinified']) ? $file['isMinified'] : 0;

            //DEBUGGING
            if($debugMode == 2){
                $debugTableScripts[] = array($fileSrc, $filemtime, date ("F d Y H:i:s.", $filemtime), $isMinified);
            }

            //
            $arrFiles[] =  $file;
            $minifyHash = ($filemtime > $minifyHash) ? $filemtime : $minifyHash;
        }
        //DEBUG
        ZC_FirePHP::table($debugTableScripts, 'scripts', array('enable' => ($debugMode == 2)));
        ZC_FirePHP::info(array($minifyHash => date("d-m-Y H:i:s P", $minifyHash)), "latest filemtime", $debugMode);

        //OUTPUT FOLDER
        $outputFolder = $basePath . DIRECTORY_SEPARATOR . 'js';
        ZC_FirePHP::info($outputFolder, 'outputFolder', $debugMode);
        if (!file_exists($outputFolder)) {
            @mkdir($outputFolder, 0777, true);
            ZC_FirePHP::warn($outputFolder, 'CREATE', $debugMode);
        }

        //OUTPUT FILE
        $outputFile = $outputFolder . DIRECTORY_SEPARATOR . basename($minifyFileName, '.js') . "-min.js";
        ZC_FirePHP::info($outputFile, 'outputFile', $debugMode);

        //CACHE FOLDER
        $cacheFolder = isset($options['cacheFolder']) ? $options['cacheFolder'] : sys_get_temp_dir();
        #ZC_FirePHP::info($cacheFolder, 'cacheFolder', $debugMode);
        if (!file_exists($cacheFolder)) {
            @mkdir($cacheFolder, 0777, true);
        }

        //CACHE FILE
        $options['APPLICATION_PATH'] = APPLICATION_PATH;
        $cacheID   = sha1(serialize($options));
        $cacheFile = $cacheFolder . DIRECTORY_SEPARATOR . "minify_{$cacheID}_{$minifyHash}.js";
        ZC_FirePHP::info($cacheFile, 'cacheFile', $debugMode);

        //CHECK STATUS
        #$startMinify = (is_readable($outputFile) ? 0 : 1);
        $startMinify = (is_readable($cacheFile) ? 0 : 1);
        $overwrite   = isset($options['overwrite']) ? $options['overwrite'] : 0;
        if($overwrite){
            @unlink($cacheFile);
            ZC_FirePHP::warn("deleting {$cacheFile}", 'OVERWITE',$debugMode);

            $startMinify = 1;
        }

        //START MINIFY PROCESS
        if($startMinify){
            //START WRITING TO CACHE FILE
            file_put_contents($cacheFile, '/*! Generated on ' . date('Y-m-d H:i:s P') . ' */' . PHP_EOL);

            //LOAD LIBRARY CLASS
            $libraryPath = realpath(APPLICATION_PATH . DIRECTORY_SEPARATOR . '..') . DIRECTORY_SEPARATOR . 'library' . DIRECTORY_SEPARATOR . 'ZC';
            Zend_Loader::loadFile('JSMin.php', $libraryPath, true);

            //START ACTUAL MINIFY
            $debugTableMinify = array(array('File', 'before(bytes)', 'after(bytes)', '%'));
            foreach($arrFiles as $file){
                //GET FILE CONTENT
                $fileSrc  = isset($file['src']) ? trim($file['src']) : '';
                $fileData = file_get_contents($fileSrc);

                //IS FILE ALREADY MINIFIED
                $isMinified = isset($file['isMinified']) ? $file['isMinified'] : 0;

                //MINIFY DATA
                $minifyData = ($isMinified) ? $fileData : JSMin::minify($fileData);
                $minifyData = '/*! File: ' . str_replace($basePath, '', $fileSrc) . ' */' . PHP_EOL . $minifyData . PHP_EOL;

                //WRITE TO CACHE FILE
                @file_put_contents($cacheFile, $minifyData, FILE_APPEND);

                //DEBUG
                if($debugMode == 2){
                    $sizeBefore = strlen($fileData);
                    $sizeAfter  = strlen($minifyData);
                    $sizePercentage = number_format(((($sizeBefore - $sizeAfter) / $sizeBefore) * 100), 4);

                    $debugTableMinify[] = array($fileSrc, $sizeBefore, $sizeAfter, $sizePercentage);
                }
            }

            //COPY
            @copy($cacheFile, $outputFile);

            //DEBUG
            ZC_FirePHP::table($debugTableMinify, 'Benchmark: Minify', array('enable' => ($debugMode == 2)));
        }

        //GENERATE OUTPUT
        $outputUrl    = rtrim($baseUrl, '/') . '/' . basename($outputFolder) . '/' . basename($outputFile) . '?u=' . $minifyHash;
        $minifyOutput = '<script src="' . $outputUrl . '"></script>' . PHP_EOL;

        if($enableBenchmark == 1){
            //TIME BENCHMARK
            $time       = explode(' ', microtime());
            $timeEnd    = $time[1] + $time[0];
            $total_time = round(($timeEnd - $timeStart), 4);
            ZC_FirePHP::info($total_time, 'Task completed (in seconds)', $debugMode);

            //MEMORY BENCHMARK
            $_ubench->end();
            $_ubenchStat = array(
                        array('Elapsed Time', 'Memory Usage', 'Memory Peak Usage'),
                        array( $_ubench->getTime(), $_ubench->getMemoryUsage(), $_ubench->getMemoryPeak()),
                        array( $_ubench->getTime(true), $_ubench->getMemoryUsage(true), $_ubench->getMemoryPeak(true))
                    );
            ZC_FirePHP::table($_ubenchStat, 'Benchmark: Memory', array('enable' => $debugMode));
        }

        ZC_FirePHP::groupEnd($debugMode);
        return $minifyOutput;
    }

    public function minifyCSS($options = array()){
        //DEBUGGING
        $debugMode = isset($options['debug']) ? $options['debug'] : 0;
        ZC_FirePHP::groupCollapsed(__CLASS__.'.'.__FUNCTION__, array('enable' => $debugMode));
        #ZC_FirePHP::info($options, 'options', $debugMode);

        //PARAMETERS
        $minify   = isset($options['minify']) ? $options['minify'] : 0;
        $basePath = isset($options['basePath']) ? $options['basePath'] : '';
        $baseUrl  = isset($options['baseUrl']) ? $options['baseUrl'] : '';
        $scripts  = isset($options['scripts']) ? $options['scripts'] : array();

        //DEFAULTS
        $minifyOutput   = '';

        //MINIFY DISABLED ($minify = 0)
        if(!$minify){
            ZC_FirePHP::warn('minify disabled', 'STATUS', $debugMode);
            foreach($scripts as $fileIndex => $file){
                $fileSrc = isset($file['src']) ? trim($file['src']) : '';
                if(empty($fileSrc)){
                    continue;
                }

                if(substr($baseUrl, -1) == '/' && substr($fileSrc, 0, 1) == '/'){
                    $fileSrc = ltrim($fileSrc, '/');
                }
                $minifyOutput .= '<link href="' . $baseUrl . $fileSrc . '" rel="stylesheet">' . PHP_EOL;
            }
            ZC_FirePHP::groupEnd($debugMode);
            return $minifyOutput;
        }

        //
        $enableBenchmark = ($debugMode == 2) ? 1 : 0;
        if($enableBenchmark == 1){
            //MEMORY BENCHMARK
            $_ubench = new ZC_Ubench();
            $_ubench->start();

            //TIME BENCHMARK
            $time      = explode(' ', microtime());
            $timeStart = $time[1] + $time[0];
        }

        //MINIFY ENABLE ($minify = 1)
        $minifyFileName = 'minify.css';
        $startMinify    = 0;
        $arrFiles       = array();
        $minifyHash     = 0;

        //LOOP THROUGH FILES TO GET THE LATEST MODIFIED DATE
        $debugTableScripts = array(array('fileSrc', 'filemtime', 'mtime', 'isMinified'));
        foreach($scripts as $fileIndex => $file){
            $fileSrc = isset($file['src']) ? trim($file['src']) : '';
            if(empty($fileSrc)){
                continue;
            }

            //FILE INFO
            $fileSrc     = $basePath . $fileSrc;
            $file['src'] = $fileSrc;
            $filemtime   = filemtime($fileSrc);
            $isMinified  = isset($file['isMinified']) ? $file['isMinified'] : 0;

            //DEBUGGING
            if($debugMode == 2){
                $debugTableScripts[] = array($fileSrc, $filemtime, date ("F d Y H:i:s.", $filemtime), $isMinified);
            }

            //
            $arrFiles[] =  $file;
            $minifyHash = ($filemtime > $minifyHash) ? $filemtime : $minifyHash;
        }
        //DEBUG
        ZC_FirePHP::table($debugTableScripts, 'scripts', array('enable' => ($debugMode == 2)));
        ZC_FirePHP::info(array($minifyHash => date("d-m-Y H:i:s P", $minifyHash)), "latest filemtime", $debugMode);

        //OUTPUT FOLDER
        $outputFolder = $basePath . DIRECTORY_SEPARATOR . 'css';
        #ZC_FirePHP::info($outputFolder, 'outputFolder', $debugMode);
        if (!file_exists($outputFolder)) {
            @mkdir($outputFolder, 0777, true);
            ZC_FirePHP::warn($outputFolder, 'CREATE', $debugMode);
        }

        //OUTPUT FILE
        $outputFile = $outputFolder . DIRECTORY_SEPARATOR . basename($minifyFileName, '.css') . '-min.css';
        ZC_FirePHP::info($outputFile, 'outputFile', $debugMode);

        //CACHE FOLDER
        $cacheFolder = isset($options['cacheFolder']) ? $options['cacheFolder'] : sys_get_temp_dir();
        #ZC_FirePHP::info($cacheFolder, 'cacheFolder', $debugMode);
        if (!file_exists($cacheFolder)) {
            @mkdir($cacheFolder, 0777, true);
        }

        //CACHE FILE
        $options['APPLICATION_PATH'] = APPLICATION_PATH;
        $cacheID   = sha1(serialize($options));
        $cacheFile = $cacheFolder . DIRECTORY_SEPARATOR . "minify_{$cacheID}_{$minifyHash}.css";
        ZC_FirePHP::info($cacheFile, 'cacheFile', $debugMode);

        //CHECK STATUS
        #$startMinify = (is_readable($outputFile) ? 0 : 1);
        $startMinify = (is_readable($cacheFile) ? 0 : 1);
        $overwrite   = isset($options['overwrite']) ? $options['overwrite'] : 0;
        if($overwrite){
            @unlink($cacheFile);
            ZC_FirePHP::warn("deleting {$cacheFile}", 'OVERWITE',$debugMode);

            $startMinify = 1;
        }

        //START MINIFY PROCESS
        if($startMinify){
            //START WRITING TO CACHE FILE
            file_put_contents($cacheFile, '/*! Generated on ' . date('Y-m-d H:i:s P') . ' */' . PHP_EOL);

            //LOAD LIBRARY CLASS
            $libraryPath = realpath(APPLICATION_PATH . DIRECTORY_SEPARATOR . '..') . DIRECTORY_SEPARATOR . 'library' . DIRECTORY_SEPARATOR . 'ZC';
            Zend_Loader::loadFile('CSSmin.php', $libraryPath, true);
            $cssMin = new CSSmin();

            //START ACTUAL MINIFY
            $debugTableMinify = array(array('File', 'before(bytes)', 'after(bytes)', '%'));
            foreach($arrFiles as $file){
                //GET FILE CONTENT
                $fileSrc  = isset($file['src']) ? trim($file['src']) : '';
                $fileData = file_get_contents($fileSrc);

                //IS FILE ALREADY MINIFIED
                $isMinified = isset($file['isMinified']) ? $file['isMinified'] : 0;

                //MINIFY DATA
                $minifyData = ($isMinified) ? $fileData : $cssMin->run($fileData);
                $minifyData = '/*! File: ' . str_replace($basePath, '', $fileSrc) . ' */' . PHP_EOL . $minifyData . PHP_EOL;

                @file_put_contents($cacheFile, $minifyData, FILE_APPEND);

                //DEBUG
                if($debugMode == 2){
                    $sizeBefore = strlen($fileData);
                    $sizeAfter  = strlen($minifyData);
                    $sizePercentage = number_format(((($sizeBefore - $sizeAfter) / $sizeBefore) * 100), 4);

                    $debugTableMinify[] = array($fileSrc, $sizeBefore, $sizeAfter, $sizePercentage);
                }
            }

            //COPY
            @copy($cacheFile, $outputFile);

            //DEBUG
            ZC_FirePHP::table($debugTableMinify, 'Benchmark: Minify', array('enable' => ($debugMode == 2)));
        }

        //
        $outputUrl    = rtrim($baseUrl, '/') . '/' . basename($outputFolder) . '/' . basename($outputFile) . '?u=' . $minifyHash;
        $minifyOutput = '<link href="' . $outputUrl . '" rel="stylesheet">' . PHP_EOL;

        if($enableBenchmark == 1){
            //TIME BENCHMARK
            $time       = explode(' ', microtime());
            $timeEnd    = $time[1] + $time[0];
            $total_time = round(($timeEnd - $timeStart), 4);
            ZC_FirePHP::info($total_time, 'Task completed (in seconds)', $debugMode);

            //MEMORY BENCHMARK
            $_ubench->end();
            $_ubenchStat = array(
                        array('Elapsed Time', 'Memory Usage', 'Memory Peak Usage'),
                        array( $_ubench->getTime(), $_ubench->getMemoryUsage(), $_ubench->getMemoryPeak()),
                        array( $_ubench->getTime(true), $_ubench->getMemoryUsage(true), $_ubench->getMemoryPeak(true))
                    );
            ZC_FirePHP::table($_ubenchStat, 'Benchmark: Memory', array('enable' => $debugMode));
        }

        ZC_FirePHP::groupEnd($debugMode);
        return $minifyOutput;
    }

}