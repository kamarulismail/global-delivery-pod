<?php
//appSettings
/**
 * Description of ZC_View_Helper_ApplicationSetting
 *
 * @author Kamarul Ariffin Ismail <kamarul.ismail@gmail.com>
 */

/**
 * @see Zend_View_Helper_Abstract
 */
require_once 'Zend/View/Helper/Abstract.php';

class ZC_View_Helper_ApplicationSetting extends Zend_View_Helper_Abstract {

    public function applicationSetting($section = null){
        $appSettings = array();

        if(Zend_Registry::isRegistered('appSettings')){
            $appSettings = Zend_Registry::get('appSettings');
        }

        if(!is_null($section)){
            $appSettings = isset($appSettings[$section]) ? $appSettings[$section] : array();
        }

        return $appSettings;
    }

}

