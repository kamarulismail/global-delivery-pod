<?php
/**
 * Description of ZC_View_Helper_BaseUrl
 *
 * @author Kamarul Ariffin Ismail <kamarul.ismail@gmail.com>
 */

/**
 * @see Zend_View_Helper_Abstract
 */
require_once 'Zend/View/Helper/Abstract.php';

class ZC_View_Helper_BaseUrl extends Zend_View_Helper_Abstract {

    public function baseUrl($url = '', $debug = 0){
        $url = trim($url);

        //CHECK IF $url IS EMPTY
        if(empty($url)){
            return $url;
        }

        //debugging
        $tableData = array(
            array('Variable', 'Data'),
            array('$url', $url),
        );

        //GET BASE URL
        $baseUrl = isset($this->view->baseUrl) ? $this->view->baseUrl : '';
        //debugging
        $tableData[] = array('$baseUrl', $baseUrl);

        //
        $newUrl  = $url;
        $matched = '';
        if(preg_match('/^#/', $url, $matched)){
            //START WITH '#'
            $tableData[] = array('^#', $matched);
        }
        elseif(preg_match('/^htt(p|ps)/', $url, $matched)){
            //START WITH 'HTTP' / 'HTTPS'
            $tableData[] = array('^htt(p|ps)', $matched);
        }
        elseif($url == '/'){
            $tableData[] = array('only /', $url);
            $newUrl = rtrim($baseUrl, '/') . $url;
        }
        else{
            //START WITH '/'
            $tableData[] = array('^/', $url);
            $newUrl = rtrim($baseUrl, '/') . '/' . ltrim($url, '/');
        }

        //debugging
        $tableData[] = array('$newUrl', $newUrl);
        ZC_FirePHP::table($tableData, __CLASS__ . '.' . __FUNCTION__, array('enable' => $debug));

        return $newUrl;
    }
}