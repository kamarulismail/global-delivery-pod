<?php
//appSettings
/**
 * Description of ZC_View_Helper_DebugSetting
 *
 * @author Kamarul Ariffin Ismail <kamarul.ismail@gmail.com>
 */

/**
 * @see Zend_View_Helper_Abstract
 */
require_once 'Zend/View/Helper/Abstract.php';

class ZC_View_Helper_DebugSetting extends Zend_View_Helper_Abstract {

    public function debugSetting($section = null){
        $debugSettings = array();

        if(Zend_Registry::isRegistered('debugSettings')){
            $debugSettings = Zend_Registry::get('debugSettings');
        }

        if(!is_null($section)){
            $debugSettings = isset($debugSettings[$section]) ? $debugSettings[$section] : array();
        }

        return $debugSettings;
    }

}

