<?php
/**
 * Description of ZC_View_Helper_DateFormatter
 *
 * @author Kamarul Ariffin Ismail <kamarul.ismail@gmail.com>
 */

/**
 * @see Zend_View_Helper_Abstract
 */
require_once 'Zend/View/Helper/Abstract.php';

class ZC_View_Helper_DateFormatter extends Zend_View_Helper_Abstract {

    function dateFormatter($options = array()){
        $dateTime = isset($options['dateTime']) ? trim($options['dateTime']) : '';
        if(empty($dateTime)){
            return $dateTime;
        }

        //DATE FORMAT
        $inputFormat = isset($options['inputFormat']) ? trim($options['inputFormat']) : 'Y-m-d H:i:s';
        $ouputFormat = isset($options['outputFormat']) ? trim($options['outputFormat']) : 'd/m/Y h:i a';

        //
        $zendDate = new Zend_Date();
        $zendDate->setOptions(array('format_type' => 'php'));

        //VALIDATE IF VALID DATE
        if(!$zendDate->isDate($dateTime, $inputFormat)){
            return $dateTime;
        }

        //SET DATE
        $zendDate->set($dateTime, $inputFormat);

        //GET NEW DATE FORMAT
        $dateFormatted = $zendDate->get($ouputFormat);
        return $dateFormatted;
    }
}