<?php
/**
 * Description of ZC_View_Filter_MinifyHTML
 *
 * @author Kamarul Ariffin Ismail <kamarul.ismail@gmail.com>
 */

/**
 * @see Zend_Filter_Interface
 */
require_once 'Zend/Filter/Interface.php';

class ZC_View_Filter_MinifyHTML implements Zend_Filter_Interface {
    protected $_html;
    protected $_isXhtml = null;
    protected $_replacementHash = null;
    protected $_placeholders = array();
    protected $_cssMinifier = null;
    protected $_jsMinifier = null;
    protected $_jsCleanComments = true;
    protected $_jsCleanCData = false;
    protected $_debug;

    /**
     * Returns the result of filtering $value
     *
     * @param  mixed $value
     * @throws Zend_Filter_Exception If filtering $value is impossible
     * @return mixed
     */
    public function filter($value) {
        $sizeBefore = strlen($value);
        if(empty($sizeBefore)){
            return $value;
        }

        //DEBUG
        $debug = 0;
        if(Zend_Registry::isRegistered('ZC_DEBUG_MinifyHTML')){
            $debug        = Zend_Registry::get('ZC_DEBUG_MinifyHTML');
            $this->_debug = $debug;
        }

        if($debug){
            //MEMORY BENCHMARK
            $_ubench = new ZC_Ubench();
            $_ubench->start();
        }

        //V2
        $this->_html       = str_replace("\r\n", "\n", trim($value));
        $this->_jsMinifier = array($this,'minifyJS');
        $minifed           = $this->process();

        //
        if($debug){
            $sizeAfter      = strlen($minifed);
            $sizePercentage = number_format(((($sizeBefore - $sizeAfter) / $sizeBefore) * 100), 4);

            //MEMORY BENCHMARK
            $_ubench->end();

            $statMinify = array(
                array('Before (bytes)', 'After (bytes)', '%'),
                array($sizeBefore, $sizeAfter, $sizePercentage),
                array('Elapsed Time', 'Memory Usage', 'Memory Peak Usage'),
                array( $_ubench->getTime(), $_ubench->getMemoryUsage(), $_ubench->getMemoryPeak()),
                array( $_ubench->getTime(true), $_ubench->getMemoryUsage(true), $_ubench->getMemoryPeak(true))
            );
            ZC_FirePHP::table($statMinify, 'Benchmark: ' . __CLASS__ , array('enable' => $debug));
        }

        return $minifed;
    }

     /**
     * Minify the markeup given in the constructor
     *
     * @return string
     */
    public function process() {
        if ($this->_isXhtml === null) {
            $this->_isXhtml = (false !== strpos($this->_html, '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML'));
        }
        $this->_replacementHash = 'MINIFYHTML' . md5($_SERVER['REQUEST_TIME']);
        $this->_placeholders    = array();

        // replace SCRIPTs (and minify) with placeholders
        $this->_html = preg_replace_callback(
                '/(\\s*)<script(\\b[^>]*?>)([\\s\\S]*?)<\\/script>(\\s*)/i'
                , array($this, '_removeScriptCB')
                , $this->_html);

        // replace STYLEs (and minify) with placeholders
        $this->_html = preg_replace_callback(
                '/\\s*<style(\\b[^>]*>)([\\s\\S]*?)<\\/style>\\s*/i'
                , array($this, '_removeStyleCB')
                , $this->_html);

        // remove HTML comments (not containing IE conditional comments).
        $this->_html = preg_replace_callback(
                '/<!--([\\s\\S]*?)-->/'
                , array($this, '_commentCB')
                , $this->_html);

        // replace PREs with placeholders
        $this->_html = preg_replace_callback('/\\s*<pre(\\b[^>]*?>[\\s\\S]*?<\\/pre>)\\s*/i'
                , array($this, '_removePreCB')
                , $this->_html);

        // replace TEXTAREAs with placeholders
        $this->_html = preg_replace_callback(
                '/\\s*<textarea(\\b[^>]*?>[\\s\\S]*?<\\/textarea>)\\s*/i'
                , array($this, '_removeTextareaCB')
                , $this->_html);

        // trim each line.
        // @todo take into account attribute values that span multiple lines.
        $this->_html = preg_replace('/^\\s+|\\s+$/m', '', $this->_html);

        // remove ws around block/undisplayed elements
        $this->_html = preg_replace('/\\s+(<\\/?(?:area|base(?:font)?|blockquote|body'
                . '|caption|center|col(?:group)?|dd|dir|div|dl|dt|fieldset|form'
                . '|frame(?:set)?|h[1-6]|head|hr|html|legend|li|link|map|menu|meta'
                . '|ol|opt(?:group|ion)|p|param|t(?:able|body|head|d|h||r|foot|itle)'
                . '|ul)\\b[^>]*>)/i', '$1', $this->_html);

        // remove ws outside of all elements
        $this->_html = preg_replace(
                '/>(\\s(?:\\s*))?([^<]+)(\\s(?:\s*))?</'
                , '>$1$2$3<'
                , $this->_html);

        // use newlines before 1st attribute in open tags (to limit line lengths)
        #$this->_html = preg_replace('/(<[a-z\\-]+)\\s+([^>]+>)/i', "$1\n$2", $this->_html);

        //remove newline/linebreak
        $this->_html = preg_replace('/[\r\n\t]+/', '', $this->_html);

        //remove whitespace between tags
        $this->_html = preg_replace('/>[\s]+</', '><', $this->_html);

        // fill placeholders
        $this->_html = str_replace(
                array_keys($this->_placeholders)
                , array_values($this->_placeholders)
                , $this->_html
        );

        // issue 229: multi-pass to catch scripts that didn't get replaced in textareas
        $this->_html = str_replace(
                array_keys($this->_placeholders)
                , array_values($this->_placeholders)
                , $this->_html
        );

        return $this->_html;
    }

    protected function _commentCB($m) {
        return (0 === strpos($m[1], '[') || false !== strpos($m[1], '<![')) ? $m[0] : '';
    }

    protected function _reservePlace($content) {
        $placeholder = '%' . $this->_replacementHash . count($this->_placeholders) . '%';
        $this->_placeholders[$placeholder] = $content;
        return $placeholder;
    }

    protected function _removePreCB($m) {
        return $this->_reservePlace("<pre{$m[1]}");
    }

    protected function _removeTextareaCB($m) {
        return $this->_reservePlace("<textarea{$m[1]}");
    }

    protected function _removeStyleCB($m) {
        $openStyle = "<style{$m[1]}";
        $css = $m[2];

        // remove HTML comments
        $css = preg_replace('/(?:^\\s*<!--|-->\\s*$)/', '', $css);

        // remove CDATA section markers
        $css = $this->_removeCdata($css);

        // minify
        $minifier = $this->_cssMinifier ? $this->_cssMinifier : 'trim';
        $css      = call_user_func($minifier, $css);
        return $this->_reservePlace($this->_needsCdata($css) ? "{$openStyle}/*<![CDATA[*/{$css}/*]]>*/</style>" : "{$openStyle}{$css}</style>");
    }

    protected function _removeScriptCB($m) {
        //DEBUG
        $tableData = array(
            array('Variable', 'Data'),
            array('$m', $m),
        );

        $openScript = "<script{$m[2]}";
        $js = $m[3];

        // whitespace surrounding? preserve at least one space
        $ws1 = ($m[1] === '') ? '' : ' ';
        $ws2 = ($m[4] === '') ? '' : ' ';

        //check flag SKIP Minify
        $matches = array();
        $flagSkipMinify = preg_match('/(flag-minify-skip="1")/i', $m[2], $matches);
        $tableData[] = array('$m[2]', $m[2]);
        $tableData[] = array('$matches', $matches);
        $tableData[] = array('$flagSkipMinify', $flagSkipMinify);

        if($flagSkipMinify){
            $_minifyData = trim($m[0]);

            //DEBUGGING
            $tableData[] = array('$_minifyData', $_minifyData);
            ZC_FirePHP::table($tableData, __CLASS__.'.'.__FUNCTION__, array('enable' => $this->_debug));

            return $this->_reservePlace($_minifyData);
        }

        // remove HTML comments (and ending "//" if present)
        if ($this->_jsCleanComments) {
            $js = preg_replace('/(?:^\\s*<!--\\s*|\\s*(?:\\/\\/)?\\s*-->\\s*$)/', '', $js);
        }

        //check flag to preserved CDATA
        $matches = array();
        $flagPreservedCData = preg_match('/(flag-minify-preserved-cdata="1")/i', $m[2], $matches);
        $tableData[] = array('$m[2]', $m[2]);
        $tableData[] = array('$matches', $matches);
        $tableData[] = array('$flagPreservedCData', $flagPreservedCData);

        // remove CDATA section markers
        $js = $this->_removeCdata($js);

        // minify
        $minifier = $this->_jsMinifier ? $this->_jsMinifier : 'trim';
        $js       = call_user_func($minifier, $js);

        //check flag to preserved CDATA
        $_needsCdata = ($flagPreservedCData) ? $flagPreservedCData : $this->_needsCdata($js);

        //DEBUGGING
        $tableData[] = array('$_needsCdata', $_needsCdata);

        //
        $_minifyData = "{$ws1}{$openScript}{$js}</script>{$ws2}";
        if($_needsCdata){
            $_minifyData = "{$ws1}{$openScript}/*<![CDATA[*/{$js}/*]]>*/</script>{$ws2}";
        }

        //DEBUGGING
        $tableData[] = array('$_minifyData', $_minifyData);
        ZC_FirePHP::table($tableData, __CLASS__.'.'.__FUNCTION__, array('enable' => $this->_debug));

        return $this->_reservePlace($_minifyData);
        #return $this->_reservePlace($_needsCdata ? "{$ws1}{$openScript}/*<![CDATA[*/{$js}/*]]>*/</script>{$ws2}" : "{$ws1}{$openScript}{$js}</script>{$ws2}");
    }

    protected function _removeCdata($str) {
        return (false !== strpos($str, '<![CDATA[')) ? str_replace(array('<![CDATA[', ']]>'), '', $str) : $str;
    }

    protected function _needsCdata($str) {
        $status = ($this->_isXhtml && preg_match('/(?:[<&]|\\-\\-|\\]\\]>)/', $str));
        return $status;
    }

    protected function minifyJS($str){
        if(empty($str)){
            return $str;
        }
        //LOAD LIBRARY CLASS
        $libraryPath = realpath(APPLICATION_PATH . DIRECTORY_SEPARATOR . '..') . DIRECTORY_SEPARATOR . 'library' . DIRECTORY_SEPARATOR . 'ZC';
        Zend_Loader::loadFile('JSMin.php', $libraryPath, true);
        $minifyStr = JSMin::minify($str);
        $minifyStr = preg_replace('/[\r\n\t]+/', ' ', $minifyStr);

        //DEBUG
        $tableData = array(
            array('Variable', 'Data'),
            array('Before Minify', $str),
            array('Before Minify', strlen($str)),
            array('After Minify', $minifyStr),
            array('After Minify', strlen($minifyStr)),
        );
        ZC_FirePHP::table($tableData, __CLASS__.'.'.__FUNCTION__, array('enable' => $this->_debug));

        return $minifyStr;
    }

}
