<?php

/**
 * Description of ZC_Controller_Action
 *
 * @author Kamarul Ariffin Ismail <kamarul.ismail@gmail.com>
 */
class ZC_Controller_Action extends Zend_Controller_Action {
    protected $_layout;
    protected $_assetUrl;
    protected $_baseUrl;
    protected $_moduleUrl;
    protected $_appSettings;
    protected $_debugSettings;
    protected $_uniquePageIdentifier;
    protected $_pageMetaData = array();
    protected $_pageGenerateTimeStart;
    protected $_pageGenerateTimeEnd;
    protected $_siteLanguage;
    protected $_siteName;
    protected $_pageTitle;

    public function init(){
        parent::init();

        //GET DEBUG SETTINGS
        $this->getDebugSettings();

        //SET LAYOUT
        $this->setLayout();

        //REGISTER VIEW HELPER
        $this->registerViewHelper();

        //REGISTER VIEW FILTER
        $this->registerViewFilter();

        //UNIQUE PAGE IDENTIFIER
        $this->_uniquePageIdentifier = $this->getUniquePageIdentifier();

        //CSRF TOKEN
        #$this->_csrfToken = $this->generateCSRFToken();
        $this->_csrfToken = $this->getCSRFToken();
    }

    public function preDispatch() {
        parent::preDispatch();

        //BENCHMARK
        $this->debugUBench('start');

        //PROCESS TIME - START
        $this->_pageProcessStartTime = $this->getMicroTime();
    }

    public function postDispatch() {
        parent::postDispatch();

        //PAGE TITLE
        $this->setPageTitle();

        //PAGE METADATA
        #$this->setPageMetaData();

        //ROUTER DEBUG
        $this->debugRouter();

        //DEBUGGING
        $this->debugPHPSession('_SESSION');

        //VIEW VARIABLES
        $this->setDefaultViewVariables();

        //BENCHMARK
        $this->debugUBench('stop');
    }

    /**
     * Get microtime
     *
     * @return int $microtime
     */
    protected function getMicroTime(){
        $time      = explode(' ', microtime());
        $microtime = $time[1] + $time[0];
        return $microtime;
    }

    /**
     * Set default view variables
     */
    protected function setDefaultViewVariables(){
        $this->view->assetUrl             = $this->_assetUrl;
        $this->view->baseUrl              = $this->_baseUrl;
        $this->view->moduleUrl            = $this->_moduleUrl;
        $this->view->uniquePageIdentifier = $this->_uniquePageIdentifier;
        $this->view->csrfToken            = $this->_csrfToken;
        $this->view->siteLanguage         = $this->_siteLanguage;

        //DEBUG
        $debugMode = isset($this->_debugSettings[__FUNCTION__]['enable']) ? $this->_debugSettings[__FUNCTION__]['enable'] : 0;
        if(!$debugMode){
            return;
        }
        $tableData = array(
            array('Variable', 'Data'),
            array('$this->view->assetUrl', $this->view->assetUrl),
            array('$this->view->baseUrl', $this->view->baseUrl),
            array('$this->view->uniquePageIdentifier', $this->view->uniquePageIdentifier),
            array('$this->view->csrfToken', $this->view->csrfToken),
            array('$this->view->siteLanguage', $this->view->siteLanguage),
        );
        ZC_FirePHP::table($tableData, __CLASS__.'.'.__FUNCTION__);
    }

    /**
     * Get Zend Layout instance/object
     *
     * @return Zend_Layout
     */
    protected function getZendLayout(){
        return $this->_helper->layout();
    }

    /**
     * Set page layout file
     *
     * @param string $layout
     * @param int $debugMode
     */
    protected function setLayout($layout = '', $debugMode = 0){
        $layout = empty($layout) ? $this->_layout : $layout;
        if(!empty($layout)){
            //CHANGE DEFAULT LAYOUT
            $this->getZendLayout()->setLayout($layout);
        }

        //DEBUG
        $debugMode = isset($this->_debugSettings[__FUNCTION__]['enable']) ? $this->_debugSettings[__FUNCTION__]['enable'] : $debugMode;
        if (!$debugMode) {
            return;
        }
        $tableData = array(
            array('Variable', 'Data'),
            array('$this->_layout', $this->_layout),
            array('$_layout', $layout),
        );
        ZC_FirePHP::table($tableData, __CLASS__ . '.' . __FUNCTION__);
    }

    /**
     * Disable page layout
     */
    protected function disableLayout(){
        //DISABLE LAYOUT
        $this->getZendLayout()->disableLayout();
    }

    /**
     * Get page view object
     *
     * @return Zend_View
     */
    protected function getView(){
        return $this->view;
    }

    /**
     * Get Action Helper ViewRenderer
     *
     * @return Zend_Controller_Action_Helper_ViewRenderer
     */
    protected function getActionHelperViewRenderer(){
        return $this->_helper->viewRenderer;
    }

    /**
     * Set page view file
     *
     * @param type $view
     */
    protected function setView($view){
        $this->getActionHelperViewRenderer()->setRender($view);

        //DEBUG
        $debugMode = isset($this->_debugSettings[__FUNCTION__]['enable']) ? $this->_debugSettings[__FUNCTION__]['enable'] : 0;
        ZC_FirePHP::info($view, __CLASS__.'.'.__FUNCTION__, $debugMode);
    }

    /**
     * Disable view
     */
    protected function disableView(){
        $this->getActionHelperViewRenderer()->setNoRender();
    }

    /**
     * HTTP Authentication
     *
     * @param type $options
     * @return boolean
     */
    protected function validateHttpAuth($options = array()) {
        //FROM APPLICATION.INI
        $_appSettings = array();
        if (Zend_Registry::isRegistered('appSettings')) {
            $_appSettings = Zend_Registry::get('appSettings');
        }
        $httpAuthSettings = isset($_appSettings['httpAuth']) ? $_appSettings['httpAuth'] : array();
        $defaultUsername  = isset($httpAuthSettings['username']) ? $httpAuthSettings['username'] : 'edge';
        $defaultPassword  = isset($httpAuthSettings['password']) ? $httpAuthSettings['password'] : 'edge@asia';
        $defaultEnable    = isset($httpAuthSettings['enable']) ? $httpAuthSettings['enable'] : 0;

        //FROM OPTIONS
        $username = (isset($options['username'])) ? $options['username'] : $defaultUsername;
        $password = (isset($options['password'])) ? $options['password'] : $defaultPassword;
        $isEnable = (isset($options['enable'])) ? $options['enable'] : $defaultEnable;

        //
        if (!$isEnable) {
            return true;
        }

        $PHP_AUTH_USER = isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_USER'] : '';
        $PHP_AUTH_PW   = isset($_SERVER['PHP_AUTH_PW']) ? $_SERVER['PHP_AUTH_PW'] : '';

        if (!empty($PHP_AUTH_USER) && !empty($PHP_AUTH_PW)) {
            if ($PHP_AUTH_USER == $username && $PHP_AUTH_PW == $password) {
                return true;
            }
        }

        header('WWW-Authenticate: Basic realm="HTTP Authorization!"');
        header('HTTP/1.0 401 Unauthorized');
        echo 'Invalid Username or Password.';
        exit();
    }

    /**
     * Get View Helper HeadTitle
     *
     * @return Zend_View_Helper_HeadTitle
     */
    protected function getViewHelperHeadTitle(){
        return $this->view->headTitle();
    }

    /**
     * Set page title
     */
    protected function setPageTitle(){
        $siteName = isset($this->_siteName) ? $this->_siteName : '';
        $this->view->siteName_ = $siteName;

        $pageTitle = isset($this->_pageTitle) ? $this->_pageTitle : '';
        if(is_array($pageTitle)){
            $pageTitle = implode(' - ', $pageTitle);
        }
        $pageTitle              = trim($pageTitle, ' - ');
        $this->view->pageTitle_ = $pageTitle;

        $headTitle = $siteName;
        if (!empty($pageTitle)) {
            $headTitle = empty($headTitle) ? $pageTitle : $headTitle . ' - ' . $pageTitle;
        }
        $this->getViewHelperHeadTitle()->headTitle($headTitle, Zend_View_Helper_Placeholder_Container_Abstract::SET);

        //DEBUG
        $debugMode = isset($this->_debugSettings[__FUNCTION__]['enable']) ? $this->_debugSettings[__FUNCTION__]['enable'] : 0;
        $tableData = array(
            array('Variable', 'Data'),
            array('$this->view->siteName_', $this->view->siteName_),
            array('$this->_pageTitle', $this->_pageTitle),
            array('$this->view->pageTitle_', $this->view->pageTitle_),
            array('$headTitle', $headTitle)
        );
        ZC_FirePHP::table($tableData, __CLASS__.'.'.__FUNCTION__, array('enable' => $debugMode));
    }

    protected function appendPageTitle($pageTitle){
        $_pageTitle = $this->_pageTitle;

        if(!is_array($_pageTitle)){
            $_pageTitle = array($_pageTitle);
        }

        if(is_array($pageTitle)){
            $pageTitleTemp = array_merge($_pageTitle, $pageTitle);
            $_pageTitle    = $pageTitleTemp;
        }
        else{
            array_push($_pageTitle, $pageTitle);
        }

        //
        $this->_pageTitle = $_pageTitle;
    }

    /**
     * Save data to PHP session
     *
     * @param type $registryKey
     * @param type $sessionKey
     * @param type $sessionData
     * @return boolean
     */
    protected function setPHPSession($registryKey, $sessionKey, $sessionData){
        if(empty($registryKey) || empty($sessionKey)){
            return false;
        }

        if(!Zend_Registry::isRegistered($registryKey)){
            return false;
        }

        $phpSession = Zend_Registry::get($registryKey);
        $phpSession->{$sessionKey} = $sessionData;
        Zend_Registry::set($registryKey, $phpSession);

        return true;
    }

    /**
     * Get PHP session data
     *
     * @param type $registryKey
     * @param type $sessionKey
     * @return null|object|array
     */
    protected function getPHPSession($registryKey, $sessionKey = ''){
        $phpSession = null;
        if(empty($registryKey)){
            return $phpSession;
        }

        if(!Zend_Registry::isRegistered($registryKey)){
            return $phpSession;
        }

        $phpSession = Zend_Registry::get($registryKey);
        if(!empty($sessionKey)){
            $sessionData = (!empty($phpSession->{$sessionKey})) ? $phpSession->{$sessionKey} : null;
            $phpSession  = $sessionData;
        }

        return $phpSession;
    }

    /**
     * Destroy PHP session
     *
     * @param type $registryKey
     * @param type $sessionKey
     * @return boolean
     */
    protected function destroyPHPSession($registryKey, $sessionKey = ''){
        if(empty($registryKey)){
            return false;
        }

        if(!Zend_Registry::isRegistered($registryKey)){
            return false;
        }

        $phpSession = Zend_Registry::get($registryKey);

        if(!empty($sessionKey)){
            unset($phpSession->{$sessionKey});
        }
        else{
            $phpSession->unsetAll();
        }
        Zend_Registry::set($registryKey, $phpSession);

        return true;
    }

    /**
     * Debug PHP session data
     *
     * @param type $registryKey
     * @param type $sessionKey
     * @return type
     */
    protected function debugPHPSession($registryKey, $sessionKey = ''){
        $_enable = isset($this->_debugSettings['phpSession']['enable']) ? $this->_debugSettings['phpSession']['enable'] : 0;
        if(!$_enable){
            return;
        }

        //
        $tableData = array(
            array('Variable', 'Data'),
            array('$registryKey', $registryKey),
            array('$sessionKey', $sessionKey),
        );

        $sessionData = ($registryKey === '_SESSION') ? $_SESSION : $this->getPHPSession($registryKey, $sessionKey);
        if(!empty($sessionData)){
            foreach($sessionData as $index => $value){
                $tableData[] = array($index, $value);
            }
        }

        ZC_FirePHP::table($tableData, __CLASS__.'.'.__FUNCTION__.".registryKey[ {$registryKey} ]", array('enable' => $_enable));
    }

    /**
     * Set Browser Cookie
     *
     * @param type $cookieOptions
     * @return type
     */
    protected function  setSessionCookie($cookieOptions = array()){
        //$defaultDomain  = preg_replace('/^http(s)?:\/\//i', '', $appInfo['app']['domain']);
        $defaultExpires = time() + (60*60*24*1); //1 day
        $defaultDomain  = ($_SERVER['HTTP_HOST'] === 'localhost') ? false : $_SERVER['HTTP_HOST'];
        $defaultSecure  = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) ? true : false;

        $name     = isset($cookieOptions['cookieName']) ? $cookieOptions['cookieName'] : 'cookieName';
        $value    = isset($cookieOptions['cookieValue']) ? $cookieOptions['cookieValue'] : 'cookieValue';
        $domain   = isset($cookieOptions['domain']) ? $cookieOptions['domain'] : $defaultDomain;
        $expires  = isset($cookieOptions['expires']) ? $cookieOptions['expires'] : $defaultExpires;
        $path     = isset($cookieOptions['path']) ? $cookieOptions['path'] : '/';
        $secure   = isset($cookieOptions['secure']) ? $cookieOptions['secure'] : $defaultSecure;
        $httpOnly = isset($cookieOptions['httpOnly']) ? $cookieOptions['httpOnly'] : false;

        $cookieStatus = @setcookie($name, $value, $expires, $path, $domain, $secure, $httpOnly);

        //DEBUG
        $_enable   = isset($this->_debugSettings['cookie']['enable']) ? $this->_debugSettings['cookie']['enable'] : 0;
        $tableData = array(
            array('Variable', 'Data'),
            array('CookieName', $name),
            array('CookieValue', $value),
            array('CookieDomain', $domain),
            array('CookieExpires', $expires),
            array('CookiePath', $path),
            array('CookieSecure', $secure),
            array('CookieHttpOnly', $httpOnly),
            array('Status', $cookieStatus),
            array('cookieOptions', $cookieOptions)
        );
        ZC_FirePHP::table($tableData, __CLASS__.'.'.__FUNCTION__, array('enable' => $_enable));

        return $cookieStatus;
    }

    /**
     * Get Browser Cookie
     *
     * @param type $cookieName
     * @return null
     */
    protected function getSessionCookie($cookieName){
        $sessionCookie = null;
        if(empty($cookieName)){
            return $sessionCookie;
        }

        if(isset($_COOKIE[$cookieName])){
            $sessionCookie = $_COOKIE[$cookieName];
        }
        return $sessionCookie;
    }

    /**
     * Return the HTTP Request object
     *
     * @return Zend_Controller_Request_Http
     */
    protected function getHttpRequest(){
        return $this->getRequest();
    }

    /**
     * Get JSON response header
     *
     * @param type $code
     * @return array
     */
    protected function getJsonResponse($code = 200){
        //
        $responseCode = "{$code}";
        $responseList = array();

        //2xx
        $responseList['200'] = array('httpResponseCode' => 200, 'status' => 200, 'message' => 'OK');

        //4xx
        $responseList['400'] = array('httpResponseCode' => 400, 'status' => 400, 'message' => 'Bad Request');
        $responseList['401'] = array('httpResponseCode' => 401, 'status' => 401, 'message' => 'Unauthorized');
        $responseList['403'] = array('httpResponseCode' => 403, 'status' => 403, 'message' => 'Forbidden');
        $responseList['404'] = array('httpResponseCode' => 404, 'status' => 404, 'message' => 'Not Found');
        $responseList['405'] = array('httpResponseCode' => 405, 'status' => 405, 'message' => 'Method Not Allowed');

        //5xx
        $responseList['500'] = array('httpResponseCode' => 500, 'status' => 500, 'message' => 'Internal Server Error - Please contact administrator');
        $responseList['501'] = array('httpResponseCode' => 501, 'status' => 501, 'message' => 'Not Implemented');

        //
        $response = array();
        if(array_key_exists($responseCode, $responseList)){
            $response = $responseList[$responseCode];
        }

        return $response;
    }

    /**
     * JSON/JSONP Response handler
     *
     * @param type $response
     * @param type $callback
     * @return type
     */
    protected function jsonResponse($response, $callback = ''){
        $apiDebugEnable = isset($this->_debugSettings['api']['enable']) ? $this->_debugSettings['api']['enable'] : 0;
        if(!$apiDebugEnable){
            unset($response['debug']);
        }

        //DISABLE LAYOUT
        $this->disableLayout();
        //DISABLE VIEW
        $this->disableView();

        //GET REPONSE OBJECT
        $httpResponse = $this->getResponse();

        // SET HTTP RESPONSE CODE
        if(isset($response['httpResponseCode'])){
            $httpResponse->setHttpResponseCode($response['httpResponseCode']);
            unset($response['httpResponseCode']);
        }

        //IE
        $isBrowserIE = (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false));

        //OUTPUT JSONP
        if(isset($callback) && !empty($callback)){
            Zend_Json::$useBuiltinEncoderDecoder = true;
            #$json = Zend_Json::encode($response);
            $json = Zend_Json::encode($response, null, array('enableJsonExprFinder' => true));
            $data = $callback ."({$json})";

            $contentType = ($isBrowserIE) ? 'text/html' : 'application/json';
            $httpResponse->setHeader('Content-Type', $contentType, true);
            $httpResponse->setBody($data);
            $httpResponse->sendResponse();
            exit(0);
        }

        // OUTPUT JSON - IE FIX
        if ($isBrowserIE){
            $data = Zend_Json::encode($response, null, array('enableJsonExprFinder' => true));

            $httpResponse->sendHeaders('Content-Type', 'text/html', true);
            $httpResponse->setBody($data);
            $httpResponse->sendResponse();
            exit(0);
        }
        else{
            $this->_helper->json($response, array('enableJsonExprFinder' => true));
        }
        return;
    }

    /**
     * Get View Helper HeadMeta (SEO)
     *
     * @return Zend_View_Helper_HeadMeta
     */
    protected function getViewHelperHeadMeta(){
        return $this->view->headMeta();
    }

    /**
     * Get View Helper Headlink (css)
     *
     * @return Zend_View_Helper_HeadLink
     */
    protected function getViewHelperHeadLink(){
        return $this->view->headLink();
    }

    /**
     * Get View Helper InlineScript (js)
     *
     * @return Zend_View_Helper_InlineScript
     */
    protected function getViewHelperInlineScript(){
        return $this->view->inlineScript();
    }

    /**
     * Get View Helper ServerUrl
     * Returns the current host's URL like http://site.com
     *
     * @param  string|boolean $requestUri  [optional] if true, the request URI
     *                                     found in $_SERVER will be appended
     *                                     as a path. If a string is given, it
     *                                     will be appended as a path. Default
     *                                     is to not append any path.
     * @return Zend_View_Helper_ServerUrl
     */
    protected function getViewHelperServerUrl($requestUri = null){
        return $this->view->serverUrl($requestUri);
    }

    /**
     * Get View Helper Doctype
     *
     * @return Zend_View_Helper_Doctype
     */
    protected function getViewHelperDoctype(){
        return $this->view->doctype();
    }



    /**
     * Dynamic load css file
     *
     * @param array $customCSS
     */
    protected function loadCustomStylesheet($customCSS = array()){
        $headLink = $this->getViewHelperHeadLink();
        foreach($customCSS as $css){
            //appendStylesheet($href, $media, $conditionalStylesheet, $extras)
            $headLink->appendStylesheet($css);
        }
    }

    /**
     * Dynamic load javascript file
     *
     * @param array $customJS
     */
    protected function loadCustomScript($customJS = array()){
        $inlineScript = $this->getViewHelperInlineScript();
        foreach($customJS as $script){
            //appendFile($src, $type = 'text/javascript', $attrs = array())
            $inlineScript->appendFile($script, 'text/javascript');
        }
    }

    /**
     * Generate pagination
     *
     * Available options:
     * - totalItemCount
     * - currentPageNumber
     * - itemCountPerPage
     * - scrollingStyle
     * - defaultPageRange
     * - pageParameters
     *
     * @param array $options
     * @return Zend_Paginator
     */
    protected function generatePagination($options = array()){
        extract($options);

        $currentPageNumber = isset($currentPageNumber) ? (int)$currentPageNumber : 1;
        $totalItemCount    = isset($totalItemCount) ? (int)$totalItemCount : 1;
        $itemCountPerPage  = isset($itemCountPerPage) ? $itemCountPerPage : $totalItemCount;
        $defaultPageRange  = isset($defaultPageRange) ? (int)$defaultPageRange : 6;

        //Adapter
        $paginator_adapter = new Zend_Paginator_Adapter_Null($totalItemCount);

        //Paginator
        $paginator = new Zend_Paginator($paginator_adapter);
        $paginator->setCurrentPageNumber($currentPageNumber);
        $paginator->setItemCountPerPage($itemCountPerPage);
        $paginator->setDefaultPageRange($defaultPageRange);

        $scrollingStyle = isset($scrollingStyle) ? $scrollingStyle : 'Sliding';
        $paginator->setDefaultScrollingStyle($scrollingStyle);

        //PAGE PARAMETERS
        $paginator->pageParameters = isset($pageParameters) ? $pageParameters : array();

        return $paginator;
    }

    /**
     * Get page unique identifier
     *
     * @return string
     */
    protected function getUniquePageIdentifier() {
        // CONTROLLER
        $controller     = $this->getParam('controller');
        $controllerPart = explode('-', $controller);
        if(count($controllerPart) > 1){
            $controller = '';
            foreach ($controllerPart as $part){
                $controller .= ucfirst($part);
            }
        }
        $uniquePageIdentifier = $controller;

        // ACTION
        $action     = $this->getParam('action', 'index');
        $actionPart = explode('-', $action);
        if(count($actionPart) > 1){
            $action = '';
            foreach ($actionPart as $item){
                $action .= ucfirst($item);
            }
        }
        $uniquePageIdentifier .= ucfirst($action);

        //ID
        $id = $this->getParam('id', null);
        if($id) {
            $uniquePageIdentifier .= ucfirst($id);
        }

        //XSS
        $uniquePageIdentifierFiltered = $this->xssFilter($uniquePageIdentifier);

        //DEBUG
        $debugMode = isset($this->_debugSettings[__FUNCTION__]['enable']) ? $this->_debugSettings[__FUNCTION__]['enable'] : 0;
        $tableData = array(
            array('Variable', 'Data'),
            array('$controllerPart', $controllerPart),
            array('$actionPart', $actionPart),
            array('$id', $id),
            array('$uniquePageIdentifier', $uniquePageIdentifier),
            array('$uniquePageIdentifierFiltered', $uniquePageIdentifierFiltered),
            array('getAllParams()', $this->getAllParams()),
        );
        ZC_FirePHP::table($tableData, __CLASS__.'.'.__FUNCTION__, array('enable' => $debugMode));

        return $uniquePageIdentifierFiltered;
    }

    /**
     * Get debug settings
     *
     * @return array
     */
    protected function getDebugSettings(){
        $debugSettings = array();
        if(Zend_Registry::isRegistered('debugSettings')){
            $debugSettings = Zend_Registry::get('debugSettings');
        }
        #ZC_FirePHP::warn($debugSettings, __CLASS__.'.'.__FILE__);
        $this->_debugSettings = $debugSettings;
        return $debugSettings;
    }

    /**
     * Router debugging
     */
    protected function debugRouter(){
        $_enable = isset($this->_debugSettings[__FUNCTION__]['enable']) ? $this->_debugSettings[__FUNCTION__]['enable'] : 0;
        if(!$_enable){
            return;
        }

        $router           = Zend_Controller_Front::getInstance()->getRouter();
        $currentRouteName = $router->getCurrentRouteName();
        $currentRoute     = $router->getCurrentRoute();

        $tableData = array(
            array('Variable', 'Data'),
            array('get_class', get_class($router)),
            array('getCurrentRouteName', $currentRouteName),
            array('getCurrentRoute', $currentRoute),
            array('getCurrentRoute.assemble', $currentRoute->assemble()),
        );
        ZC_FirePHP::table($tableData, __CLASS__.'.'.__FUNCTION__);
    }

    /**
     * Memory debugging using uBench
     *
     * @param type $action
     * @return array
     */
    protected function debugUBench($action = 'start'){
        $_ubenchStat = array();
        $_enable     = isset($this->_debugSettings[__FUNCTION__]['enable']) ? $this->_debugSettings[__FUNCTION__]['enable'] : 0;
        if(!$_enable){
            return $_ubenchStat;
        }

        if($action == 'start'){
            $_ubench = new ZC_Ubench();
            $_ubench->start();
            $this->_ubench = $_ubench;
            #ZC_FirePHP::groupCollapsed(__FUNCTION__);
        }

        if($action == 'stop'){
            $_ubench = isset($this->_ubench) ? $this->_ubench : null;
            if($_ubench && get_class($_ubench) == 'ZC_Ubench'){
                $_ubench->end();


                $_ubenchStat = array(
                    array('Elapsed Time', 'Memory Usage', 'Memory Peak Usage'),
                    array( $_ubench->getTime(), $_ubench->getMemoryUsage(), $_ubench->getMemoryPeak()),
                    array( $_ubench->getTime(true), $_ubench->getMemoryUsage(true), $_ubench->getMemoryPeak(true))
                );
            }
            $this->_ubench = null;
            ZC_FirePHP::table($_ubenchStat, __CLASS__.'.'.__FUNCTION__ . ' [ ' . date('Y-m-d H:i:s O T') .' ]');
            #ZC_FirePHP::groupEnd();
        }

        return $_ubenchStat;
    }

    /**
     * getCacheManager
     *
     * @return Zend_Cache_Manager
     */
    public function getCacheManager(){
        $zendResourceCacheManager = new Zend_Application_Resource_Cachemanager();
        $zendCacheManager         = $zendResourceCacheManager->getCacheManager();
        if(Zend_Registry::isRegistered('zendCacheManager')){
            $zendCacheManagerX = Zend_Registry::get('zendCacheManager');
            if(get_class($zendCacheManagerX) === 'Zend_Cache_Manager'){
                $zendCacheManager = $zendCacheManagerX;
            }
        }

        return $zendCacheManager;
    }

    /**
     * getSystemCache
     *
     * @param string $cacheKey
     * @return Zend_Cache_Core
     */
    public function getSystemCache($cacheKey = 'default'){
        $debugSettings = $this->_debugSettings;
        $debugMode     = isset($debugSettings['cache']['enable']) ? $debugSettings['cache']['enable'] : 0;

        ZC_FirePHP::groupCollapsed(__CLASS__.'.'.__FUNCTION__, array('enable' => $debugMode));
        ZC_FirePHP::log($cacheKey, 'cacheKey', $debugMode);

        //CACHE
        $zendCacheManager = $this->getCacheManager();
        $hasCache         = $zendCacheManager->hasCache($cacheKey);
        ZC_FirePHP::log($hasCache, 'hasCache', $debugMode);
        if(!$hasCache){
            $cacheKey = 'default';
        }

        $zendCache = $zendCacheManager->getCache($cacheKey);
        ZC_FirePHP::groupEnd($debugMode);
        return $zendCache;
    }

    /**
     * Write to php error log file
     *
     * @param type $logData
     */
    protected function phpErrorLogger($logData){
        $logDate = '['.date('Y-m-d H:i:s P').']' . PHP_EOL;
        $logData = (is_array($logData)) ? var_export($logData, true) : $logData;
        $logData = $logDate . $logData . str_repeat(PHP_EOL, 2);

        error_log($logData);
    }

    /**
     * Get client remote IP address
     *
     * @param int $full (0|1)
     * @return $clientIP
     */
    protected function getClientIP($full = 0){
        $clientIPFull = array();

        $clientIP       = $_SERVER['REMOTE_ADDR'];
        $clientIPFull[] = "REMOTE_ADDR:{$clientIP}";

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $clientIP       = $_SERVER['HTTP_CLIENT_IP'];
            $clientIPFull[] = "HTTP_CLIENT_IP:{$clientIP}";
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $clientIP       = $_SERVER['HTTP_X_FORWARDED_FOR'];
            $clientIPFull[] = "HTTP_X_FORWARDED_FOR:{$clientIP}";
        }

        if($full){
            $clientIP = implode('||', $clientIPFull);
        }

        return $clientIP;
    }

    /**
     * Update page metadata (SEO)
     *
     * @param type $metaData
     * @return type
     */
    protected function updatePageMetaData($metaData = array()){
        if(empty($metaData)){
            return;
        }

        //Get existing metadata
        $_pageMetaData = $this->_pageMetaData;

        //Merge new metadata with existing metadata
        $mergeMetaData = array_merge($_pageMetaData, $metaData);

        //Save merged metadata
        $this->_pageMetaData  = $mergeMetaData;
    }

    /**
     * Set page metadata (SEO)
     */
    protected function setPageMetaData(){
        $metaData = isset($this->_pageMetaData) ? $this->_pageMetaData : array();
        if(!is_array($metaData)){
            $metaData = array();
        }

        //IF EMPTY - PUT DEFAULT METADATA
        if(empty($metaData)){
            $metaData = array(
                'title' => isset($this->view->pageTitle_) ? $this->view->pageTitle_ : ''
            );
        }

        //DEBUG
        $tableData = array(
            array('Variable', 'Data'),
            array('$this->_pageMetaData', $this->_pageMetaData),
            array('$this->view->pageTitle_', $this->view->pageTitle_),
            array('$metaData', $metaData),
        );

        //
        $doctype = $this->getViewHelperDoctype();
        $doctype->setDoctype(Zend_View_Helper_Doctype::XHTML1_RDFA);

        //
        $headMeta = $this->getViewHelperHeadMeta();

        $metaOpenGraph = array('og:type' => 'website');
        foreach ($metaData as $metaKey => $metaValue) {
            //META
            #if (in_array($metaKey, array('url', 'description', 'keyword'))) {
            #    $headMeta->appendName($metaKey, $metaValue);
            #    $tableData[] = array($metaKey, $metaValue);
            #}
            if (!in_array($metaKey, array('image', 'url'))) {
                $headMeta->appendName($metaKey, $metaValue);
                $tableData[] = array($metaKey, $metaValue);
            }

            //OPEN GRAPH
            if (in_array($metaKey, array('title', 'type', 'url', 'image', 'site_name', 'description'))) {
                $metaOpenGraph["og:{$metaKey}"] = $metaValue;
            }
            if (in_array($metaKey, array('twitter:card', 'twitter:site', 'twitter:title', 'twitter:description', 'twitter:image'))) {
                $metaOpenGraph["{$metaKey}"] = $metaValue;
            }
        }

        //OPEN GRAPH
        foreach ($metaOpenGraph as $keyValue => $content) {
            $headMeta->appendProperty($keyValue, $content);
            $tableData[] = array($keyValue, $content);
        }

        //DEBUG
        $debugMode = isset($this->_debugSettings[__FUNCTION__]['enable']) ? $this->_debugSettings[__FUNCTION__]['enable'] : 0;
        $tableData[] = array('$headMeta->toString()', $headMeta->toString());
        ZC_FirePHP::table($tableData, __CLASS__.'.'.__FUNCTION__, array('enable' => $debugMode));
    }

    /**
     * Generate CSRF token
     *
     * @return string $csrfToken
     */
    protected function generateCSRFToken(){
        $phpSessID = isset($_COOKIE['PHPSESSID']) ? $_COOKIE['PHPSESSID'] : '';
        if(empty($phpSessID)){
            $phpSessID = date('Ymd');
        }

        $sessionID   = md5($phpSessID);
        $tokenString = "{$sessionID}|{$phpSessID}|edge@sia";
        $csrfToken   = sha1($tokenString);

        //SAVE TO COOKIE
        $cookieOptions = array(
            'cookieName'  => 'csrfToken',
            'cookieValue' => $csrfToken,
            'expires'     => 0,
            'httpOnly'    => 1
        );
        $cookieStatus = $this->setSessionCookie($cookieOptions);

        //SAVE TO SESSION
        $isRegistered = Zend_Registry::isRegistered('csrfToken');
        if(!$isRegistered){
            $zendSession = new Zend_Session_Namespace('csrfToken');
            $zendSession->hash = $csrfToken;
            Zend_Registry::set('csrfToken', $zendSession);
        }

        //DEBUG
        $debugMode = isset($this->_debugSettings['csrfToken']['enable']) ? $this->_debugSettings['csrfToken']['enable'] : 0;
        $tableData = array(
            array('Variable', 'Data'),
            array('$sessionID', $sessionID),
            array('$phpSessID', $phpSessID),
            array('$tokenString', $tokenString),
            array('$csrfToken', $csrfToken),
            array('$cookieStatus', $cookieStatus)
        );
        ZC_FirePHP::table($tableData, __CLASS__.'.'.__FUNCTION__, array('enable' => $debugMode));

        return $csrfToken;
    }

    /**
     * Get CSRF token
     *
     * @return string $csrfToken
     */
    public function getCSRFToken(){
        //GET FROM COOKIE
        $csrfToken = isset($_COOKIE['csrfToken']) ? $_COOKIE['csrfToken'] : '';

        //IF EMPTY, GET FROM SESSION
        if(empty($csrfToken)){
            $isRegistered = Zend_Registry::isRegistered('csrfToken');
            if($isRegistered){
                $zendSession = Zend_Registry::get('csrfToken');
                $csrfToken   = isset($zendSession->hash) ? $zendSession->hash : '';
            }
        }

        //IF EMPTY, GENERATE NEW TOKEN
        if(empty($csrfToken)){
            $csrfToken = $this->generateCSRFToken();
        }

        return $csrfToken;
    }

    /**
     * Verify CSRF token
     *
     * @param type $csrfToken
     * @return int $isValidToken (0|1)
     */
    protected function verifyCSRFToken($csrfToken = ''){
        $isValidToken = 0;

        //GET SEVER TOKEN
        $serverToken = $this->getCSRFToken();
        if($csrfToken == $serverToken){
            $isValidToken = 1;
        }

        //DEBUG
        $debugMode = isset($this->_debugSettings['csrfToken']['enable']) ? $this->_debugSettings['csrfToken']['enable'] : 0;
        $tableData = array(
            array('Variable', 'Data'),
            array('$csrfToken', $csrfToken),
            array('$serverToken', $serverToken),
            array('$isValidToken', $isValidToken),
        );
        ZC_FirePHP::table($tableData, __CLASS__.'.'.__FUNCTION__, array('enable' => $debugMode));

        return $isValidToken;
    }

    /**
     * Register ZC view helper
     */
    protected function registerViewHelper(){
        //GET VIEW
        $view = $this->getView();

        //REGISTER HELPER PATH
        $view->addHelperPath('ZC/View/Helper', 'ZC_View_Helper');

        //DEBUG
        $debugMode = isset($this->_debugSettings[__FUNCTION__]['enable']) ? $this->_debugSettings[__FUNCTION__]['enable'] : 0;
        if(!$debugMode){
            return;
        }

        $tableData = array(
            array('Variable', 'Data'),
            array('$view->getHelpers()', $view->getHelpers()),
            array('$view->getHelperPaths()', $view->getHelperPaths()),
            array('$view->getAllPaths()', $view->getAllPaths()),
        );
        ZC_FirePHP::table($tableData, __CLASS__.'.'.__FUNCTION__);
    }

    /**
     * Register ZC view filter
     */
    protected function registerViewFilter(){
        //GET VIEW
        $view = $this->getView();

        //REGISTER FILTER PATH
        $view->addFilterPath('ZC/View/Filter', 'ZC_View_Filter');

        //DEBUG
        $debugMode = isset($this->_debugSettings[__FUNCTION__]['enable']) ? $this->_debugSettings[__FUNCTION__]['enable'] : 0;
        if(!$debugMode){
            return;
        }

        $tableData = array(
            array('Variable', 'Data'),
            array('$view->getFilters()', $view->getFilters()),
            array('$view->getFilterPaths()', $view->getFilterPaths()),
            array('$view->getAllPaths()', $view->getAllPaths()),
        );
        ZC_FirePHP::table($tableData, __CLASS__.'.'.__FUNCTION__);
    }

    /**
     * Gets a parameter from the {@link $_request Request object}.  If the
     * parameter does not exist, NULL will be returned.
     *
     * If the parameter does not exist and $default is set, then
     * $default will be returned instead of NULL.
     *
     * Clean any XSS potential attacks
     *
     * @param string $paramName
     * @param mixed $default
     * @return mixed
     */
    public function getFilterParam($paramName, $default = null) {
        //debugging
        $tableData = array(
            array('Variable', 'Data'),
            array('$paramName', $paramName),
            array('$default', $default),
        );

        //GET PARAM VALUE
        $value = $this->getParam($paramName, $default);
        //debugging
        $tableData[] = array('$value', $value);

        if($value == ''){
            return $value;
        }

        //START FILTER
        $cleanValue = $this->xssFilter($value);

        //debugging
        $tableData[] = array('$cleanValue', $cleanValue);
        $debugMode = isset($this->_debugSettings[__FUNCTION__]['enable']) ? $this->_debugSettings[__FUNCTION__]['enable'] : 0;
        ZC_FirePHP::table($tableData, __CLASS__.'.'.__FUNCTION__, array('enable' => $debugMode));

        return $cleanValue;
    }

    public function xssFilter($data = '') {
        //START FILTER
        $cleanData = trim($data);
        if($cleanData == ''){
            return $cleanData;
        }

        //
        $xssFilter = new ZC_XssFilter();
        $cleanData = $xssFilter->filterData($data);

        return $cleanData;
    }

    protected function fileLogger($logFile = '', $logData = '', $logEnable = 1){
        if(empty($logEnable)){
            return false;
        }

        if (empty($logFile)) {
            return false;
        }

        if (file_exists($logFile) && !is_writable($logFile)) {
            return false;
        }

        $logDir = dirname($logFile);
        if (!file_exists($logDir)) {
            $statusCreateDir = @mkdir($logDir, 0777, true);
            if ($statusCreateDir === FALSE) {
                return false;
            }
        }

        //FORMAT LOG DATE
        $logDate = '[' . date('Y-m-d H:i:s P') . ']' . PHP_EOL;
        $logData = (is_array($logData)) ? var_export($logData, true) : $logData;
        $logData = $logDate . $logData . str_repeat(PHP_EOL, 2);

        //WRITE TO FILE
        $statusWrite = @file_put_contents($logFile, $logData, FILE_APPEND);

        return ($statusWrite === FALSE) ? false : true;
    }

    protected function writeToApplicationLog($logData = '', $logEnable = 1){
        $logFile = APPLICATION_LOG_PATH . DIRECTORY_SEPARATOR . 'application-log'.date('Ymd').'.txt';
        return $this->fileLogger($logFile, $logData, $logEnable);
    }
}
