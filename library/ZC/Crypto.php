<?php

/**
 * Description of ZC_Crypto
 *
 * @author Kamarul Ariffin Ismail <kamarul.ismail@gmail.com>
 */

class ZC_Crypto {

    public function __construct() {
        // //INCLUDE phpseclib
        // $phpseclib = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'phpseclib';
        // // Ensure library/ is on include_path
        // set_include_path(implode(PATH_SEPARATOR, array(
        //     $phpseclib,
        //     get_include_path(),
        // )));
    }

    public function encrypt($data){
        $data_encrypted = $this->doEncryption($data);
        return $data_encrypted;
    }

    public function decrypt($data){
        $data_decrypted = $this->doDecryption($data);
        return $data_decrypted;
    }

    private function doEncryption($data){
        //DEBUGGING
        $tableData = array(
            array('Variable', 'Data'),
            array('$data', $data)
        );
        //
        $data_encrypted = $data;

        //
        $data_encrypted = str_rot13($data_encrypted);
        $tableData[] = array('str_rot13', $data_encrypted);

        //
        $data_encrypted = base64_encode($data_encrypted);
        $tableData[] = array('base64_encode', $data_encrypted);

        //
        $data_encrypted = base64_encode($data_encrypted);
        $tableData[] = array('base64_encode', $data_encrypted);

        //DEBUGGING
        $tableData[] = array('Final', $data_encrypted);
        #ZC_FirePHP::table($tableData, __CLASS__ . '.' . __FUNCTION__);

        return $data_encrypted;
    }

    private function doDecryption($data){
        //DEBUGGING
        $tableData = array(
            array('Variable', 'Data'),
            array('$data', $data)
        );
        //
        $data_decrypted = $data;

        //
        $data_decrypted = base64_decode($data_decrypted);
        $tableData[] = array('base64_decode', $data_decrypted);

        //
        $data_decrypted = base64_decode($data_decrypted);
        $tableData[] = array('base64_decode', $data_decrypted);

        //
        $data_decrypted = str_rot13($data_decrypted);
        $tableData[] = array('str_rot13', $data_decrypted);

        //DEBUGGING
        $tableData[] = array('Final', $data_decrypted);
        #ZC_FirePHP::table($tableData, __CLASS__ . '.' . __FUNCTION__);

        return $data_decrypted;
    }
}
