<?php

/**
 * FirePHP Static class
 *
 * @author Kamarul Ariffin Ismail <kamarul.ismail@gmail.com>
 */
class ZC_FirePHP {
    protected static $_instance = null;
    protected static $_isEnable = 0;
    protected static $_library;

    private static function getInstance() {
        $_instance = self::$_instance;

        if ($_instance === null) {
            if(self::$_library == 'FirePHPCore'){
                require_once('ZC/FirePHPCore/FirePHP.class.php');
                $_instance = FirePHP::getInstance(true);
                ob_start();
            }
            else{
                $_instance = Zend_Wildfire_Plugin_FirePhp::getInstance();
            }
            self::$_instance = $_instance;
        }

        return $_instance;
    }

    /**
     * Set library
     *
     * @param type $className
     */
    public static function setLibrary($className){
        self::$_library = $className;
    }

    /**
     * Get library
     *
     * @return type $className
     */
    public static function getLibrary(){
        return self::$_library;
    }

    /**
     * Enable FirePHP
     *
     * @param type $status
     */
    public static function setEnable($status){
        self::$_isEnable = (int)$status;
    }

    /**
     * Get Enable status
     *
     * @return type
     */
    public static function isEnable($bypassEnable = null) {
        $isEnable = self::$_isEnable;

        //
        if(!is_null($bypassEnable)){
            if($isEnable){
                //IF firephp = 1
                if($bypassEnable){
                    //IF BYPASS = 1
                }
                else{
                    //IF BYPASS = 0
                    $isEnable = 0;
                }
            }
            else{
                //IF GLOBAL = 0
                if($bypassEnable){
                    //IF BYPASS = 1
                    $isEnable = 1;
                }
                else{
                    //IF BYPASS = 0
                    $isEnable = 0;
                }
            }
        }

        return $isEnable;
    }

    private static function write($message, $level = Zend_Log::DEBUG, $label = null, $bypassEnable = null) {
        //
        $isEnable = self::isEnable($bypassEnable);
        if($isEnable == 0){
            return false;
        }

        if(self::$_library == 'FirePHPCore'){
            self::getInstance()->fb($message, $label, $level);
        }
        else {
            $options = array(
                'traceOffset' => 4,
                'fixZendLogOffsetIfApplicable' => true
            );
            self::getInstance()->send($message, $label, $level, $options);
        }
    }

    /**
     * Normal log
     *
     * @param type $message
     * @param type $label
     * @param type $enable
     */
    public static function log($message, $label = null, $enable = null) {
        self::write($message, Zend_Wildfire_Plugin_FirePhp::LOG, $label, $enable);
    }

    /**
     * Info
     *
     * @param type $message
     * @param type $label
     * @param type $enable
     */
    public static function info($message, $label = null, $enable = null) {
        self::write($message, Zend_Wildfire_Plugin_FirePhp::INFO, $label, $enable);
    }

    /**
     * Warning
     *
     * @param type $message
     * @param type $label
     * @param type $enable
     */
    public static function warn($message, $label = null, $enable = null) {
        self::write($message, Zend_Wildfire_Plugin_FirePhp::WARN, $label, $enable);
    }

    /**
     * Error
     *
     * @param type $message
     * @param type $label
     * @param type $enable
     */
    public static function error($message, $label = null, $enable = null) {
        self::write($message, Zend_Wildfire_Plugin_FirePhp::ERROR, $label, $enable);
    }

    /**
     * Grouping - start
     *
     * @param type $title
     * @param boolean $options
     * @return boolean
     */
    public static function group($title = '', $options = array()){
        //
        $bypassEnable = isset($options['enable']) ? $options['enable'] : null;
        $isEnable     = self::isEnable($bypassEnable);
        if($isEnable == 0){
            return false;
        }

        if(!isset($options['Collapsed'])){
            $options['Collapsed'] = true;
        }

        unset($options['enable']);
        self::getInstance()->group($title, $options);
    }

    /**
     * Grouping - start collapsed
     *
     * @param type $title
     * @param array $options
     * @return boolean
     */
    public static function groupCollapsed($title = '', $options = array()){
        //
        $bypassEnable = isset($options['enable']) ? $options['enable'] : null;
        $isEnable     = self::isEnable($bypassEnable);
        if($isEnable == 0){
            return false;
        }

        $options['Collapsed'] = true;
        unset($options['enable']);
        self::getInstance()->group($title, $options);
    }

    /**
     * Grouping - close
     *
     * @param type $enable
     * @return boolean
     */
    public static function groupEnd($bypassEnable = null){
        //
        $isEnable = self::isEnable($bypassEnable);
        if($isEnable == 0){
            return false;
        }

        self::getInstance()->groupEnd();
    }

    /**
     * Table
     *
     * @param type $tableData
     * @param type $label
     * @param type $options
     * @return boolean
     */
    public static function table($tableData, $label = null, $options = array()){
        //
        $bypassEnable = isset($options['enable']) ? $options['enable'] : null;
        $isEnable     = self::isEnable($bypassEnable);
        if($isEnable == 0){
            return false;
        }

        if(!is_array($tableData)){
            return false;
        }

        $label = empty($label) ? __CLASS__.'.'.__FUNCTION__ : $label;

        if(self::$_library == 'FirePHPCore'){
            self::getInstance()->fb($tableData, $label, FirePHP::TABLE);
        }
        else {
            $message = new Zend_Wildfire_Plugin_FirePhp_TableMessage($label);
            #$message->setOption('includeLineNumbers', true);
            #$message->setBuffered(true);

            foreach($tableData as $index => $row){
                if($index == 0){
                    $message->setHeader($row);
                    continue;
                }

                $message->addRow($row);
            }

            //
            self::getInstance()->send($message, $label, Zend_Wildfire_Plugin_FirePhp::TABLE);
        }
    }

    /**
     * Trace
     *
     * @param type $label
     * @param type $enable
     * @return boolean
     */
    public static function trace($label = null, $enable = 1){
        if(!$enable){
            return false;
        }

        if(self::$_library == 'FirePHPCore'){
            self::getInstance()->fb($label, FirePHP::TRACE);
            return;
        }

        #Zend_Wildfire_Plugin_FirePhp::TRACE
        self::getInstance()->send('TRACE', $label, Zend_Wildfire_Plugin_FirePhp::TRACE);
    }

}