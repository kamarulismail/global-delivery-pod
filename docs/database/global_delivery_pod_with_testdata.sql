/*
 Navicat Premium Data Transfer

 Source Server         : AMPPS - root
 Source Server Type    : MySQL
 Source Server Version : 50631
 Source Host           : localhost
 Source Database       : global_delivery_pod

 Target Server Type    : MySQL
 Target Server Version : 50631
 File Encoding         : utf-8

 Date: 06/14/2017 09:41:11 AM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `administrator`
-- ----------------------------
DROP TABLE IF EXISTS `administrator`;
CREATE TABLE `administrator` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `admin_type` varchar(25) DEFAULT NULL,
  `display_name` varchar(50) DEFAULT NULL,
  `login_name` varchar(50) DEFAULT NULL,
  `login_pass` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) unsigned DEFAULT '1',
  `last_login` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_is_active` (`is_active`) USING BTREE,
  KEY `index_admin_type` (`admin_type`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `administrator`
-- ----------------------------
BEGIN;
INSERT INTO `administrator` VALUES ('1', '2017-06-14 00:44:42', null, null, 'Administrator', 'administrator', 'd5db308b97db61c4f7659318955062c4060e530a', '1', null);
COMMIT;

-- ----------------------------
--  Table structure for `advertisment`
-- ----------------------------
DROP TABLE IF EXISTS `advertisment`;
CREATE TABLE `advertisment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `ads_name` varchar(255) DEFAULT NULL,
  `ads_type` varchar(50) DEFAULT NULL,
  `ads_price` decimal(12,4) DEFAULT '0.0000',
  `is_active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `advertisment`
-- ----------------------------
BEGIN;
INSERT INTO `advertisment` VALUES ('1', '2017-06-14 01:57:53', null, 'Classic Ads', 'classic', '269.9900', '1'), ('2', '2017-06-14 02:00:03', null, 'Standout Ads', 'standout', '322.9900', '1'), ('3', '2017-06-14 02:00:27', null, 'Premium Ads', 'premium', '349.9900', '1');
COMMIT;

-- ----------------------------
--  Table structure for `customer`
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `login_name` varchar(255) DEFAULT NULL,
  `login_pass` varchar(50) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `customer`
-- ----------------------------
BEGIN;
INSERT INTO `customer` VALUES ('1', '2017-06-14 00:36:39', '2017-06-14 00:38:53', 'UNILEVER', null, 'unilever@user.com', 'd5db308b97db61c4f7659318955062c4060e530a', '1'), ('2', '2017-06-14 00:37:12', '2017-06-14 00:39:07', 'APPLE', null, 'apple@user.com', 'd5db308b97db61c4f7659318955062c4060e530a', '1'), ('3', '2017-06-14 00:38:39', null, 'NIKE', null, 'nike@user.com', 'd5db308b97db61c4f7659318955062c4060e530a', '1');
COMMIT;

-- ----------------------------
--  Table structure for `customer_package`
-- ----------------------------
DROP TABLE IF EXISTS `customer_package`;
CREATE TABLE `customer_package` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(1) unsigned DEFAULT '0',
  `package_name` varchar(255) DEFAULT NULL,
  `customer_id` int(10) unsigned DEFAULT '0',
  `advertistment_id` int(10) unsigned DEFAULT '0',
  `package_type` varchar(50) DEFAULT NULL,
  `package_rules` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `customer_package`
-- ----------------------------
BEGIN;
INSERT INTO `customer_package` VALUES ('1', '2017-06-14 02:58:16', '2017-06-14 03:09:45', '1', '3 for 2 deal on Classic Ads', '1', '1', 'deal', '{\"deal\":{\"buy\":\"3\",\"free\":\"1\"},\"discount\":{\"buy\":\"0\",\"price\":\"0\"}}');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
